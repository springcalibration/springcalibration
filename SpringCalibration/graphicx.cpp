#include "graphicx.h"

Oscilloscope::Oscilloscope(double width, double height, QWidget *parent): QLabel(parent), m_isDraw(false),
	m_width(width), m_height(height), pnts(0)//, m_point_per_sec(10)
{
	
	setFrameStyle(QFrame::Panel|QFrame::Sunken);
	setLineWidth(2);
	setMidLineWidth(0);
	setFixedSize(m_width, m_height);
	QPalette pal;
	pal.setColor(QPalette::Background, Qt::gray);
	setPalette(pal);
	setAutoFillBackground(true);
	// ������ ����������� �����, ������ ���������������, �������� ������ ��� �����
	setCoord(600,500, 25, 25);
}
Oscilloscope::~Oscilloscope()
{}

void Oscilloscope::setSize(double width, double height)
{
	m_width = width;
	m_height = height;
	setFixedSize(m_width, m_height);
	dimX = (double)m_width / m_maxX;
	dimY = (double)m_height/ m_maxY;
	m_phyz_stepX = m_width/(m_maxX / m_stepX);
	m_phyz_stepY = m_height/(m_maxY / m_stepY);
	update();
}
void Oscilloscope::setCoord(double maxX, double maxY, double stepX, double stepY)
{
	m_maxX = maxX;
	m_maxY = maxY;
	m_stepX = stepX;
	m_stepY = stepY;
	m_stepX_cnt = m_maxX / m_stepX;
	m_stepY_cnt = m_maxY / m_stepY;
	//��������� ���������� ���
	m_phyz_stepX = m_width  / (m_maxX / m_stepX);
	m_phyz_stepY = m_height / (m_maxY / m_stepY);
	dimX = (double)m_width  / m_maxX;
	dimY = (double)m_height / m_maxY;	
}

bool Oscilloscope::drawGraph(double* data, size_t size)
{
	for(pnts = 0; pnts < size && pnts < max_pnts; pnts++)
		m_points[pnts] = QPointF(0.1*pnts, data[pnts]);
	update();
	return true;
}

void Oscilloscope::beginDraw()
{
	m_isDraw = true;
	pnts = 0;
}
void Oscilloscope::endDraw()
{
	m_isDraw = false;
}
void Oscilloscope::clear()
{
	m_isDraw = false;
	pnts = 0;
	update();
}
void Oscilloscope::paintEvent(QPaintEvent * e)
{
	QPainter painter(this);
	painter.setPen(QPen(Qt::black, 1));
	//���������� ���������� � ����� �� ���� Y � X ��������������
	for(int i = 1; i <= m_height/m_phyz_stepY; i++)
	{
		painter.drawText(5, m_phyz_stepY*i, " " + QString::number(m_stepY*i));
		painter.drawLine(0,m_phyz_stepY*i,5,m_phyz_stepY*i);
	}

	for(int i = 0; i < m_width/m_phyz_stepX; i++)
	{
		painter.drawText(m_phyz_stepX*i, 10, " " + QString::number(m_stepX*i));
		if(i>0) painter.drawLine(m_phyz_stepX*i, 5, m_phyz_stepX*i, 0);
	}
	//������� ��������� �� ����
	painter.setPen(QPen(Qt::blue, 1));
	painter.drawText(m_width - 15, 20, tr("��"));
	painter.drawText(30, m_height, tr("��"));
	//������ ������������ �����
	painter.setPen(QPen(Qt::darkRed,1,Qt::DotLine));
	for(int i = 0; i <= m_height/m_phyz_stepY; i++)
	{
		painter.drawLine(0,m_phyz_stepY*i,m_width,m_phyz_stepY*i);
	}
	for(int i = 1; i < m_width/m_phyz_stepX; i++)
	{
		painter.drawLine(m_phyz_stepX*i, 0, m_phyz_stepX*i, m_height);
	}
	//������ �������
	if(m_isDraw)
	{
		painter.setRenderHint(QPainter::Antialiasing);
		painter.setPen(QPen(Qt::darkBlue, 1));
		painter.drawPolyline(m_points, pnts);
	}
}