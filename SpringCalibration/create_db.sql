
CREATE TABLE users(
	login varchar(32),
	password varchar(32)
);

INSERT INTO users VALUES('admin', 'transatom');

CREATE TABLE springs (
	id int primary key, 
	engine varchar(20), 
	target varchar(20), 
	scheme varchar(20),
	sp_type varchar(20), 
	sp_group int, 
	step real, 
	step_irreg real, 
	not_ortogonal real, 
	stick_diam real,
	inner_diam real, 
	outer_diam real, 
	height_min real, 
        height_max real, 
	static_press int, 
	static_height real, 
	static_arrow_min real, 
        static_arrow_max real,
	exam_press int, 
	cover_height int
);

INSERT INTO springs VALUES 
(1,'2��10�, 2��10��, 2��116','��������� ������������','2��116.30.30.146','����������',1,39.2,4.64,7,16,104,136,344,350,565,217,0,0,880,152);
