#ifndef PROTOCOL_H
#define PROTOCOL_H

#include <QtGui/QtGui>

class Spring;

class Protocol: public QObject
{
	Q_OBJECT
public:
	Protocol(QObject* parent = 0);
	~Protocol();

	QString m_operator;
	QString m_executor;
	// ������������� �������
	//QString engine;
	//QString target;
	//QString scheme;
	//QString type;
	// ���������� ��������� (������������; �����; ����; ���������)
	//QString stick_diam;
	//QString inner_diam;
	//QString outer_diam;
	//QString height;
	//QString static_height; // ������ ��� ����������� ���������, ��
	//QString deform;        // ���������� ����������, ����� - 0 ��

	//Spring sp;
	

	void clear();
	void readNum(){ m_num++; }
public slots:
	void writeProtocol(Spring& sp); // ��������� �������� � ���� PDF-���������
	void printProtocol(Spring& sp); // ��������� �������� �� ������
private:
	QString& buildProtocol(Spring& sp); // c����������� ����� ���������
	int m_num; // ����� ���������
	QString m_text;
};

#endif //PROTOCOL_H