#ifndef GRAPHICX_H
#define GRAPHICX_H

#include <QLabel>
#include <QPalette>
#include <QPainter>
#include <QTimer>
#include "params.h"


struct Point
{
	Point(double _x = 0, double _y = 0): x(_x), y(_y) {}
	double x, y;
};

//�����������
class Oscilloscope: public QLabel{
	Q_OBJECT
public:
	Oscilloscope(double width, double height, QWidget *parent = 0);
	~Oscilloscope();
	void setSize(double w, double h);
	bool drawGraph(double* data, size_t size);

	void setCoord(double maxX, double maxY, double stepX, double stepY);
	void beginDraw();
	void endDraw();
	bool isDraw() const {return m_isDraw;}
	void clear();
protected:
	void paintEvent(QPaintEvent*);
private:
	double m_width, m_height;//������ � ������ ������� � �������
	double m_phyz_stepX, m_phyz_stepY;//������ � �������� ������ �������
	//������������� ��������� ������������ �����
	double m_maxX, m_maxY;//������������ �������� ������� �� ��������������� ����
	double m_stepX, m_stepY;//���� ������� �� �����. ��� ��������� (�������/��������)
	double m_stepX_cnt, m_stepY_cnt;//����� ������������ �������� �� ����
	double dimX, dimY;//������������ ��������������� �� ��� X � ��� Y ��������������

	bool m_isDraw;
	//const int m_point_per_sec;
	static const size_t max_pnts = 6000; // ������������ ����� �����, ������� ����� ����������
	size_t pnts; // ����� ���������� ����� � �������
	QPointF m_points[max_pnts]; 
	
};

#endif //GRAPHICX_H