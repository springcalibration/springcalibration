#include "LtrModules.h"

//��������� ��� ������ � �������� ���������� � ������������ ���������� ��� ������� ����������� ������
//������������ � ������ �� ������� ����� �������� ������ ���� �����
QMutex Ltr41Mod, Ltr42Mod, Ltr11Mod, Ltr212Mod;

//���� - ������� ��������������� ����� ��� ���
volatile bool Ltr11Run = false, Ltr41Run = false, Ltr212Run = false;

std::ofstream Ltr41Log;

//�����, ������� ������������ ������� Ltr11 ��� �������� ������������� ����������
static DWORD src[10000];//������ �������� � �������
//-------------------------------------------------------------------------------------
//������ Ltr11
Ltr11Module::Ltr11Module(QObject* parent, unsigned int slot): QObject(parent), m_slot(slot)
{
	m_isOpen = false;
	m_error = LTR11_Init(&m_ltr);
}
bool Ltr11Module::open()
{
	Ltr11Mod.lock();
	if((m_error = LTR11_Open(&m_ltr, SADDR_DEFAULT, SPORT_DEFAULT, "", m_slot)) != LTR_OK){
		return (m_isOpen = false);
	}
	if((m_error = LTR11_GetConfig(&m_ltr)) != LTR_OK){
		return (m_isOpen = false);
	}
	Ltr11Mod.unlock();
	return (m_isOpen = true);
}
bool Ltr11Module::isOpen()
{
	return m_isOpen;
}
void Ltr11Module::close()
{
	Ltr11Mod.lock();
	m_error = LTR11_Close(&m_ltr);
	Ltr11Mod.unlock();
	if(m_error == LTR_OK) m_isOpen = false;
}
QString Ltr11Module::getErrorString(INT error)
{
	return tr((const char*)LTR11_GetErrorString(error));
}
INT Ltr11Module::setConfig(BYTE canal, INT divider, INT prescaler)
{
	//F = LTR11_CLOCK / (prescaler*(divider+1)) - ������� �������������
	//LTR11_CLOCK = 15000
	//prescaler = 64, divider = 116 => F = 2000 ��
	Ltr11Mod.lock();
	//��������� ���������������
	m_ltr.StartADCMode = LTR11_STARTADCMODE_INT;//����� ����� ������ �� ���������� ������� �� ��
	m_ltr.InpMode = LTR11_INPMODE_INT;//���������� ����� �������������
	m_ltr.ADCMode = LTR11_ADCMODE_ACQ;
	m_ltr.ADCRate.divider = divider;//��������
	m_ltr.ADCRate.prescaler = prescaler;//������������
	m_ltr.LChQnt = 1;
	m_ltr.LChTbl[0] = canal;
	INT err = LTR11_SetADC(&m_ltr);
	Ltr11Mod.unlock();
	return err;
}
//����������� �������������������� ���������� (���������� ���), ������������ �� ������������
double Ltr11Module::readVoltage()
{
	std::ofstream out("Ltr11Thread.log", std::ios::out);
	if(!isOpen()){
		if(out) out << "������. �� ��� ������ ����� ��� ������ � �������." << std::endl;
		out.close();
		return 0;
	}
	Ltr11Mod.lock();

	//������ ������
	const unsigned int sz = 50, timeout = 500;
	INT size = sz;
	DWORD src[sz];
	double data[sz] = {0.0}, U;

	//��������� ���� ������
	if(LTR11_Start(&m_ltr) != LTR_OK){
		if(out) out << "������. �� ������� ��������� ���� ������." << std::endl;
	}else
		if(out) out << "������� ���� ������." << std::endl;

	//������ ������� ������
	if(LTR_Recv(&m_ltr.Channel, src, 0, size, timeout) != sz){
		if(out) out << "������. �� ������� ������� ������ ������ �� ������." << std::endl;
	}
	//���������� ������������� ������������, ��������� ������ � ������
	if(LTR11_ProcessData(&m_ltr, src, data, &size, true, true) != LTR_OK){
		if(out) out << "������. �� ������� ���������� �������� �� ������ ������ ������." << std::endl;
	}
	if(size == sz)
	{
		U = 0.0;
		for(int i = 0; i < size; i++)
			U += data[i];
		U = U/size;
		if(out) out << "����������: " << U << std::endl;
	}
	else{
		if(out) out << "������. ����� ������ ���� �������." << std::endl;
	}

	//������������� ���� ������
	if(LTR11_Stop(&m_ltr) != LTR_OK){
		if(out) out << "������. �� ������� ���������� ���� ������ � ������." << std::endl;
	}else
		if(out) out << "���� ������ ����������." << std::endl;
	Ltr11Mod.unlock();
	if(out.is_open()) out.close();
	return U;
}
//�����������: ����� ������ � ���������������
//�������: ��������� sz ������� ������, �������� �� � ������ data
//�����������: ����� �������� ��������
void Ltr11Module::readData(double data[], int sz, size_t timeout_)
{
	std::ofstream out("Ltr11Thread.log", std::ios::out);
	if(!isOpen()){
		if(out) out << "������. �� ��� ������ ����� ��� ������ � �������." << std::endl;
		out.close();
		return;
	}
	Ltr11Mod.lock();
	//������ ������
	const unsigned int timeout = timeout_;
	//DWORD src[10000];//����� � �������
	INT size = sz;

	//��������� ���� ������
	if(LTR11_Start(&m_ltr) != LTR_OK){
		if(out) out << "������. �� ������� ��������� ���� ������." << std::endl;
	}else
		if(out) out << "������� ���� ������." << std::endl;

	//������ ������� ������
	if(LTR_Recv(&m_ltr.Channel, src, 0, size, timeout) != sz){
		if(out) out << "������. �� ������� ������� ������ ������ �� ������." << std::endl;
	}
	//���������� ������������� ������������, ��������� ������ � ������
	if(LTR11_ProcessData(&m_ltr, src, data, &size, true, true) != LTR_OK){
		if(out) out << "������. �� ������� ���������� �������� �� ������ ������ ������." << std::endl;
	}
	if(size != sz){
		if(out) out << "������. ����� ������ ���� �������." << std::endl;
	}

	//������������� ���� ������
	if(LTR11_Stop(&m_ltr) != LTR_OK){
		if(out) out << "������. �� ������� ���������� ���� ������ � ������." << std::endl;
	}else
		if(out) out << "���� ������ ����������." << std::endl;
	Ltr11Mod.unlock();
	if(out.is_open()) out.close();
}
//-------------------------------------------------------------------------------------
//������ Ltr41
Ltr41Module::Ltr41Module(QObject* parent, unsigned int slot): QObject(parent), m_slot(slot)
{
	m_terminal = 0;
	m_error = LTR41_Init(&m_ltr);
}
bool Ltr41Module::open()
{
	Ltr41Mod.lock();
	m_error = LTR41_Open(&m_ltr, SADDR_DEFAULT, SPORT_DEFAULT, "", m_slot);
	if(m_error != LTR_OK)
		print(tr("������. �� ������� ������� ����� ����� � Ltr41"));
	m_ltr.StreamReadRate = 10000;//������� ���������� ����� ������ - 10���
	//
	m_error = LTR41_Config(&m_ltr);
	if(m_error != LTR_OK)
		print(tr("������. �� ������� ���������������� ����� ����� � Ltr41"));
	Ltr41Mod.unlock();
	return (m_error == LTR_OK);
}
bool Ltr41Module::isOpen()
{
	return LTR41_IsOpened(&m_ltr) == LTR_OK;
}
void Ltr41Module::close()
{
	Ltr41Mod.lock();
	m_error = LTR41_Close(&m_ltr);
	Ltr41Mod.unlock();
}
QString Ltr41Module::getErrorString(INT error)
{
	return tr((const char*)LTR41_GetErrorString(error));
}
void Ltr41Module::print(const QString &text)
{
	if(m_terminal) m_terminal->append(text);
}
WORD Ltr41Module::readPort()
{
	WORD InputData = 0;
	Ltr41Mod.lock();
	LTR41_ReadPort(&m_ltr, &InputData);
	Ltr41Mod.unlock();
	return InputData;
}
void Ltr41Module::readPort(WORD *InputData)
{
	Ltr41Mod.lock();
	LTR41_ReadPort(&m_ltr, InputData);
	Ltr41Mod.unlock();
}
void Ltr41Module::processData(DWORD* src, WORD* dest, DWORD size)
{
	for(DWORD i = 0; i < size; i++)
		dest[i] = (WORD)(src[i] >> sizeof(WORD)*8);
}
//-------------------------------------------------------------------------------------
//������ Ltr42
Ltr42Module::Ltr42Module(QObject* parent, unsigned int slot): QObject(parent), m_slot(slot)
{
	m_terminal = 0;
	m_error = LTR42_Init(&m_ltr);
}
bool Ltr42Module::open()
{
	Ltr42Mod.lock();
	m_error = LTR42_Open(&m_ltr, SADDR_DEFAULT, SPORT_DEFAULT, "", m_slot);
	Ltr42Mod.unlock();
	return (m_error == LTR_OK);
}
bool Ltr42Module::isOpen()
{
	return LTR42_IsOpened(&m_ltr) == LTR_OK;
}
void Ltr42Module::close()
{
	Ltr42Mod.lock();
	m_error = LTR42_WritePort(&m_ltr, 0);
	m_error = LTR42_Close(&m_ltr);
	Ltr42Mod.unlock();
}
QString Ltr42Module::getErrorString(INT error)
{
	return tr((const char*)LTR42_GetErrorString(error));
}
void Ltr42Module::print(const QString &text)
{
	if(m_terminal) m_terminal->append(text);
}
bool Ltr42Module::writePort(WORD data)
{
	Ltr42Mod.lock();
	m_error = LTR42_WritePort(&m_ltr, data);
	Ltr42Mod.unlock();
	return m_error == LTR_OK;
}
//-------------------------------------------------------------------------------------
//������ Ltr212
Ltr212Module::Ltr212Module(QObject *parent, unsigned int slot): QObject(parent), m_slot(slot)
{
	//�������� ������������� ������ ����� � ������� � 
	//���������� ����� ��������� �������� ������ ���������� �� ���������
	LTR212_Init(&m_ltr);
	m_log.open("Ltr212Log.txt");//debug - ������� ���� ������ ����� ������� ���������
}
//������������� � �������� ������
bool Ltr212Module::open()
{
	Ltr212Mod.lock();
	//�������� ������������� ������ ����� � �������,
	//�������� ����, ���������� ����������� ��������� ��������
	m_error = LTR212_Open(&m_ltr, SADDR_DEFAULT, SPORT_DEFAULT, "", m_slot, "LTR212.BIO");
	if(m_error != 0){
		if(m_error == -10)
			print("������. ������������ ����� ��� ������������!");
		print("������. �� ������� ������� ����� ����� � Ltr212");
		print((const char*)LTR212_GetErrorString(m_error));
		Ltr212Mod.unlock();
		return false;
	}
	//�������� ����������� ���������� � ���� ������
	if(LTR212_TestEEPROM(&m_ltr) != LTR_OK)
		print("������ � ���� ������ �� ������ �������� �� �����������!");

	//��� ������� ������ ������ ������ ������������ ��������� ���� ���������
	m_ltr.size = sizeof(TLTR212);
	m_ltr.AcqMode = 1; //����� ����� ������ (4-��������� ������� ��������)
	m_ltr.UseClb = 0;  //���� ������������� ������������� ������������� (�� ������������)
	m_ltr.UseFabricClb = 0; //���� ������������� ��������� ������������� ������������� (�� ������������)
	m_ltr.LChQnt = 2;  //���������� ���������� �������
	m_ltr.LChTbl[0] = LTR212_CreateLChannel(1, 0);//���������� 1-� �����, ���������� +-20mV
	m_ltr.LChTbl[1] = LTR212_CreateLChannel(2, 0);//���������� 2-� �����, ���������� +-20mV
	m_ltr.REF = 1; // ������� ���������� 5V
	m_ltr.AC = 0;  // ������������ ���������� ������� ����������

	//�������� ��������������� ���������� �� ������
	m_error = LTR212_SetADC(&m_ltr);
	if(m_error != LTR_OK)
		print("������. ��������� � ��������� ���������������� ������!");

	//������� ������ ������ � ������
	//print(QString::number(m_ltr.Fs));
	
	Ltr212Mod.unlock();
	return (m_error == LTR_OK);
}
bool Ltr212Module::isOpen()
{
	return LTR212_IsOpened(&m_ltr) == LTR_OK;
}
void Ltr212Module::close()
{
	Ltr212Mod.lock();
	m_error = LTR212_Close(&m_ltr);
	Ltr212Mod.unlock();
}
QString Ltr212Module::getErrorString(INT error)
{
	return tr((const char*)LTR212_GetErrorString(error));
}
void Ltr212Module::print(const QString& text)
{
	if(m_log) m_log << text.toAscii().data() << std::endl;
}
void Ltr212Module::print(const char* text)
{
	if(m_log) m_log << text << std::endl;
}