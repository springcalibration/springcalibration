#include "widgets.h"

AnalogDisplay::AnalogDisplay(QString title, QWidget *parent): QWidget(parent), m_title(title)
{
	m_minVal = 0;
	m_maxVal = 10.0;
	m_halfVal = 5.0;
	m_anglePerVal = 30.0;
	m_value = 0;
	//m_title = QString(tr("P ��.\n���/��2"));
	//setMinimumSize(200, 200);
	setFixedSize(200, 200);
}
const AnalogDisplay::CalbCoef AnalogDisplay::m_coef[] = {
	CalbCoef(1,0),CalbCoef(1,0),CalbCoef(1,-0.04),CalbCoef(0.98,-0.04),CalbCoef(0.93,-0.04),
	CalbCoef(0.90,-0.04),CalbCoef(0.89,0),CalbCoef(0.89,0.04),CalbCoef(0.93,0.05),CalbCoef(0.95,0.05),CalbCoef(0.95,0.05)
};
//��������� �������
void AnalogDisplay::paintEvent(QPaintEvent *e)
{
	//�����, �������� ����� � ������ �������
	static const QPoint hand[] = {QPoint(7, 8), QPoint(0, 13), QPoint(-7, 8), QPoint(0, -70)};
	QColor handColor(256, 0, 0);
	QColor delimColor(Qt::black);
	//�������� ��������������� ��� ���������� ���������
	int side = qMin(width(), height());
	//������� ���������� ��������� �� �������
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing);
	painter.translate(width()/2, height()/2);
	painter.scale(side/200.0, side/200.0);

	//����������� ������ ����������
	QRadialGradient rgrad(0, 0, 100, 0, 0);
	rgrad.setColorAt(0.0, Qt::white);
	rgrad.setColorAt(0.88, Qt::darkGray);
	rgrad.setColorAt(0.92, Qt::gray);
	rgrad.setColorAt(0.96, Qt::black);
	painter.setPen(Qt::NoPen);
	painter.setBrush(rgrad);
	painter.drawEllipse(QPointF(0.0, 0.0), 96, 96); 
	//����������� ���������� ������� ����������
	//QRadialGradient grad(0, 0, 100, 40, 40);
	QLinearGradient grad(100, 100, -90, -90);
	grad.setColorAt(0.0, Qt::lightGray);
	grad.setColorAt(0.3, Qt::white);
	grad.setColorAt(0.5, Qt::white);
	grad.setColorAt(1.0, Qt::lightGray);
	//grad.setColorAt(0.3, Qt::red);
	//grad.setColorAt(0.7, Qt::darkRed);
	//grad.setColorAt(1.0, Qt::black);
	painter.setPen(Qt::NoPen);
	painter.setBrush(grad);
	painter.drawEllipse(QPointF(0.0, 0.0), 88, 88); 
	//������ �������: ��������� ������������ ������� rotate
	painter.save();
	painter.setPen(Qt::NoPen);
	painter.setBrush(handColor);
	painter.rotate((m_value - m_halfVal)*m_anglePerVal);
	painter.drawConvexPolygon(hand, sizeof(hand)/sizeof(*hand));
	painter.restore();
	//�������� �� �������
	QRadialGradient pgrad(0, 0, 3, 0, 0);
	pgrad.setColorAt(0.0, Qt::lightGray);
	pgrad.setColorAt(1.0, Qt::darkGray);
	painter.setPen(Qt::NoPen);
	painter.setBrush(pgrad);
	painter.drawEllipse(QPointF(0.0, 0.0), 3, 3);
	//������ ������� �� ����������
	painter.setPen(handColor);
	for(int i = 0; i < 12; i++)
	{
		painter.drawLine(88, 0, 96, 0);
		painter.rotate(30.0);
	}
	painter.setPen(delimColor);
	for(int i = 0; i < 60; i++)
	{
		if(i >= 11 && i < 20);//��� ������� �� ������
		else if(i % 5 == 0)//������ "�������" �������
			painter.drawLine(78, 0, 88, 0);
		else
			painter.drawLine(83, 0, 88, 0);
		painter.rotate(6.0);
	}
	
	static const double pi = 3.14;
	static const double angle_inc = pi/6;
	static const int radius = 76;
	int mul = 0;//���������
	double angle = 0;//���� ��������
	painter.setPen(delimColor);
	for(int i = 0; i <= 10; i++)
	{
		angle = angle_inc*(i + 4) + m_coef[i].cangle;
		mul = radius*m_coef[i].cmul;
		painter.drawText(QPointF(mul*cos(angle), mul*sin(angle)), QString::number(i));
	}
	//�������
	QRectF rect(QPointF(50*cos(2*pi/3),50*sin(2*pi/3)), QSizeF(50, 40));
	painter.setPen(Qt::black);
	painter.setFont(QFont("Arial", 10, QFont::Bold));
	painter.drawText(rect, Qt::AlignCenter | Qt::AlignBottom, m_title);
}
void AnalogDisplay::setRangeValue(double minVal, double halfVal, double maxVal)
{
	m_minVal = minVal;
	m_maxVal = maxVal;
	m_halfVal = halfVal;
}
void AnalogDisplay::setValue(double value)
{
	if(value > m_maxVal) m_value = m_maxVal;
	else if(value < m_minVal) m_value = m_minVal;
	else m_value = value;
	update();
}
MainTestButton::MainTestButton(const QString& title, int num, QWidget* parent): QLabel(title, parent), m_num(num)
{
	setFrameStyle(QFrame::Panel|QFrame::Raised);
	setLineWidth(2);
	setMidLineWidth(0);

	QFont btnFont("Times", 15, QFont::Normal);
	QSize minSize(350, 50), maxSize(500, 80);
	setMinimumSize(minSize);
	setMaximumSize(maxSize);
	setFont(btnFont);
	setWordWrap(true);
	setAlignment(Qt::AlignCenter);
	setBackgroundRole(QPalette::Window);
	setAutoFillBackground(true);
	default_palette = palette();
	QLinearGradient lgrad(0, 0, 0, height());
	lgrad.setColorAt(0.0, Qt::darkGray);
	lgrad.setColorAt(0.5, Qt::gray);
	lgrad.setColorAt(1.0, Qt::darkGray);
	default_palette.setBrush(QPalette::Window, QBrush(lgrad));
	setPalette(default_palette);
	active_palette.setBrush(QPalette::Window, QBrush(QColor(154, 245, 135)));
}
void MainTestButton::mousePressEvent(QMouseEvent* e)
{
	if(e->button() == Qt::LeftButton) press();
	QWidget::mousePressEvent(e);
}
void MainTestButton::mouseReleaseEvent(QMouseEvent* e)
{
	if(e->button() == Qt::LeftButton){
		release();
		emit pressed(m_num);
	}
	QWidget::mouseReleaseEvent(e);
}
SubTestButton::SubTestButton(const QString& title, int num, QWidget* parent): MainTestButton(title, num, parent)
{
	setLineWidth(1);
	QFont btnFont("Times", 12, QFont::Normal);
	QSize minSize(600, 30), maxSize(800, 50);
	setMinimumSize(minSize);
	setMaximumSize(maxSize);
	setFont(btnFont);
	setWordWrap(false);
	setAlignment(Qt::AlignLeft|Qt::AlignVCenter);
}
void MenuList::showMenu(){
	for(int i = 0; i < size; i++)
		items[i]->show();
}
void MenuList::hideMenu(){
	for(int i = 0; i < size; i++)
		items[i]->hide();
}
void MenuList::nextItem(){
	items[item]->release();
	item = (item + 1)%size;
	items[item]->press();
}
void MenuList::prevItem(){
	items[item]->release();
	items[(--item >= 0) ? item : (item = size - 1)]->press();
}
void MenuList::pressItem(int i){
	if(i < 0) return;
	if(item >= 0) items[item]->release();
	if(i%size == item){item = -1; return;}
	items[item = i%size]->press();
}
void MenuList::setMinSize(const QSize& sz){
	for(int i = 0; i < size; i++)
		items[i]->setMinimumSize(sz);
}
void MenuList::setMaxSize(const QSize& sz){
	for(int i = 0; i < size; i++)
		items[i]->setMaximumSize(sz);
}
void MenuList::setFixedSize(const QSize& sz){
	for(int i = 0; i < size; i++)
		items[i]->setFixedSize(sz);
}
//to do
void CheckMenuList::pressItem(int i)
{
	if(i < 0) return;
	i = i % size;
	if(m_check_item[i])
	{
		items[i]->release();
		m_check_item[i] = false;
	}
	else
	{
		items[i]->press();
		m_check_item[i] = true;
	}
}
TestInfo::TestInfo(const QString& text, QWidget *parent): QLabel(text, parent)
{
	setFrameStyle(QFrame::Panel|QFrame::Sunken);
	setLineWidth(1);
	setMidLineWidth(0);
	setMinimumWidth(180);
	setMinimumHeight(300);
	//setMaximumWidth(300);
	QFont font("Times", 12, QFont::Light);
	setFont(font);
	setWordWrap(true);
	setAlignment(Qt::AlignLeft|Qt::AlignTop);
	setBackgroundRole(QPalette::Base);
	setAutoFillBackground(true);
}
TestTitle::TestTitle(const QString& text, QWidget *parent): QLabel(text, parent)
{
	QFont font("Times", 18, QFont::Bold);
	setFont(font);
	setWordWrap(true);
	setAlignment(Qt::AlignCenter | Qt::AlignTop);
}
MsgQuestion::MsgQuestion(const QString& title, const QString& text, QWidget* parent)
:QMessageBox(QMessageBox::Question, title, text, QMessageBox::NoButton, parent)
{
	QPushButton *m_cmdYes = new QPushButton(tr("��"), this);
	QPushButton *m_cmdNo = new QPushButton(tr("���"), this);
	addButton(m_cmdYes, QMessageBox::YesRole);
	addButton(m_cmdNo, QMessageBox::NoRole);
	connect(m_cmdYes, SIGNAL(clicked()), SIGNAL(yesClicked()));
	connect(m_cmdNo, SIGNAL(clicked()), SIGNAL(noClicked()));
	pYes = m_cmdYes;
	pNo = m_cmdNo;
}
//-----------------------------------------------------------------------------------------------
//����������� �������������
Switcher::Switcher(const QString& title, QWidget* parent)
	: QLabel(parent), m_on_angle(40), m_off_angle(-40), m_title(title)
{
	m_on_text = tr("���");
	m_off_text = tr("����");
	m_state = _off;
	m_angle = m_off_angle;
}
void Switcher::paintEvent(QPaintEvent *)
{
	//�������� ��������������� ��� ���������� ���������
	int side = qMin(width(), height());
	qreal scale = 0.7;
	//������� ���������� ��������� �� �������
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing);
	//(1 - scale)*height()/2 - ��������, ����������� ��� ������� �������
	painter.translate(width()/2, height()/2 + (1 - scale)*height()/2);
	painter.scale(side/100.0, side/100.0);

	QColor lineColor(Qt::black);
	//������� �����
	QColor lightC(242, 230, 157), mediumC(234, 213, 90), darkC(134, 117, 17), blackC(56, 49, 7);
	
	//����������� ������ �������������(����� ����)
	QRadialGradient rgrad(0, 0, 50*scale, 0, 0);
	//����� �������
	rgrad.setColorAt(0.0, Qt::white);
	rgrad.setColorAt(0.88, Qt::darkGray);
	rgrad.setColorAt(0.92, Qt::gray);
	rgrad.setColorAt(1.0, Qt::black);
	painter.setPen(Qt::NoPen);
	painter.setBrush(rgrad);
	painter.drawEllipse(QPointF(0.0, 0.0), 50*scale, 50*scale);

	//���������� ������ �������������
	QLinearGradient llgrad(48*scale, 10*scale, -48*scale, -10*scale);
	llgrad.setColorAt(0.0, Qt::white);
	llgrad.setColorAt(0.2, Qt::lightGray);
	llgrad.setColorAt(0.6, Qt::white);
	llgrad.setColorAt(1.0, Qt::lightGray);
	painter.setBrush(llgrad);
	painter.drawEllipse(QPointF(0.0, 0.0), 45*scale, 45*scale);

	//���������� ���� �������������
	painter.setBrush(Qt::gray);
	painter.drawEllipse(QPointF(0.0, 0.0), 40*scale, 40*scale);

	//������ "�����" �������������
	static const QPoint hand[] = {QPoint(10*scale, 23*scale), QPoint(0, 38*scale), QPoint(-10*scale, 23*scale), QPoint(-10*scale, -25*scale), QPoint(10*scale, -25*scale)};
	painter.save();
	painter.setBrush(Qt::black);
	painter.rotate(m_angle);
	painter.scale(1, -1);
	painter.drawConvexPolygon(hand, sizeof(hand)/sizeof(*hand));
	painter.restore();

	//�������� �� �������
	QRadialGradient pgrad(0, 0, 3, 0, 0);
	pgrad.setColorAt(0.0, Qt::lightGray);
	pgrad.setColorAt(1.0, Qt::darkGray);
	painter.setPen(Qt::NoPen);
	painter.setBrush(pgrad);
	painter.drawEllipse(QPointF(0.0, 0.0), 3*scale, 3*scale);

	//������ ������� "���" � "����"
	painter.setPen(lineColor);
	painter.setFont(QFont("Arial", 10, QFont::Bold));
	painter.drawText(-50, -50, 40, 20, Qt::AlignLeft, m_off_text);
	painter.drawText(10, -50, 40, 20, Qt::AlignRight, m_on_text);
	painter.drawText(-50, -65, 100, 30, Qt::AlignHCenter, m_title);
}
void Switcher::mousePressEvent(QMouseEvent* e)
{
	toggle();
	QWidget::mousePressEvent(e);
}
void Switcher::toggle()
{
	switch(m_state){
		case _on:
			m_angle = m_off_angle;
			m_state = _off;
			emit off();
			break;
		case _off:
			m_angle = m_on_angle;
			m_state = _on;
			emit on();
			break;
	}
	update();
}
void Switcher::switchOff()
{
	m_angle = m_off_angle;
	m_state = _off;
	emit off();
	update();
}
void Switcher::switchOn()
{
	m_angle = m_on_angle;
	m_state = _on;
	emit on();
	update();
}
//-----------------------------------------------------------------------------------------------
//������������� �� ������ ������ QDial
SwitcherSA4::SwitcherSA4(QSize size, QWidget* parent): QLabel(parent)
{
	m_title = tr("SA4");
	m_zero_text = tr("0");
	m_one_text = tr("��");
	m_two_text = tr("��");
	m_three_text = tr("����-3");
	m_four_text = tr("����-2");

	m_switch = new QDial;
	m_switch->setRange(0, 4);
	m_switch->setNotchesVisible(true);
	m_switch->setFixedSize(size);
	//setFixedSize(size.width() + 50, size.height() + 40);
	setFixedSize(size.width() + 100, size.height() + 40);

	QHBoxLayout *layout = new QHBoxLayout;
	layout->addWidget(m_switch);
	setLayout(layout);
	connect(m_switch, SIGNAL(valueChanged(int)), SIGNAL(valueChanged(int)));
}
void SwitcherSA4::setValue(int value)
{
	m_switch->setValue(value);
}
void SwitcherSA4::paintEvent(QPaintEvent* e)
{
	QLabel::paintEvent(e);
	//�������� ��������������� ��� ���������� ���������
	int side = qMin(width(), height());
	//������� ���������� ��������� �� �������
	QPainter painter(this);
	painter.scale(side/100.0, side/100.0);
	painter.setPen(Qt::black);
	painter.setFont(QFont("Arial", 8, QFont::Bold));
	//������ �������
	//painter.drawText(30, 80, 20, 20, Qt::AlignLeft, m_zero_text);
	//painter.drawText(5, 33, 20, 20, Qt::AlignLeft, m_one_text);
	//painter.drawText(43, 0, 20, 20, Qt::AlignCenter|Qt::AlignTop, m_two_text);
	//painter.drawText(87, 33, 20, 20, Qt::AlignRight, m_three_text);
	//painter.drawText(70, 80, 20, 20, Qt::AlignLeft, m_four_text);
	painter.drawText(48, 80, 20, 20, Qt::AlignLeft, m_zero_text);
	painter.drawText(23, 33, 20, 20, Qt::AlignLeft, m_one_text);
	painter.drawText(62, 0, 20, 20, Qt::AlignCenter|Qt::AlignTop, m_two_text);
	painter.drawText(107, 33, 60, 20, Qt::AlignLeft, m_three_text);
	painter.drawText(87, 80, 60, 20, Qt::AlignLeft, m_four_text);
}

SwitcherSA::SwitcherSA(QSize size, QWidget *parent): QLabel(parent)
{
	m_title = tr("SA7");
	m_on_text = tr("���");
	m_off_text = tr("����");

	m_switch = new QDial;
	m_switch->setRange(0, 1);
	m_switch->setNotchesVisible(true);
	m_switch->setFixedSize(size);
	//setFixedSize(size.width() + 40, size.height() + 40);
	setFixedSize(size.width() + 20, size.height() + 20);

	QHBoxLayout *layout = new QHBoxLayout;
	layout->addWidget(m_switch);
	setLayout(layout);
	connect(m_switch, SIGNAL(valueChanged(int)), SIGNAL(valueChanged(int)));
}
void SwitcherSA::setValue(int value)
{
	m_switch->setValue(value);
}
void SwitcherSA::paintEvent(QPaintEvent* e)
{
	QLabel::paintEvent(e);
	//�������� ��������������� ��� ���������� ���������
	int side = qMin(width(), height());
	//������� ���������� ��������� �� �������
	QPainter painter(this);
	painter.scale(side/100.0, side/100.0);
	painter.setPen(Qt::black);
	painter.setFont(QFont("Arial", 8, QFont::Bold));
	//������ �������
	//painter.drawText(20, 80, 30, 20, Qt::AlignLeft, m_off_text);
	//painter.drawText(60, 80, 30, 20, Qt::AlignLeft, m_on_text);
	painter.drawText(15, 85, 30, 20, Qt::AlignLeft, m_off_text);
	painter.drawText(65, 85, 30, 20, Qt::AlignLeft, m_on_text);
	painter.drawText(37, 0, 30, 20, Qt::AlignCenter|Qt::AlignTop, m_title);
}
//-----------------------------------------------------------------------------------------------
//���� ����������� ���������
ProtocolDialog::ProtocolDialog(QWidget *parent, Qt::WFlags flags)
	: QDialog(parent, flags)
{
	QGridLayout *glayout = new QGridLayout;
	glayout->addWidget(new QLabel(tr("��� ���������:")), 0, 0, Qt::AlignRight);
	glayout->addWidget(m_eOperator = new QLineEdit, 0, 1);
	glayout->addWidget(new QLabel(tr("��� ������������:")), 1, 0, Qt::AlignRight);
	glayout->addWidget(m_eMaster = new QLineEdit, 1, 1);

	QHBoxLayout *blayout = new QHBoxLayout;
	blayout->addStretch(2);
	blayout->addWidget(pYes = new QPushButton(tr("��")));
	blayout->addStretch(1);
	blayout->addWidget(pNo = new QPushButton(tr("������")));
	blayout->addStretch(2);

	QVBoxLayout *layout = new QVBoxLayout;
	layout->addLayout(glayout);
	layout->addStretch(1);
	layout->addLayout(blayout);

	setLayout(layout);
	setWindowTitle(tr("������ ��� ���������"));
	setMinimumSize(300, 140);

	connect(pYes, SIGNAL(clicked()), SLOT(pressYes()));
	connect(pNo, SIGNAL(clicked()), SLOT(pressNo()));
}
void ProtocolDialog::pressYes()
{
	//���������, ��� ��� ���� ���������
	m_operator = m_eOperator->text().trimmed();
	m_master = m_eMaster->text().trimmed();
	if(m_master.isEmpty() || m_operator.isEmpty()){
		QMessageBox msg(QMessageBox::Information,tr("�������� ������"), tr("�� ������� ������ ��� ���������!"));
		msg.exec();
	}else{
		accept();
	}
}
void ProtocolDialog::pressNo()
{
	m_eOperator->clear();
	m_eMaster->clear();
	reject();
}

ProtocolDialog::~ProtocolDialog(){}
