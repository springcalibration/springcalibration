#ifndef PARAMS_H
#define PARAMS_H

#include <QByteArray>
#include <QString>
#include <QMutex>

// ������ ����������������� �����
#include "BaseParser.h" 

//����� ��������� ����������
struct CC{
	CC(const std::string& _name)
		: name(_name)
		, not_ort_coef    (0.015)
		, laser_scale     (65.53)
		, laser_offset    (1.9)
		, laser_len_scale (0.05762)
		, laser_dest_roll (315)
		, roll_radius     (25)
		, press_scale_1   (2000200.02)
		, press_offset_1  (0)
		, press_scale_2   (2000200.02)
		, press_offset_2  (0)
		, ultra_scale     (42.36)
		, ultra_offset    (1.9)
		, ultra_const     (65)
		, deform_allow    (2) 
		, debug           (true)

		, stick_diam_min(2)
		, stick_diam_max(2)
		, inner_diam_min(4)
		, inner_diam_max(4)
		, outer_diam_min(4)
		, outer_diam_max(4)
		//, height_min(3)
		//, height_max(3)
		//, static_height_min(3)
		//, static_height_max(3)
		//, static_arrow_min(3)
		//, static_arrow_max(3)
	{}
	std::string name; // ��� ������ ����������
	// ���������� ������� � ��������� �������
	double laser_scale;
	double laser_offset;
	// ���������� �� ������� ����� (� ����� ������ ��������� �������)
	double laser_dest_roll; 
	double roll_radius;
	// ����. ���������� �� ����� ����� � ������, ����� ������� ���������� ����������
	double laser_len_scale;
	// ���������� �������� � ������������� (2 �������)
	double press_scale_1;
	double press_offset_1;
	double press_scale_2;
	double press_offset_2;
	// ���������� ������� � �������������� �������
	// �������: (x - ultra_offset)*ultra_scale + ultra_const
	double ultra_scale;
	double ultra_offset;
	double ultra_const;
	// ���������� ����������� ��� ��������� ���������� ���������� 
	double deform_allow;
	
	bool debug;

	double height_coef; // ����������� ��� ������� ������ �������
	double not_ort_coef; // ����������� ��� ������� �������������������� ������� ����������

	////////////////////////////////////////////////////
	// ������� �� ���������� ���������
	// _min - ������ �� ������ �������
	// _max - ������ �� ������� �������
	double stick_diam_min;
	double stick_diam_max;
	double inner_diam_min;
	double inner_diam_max;
	double outer_diam_min;
	double outer_diam_max;
	//double height_min;
	//double height_max;
	//double static_height_min;
	//double static_height_max;
	//double static_arrow_min;
	//double static_arrow_max;
};

class SpringCalibrationConfig: public ConfigParser{
public:
	SpringCalibrationConfig(): m_common("common"){}
	void printValue();//debug
	//������� ������� � ����������
	CC const& getCommon() const {return m_common;}
protected:
	virtual void parseConfigParameter(std::string group_name);
private:
	CC m_common;
};

//���������� ���������� ����������
extern SpringCalibrationConfig config;

#endif //PARAMS_H