#ifndef SPRINGDATABASE_H
#define SPRINGDATABASE_H

#include "widgets.h"
#include "params.h"
#include <QtSql/QtSql>
#include <QtSql/QSQLiteDriver>

// ������� ����������� ��������� � ���� ����� �����
#include <vector>
#include <fstream>
#include <iostream>

// �������������� ������� �������� �������
struct SpringScheme
{
	SpringScheme(int id_ = 0)
		: id(id_), engine(""), target(""), scheme(""), type("")
		, group(0), step(0), step_irregular(0), not_ortogonal(0)
		, stick_diam(0)
		//, inner_diam_min(0), inner_diam_max(0)
		//, outer_diam_min(0), outer_diam_max(0)
		, height_min(0), height_max(0)
		, inner_diam(0)
		, outer_diam(0)
		//, height(0)
		, static_press(0)
		//, static_height_min(0), static_height_max(0)
		, static_arrow_min(0), static_arrow_max(0)
		, static_height(0)
		//, static_arrow(0)
		, deformation(0)
		, exam_press(0), cover_height(0)
	{}
	int id;
	QString engine;
	QString target;
	QString scheme;
	QString type;
	int group;
	double step;
	double step_irregular;
	double not_ortogonal;
	double stick_diam;
	//double inner_diam_min;
	//double inner_diam_max;
	double inner_diam;
	//double outer_diam_min;
	//double outer_diam_max;
	double outer_diam;
	double height_min;
	double height_max;
	//double height;
	int static_press;
	//double static_height_min;
	//double static_height_max;
	double static_height;
	double static_arrow_min;
	double static_arrow_max;
	double deformation; // ���������� ���������� (����� ��� ���������)
	//double static_arrow;
	int exam_press;
	int cover_height;

	QString insertQuery() const;
	QString updateQuery() const;
	QString deleteQuery() const;
	inline QString toString(int) const;
	inline QString toString(double) const;
};

class SpringEdit: public QDialog
{
	Q_OBJECT
public:
	SpringEdit(const SpringScheme& s, QWidget *parent = 0, Qt::WFlags flags = 0);
	~SpringEdit();

	// �������� ���������, ���� ������ ������ ���������
	SpringScheme m_spring;

public slots:
	virtual void accept();
	virtual void reject();
private slots:
	void editEvent(const QString&);
	void setSpring(const SpringScheme& sp);
	void clear();

private:
	// ���� ��� �������������� ���������� �������
	enum {
		_f_engine,
		_f_target,
		_f_scheme,
		_f_type,
		_f_group,
		_f_step,
		_f_step_irregular,
		_f_not_ortogonal,
		_f_stick_diam,
		//_f_i_diam_min,
		//_f_i_diam_max,
		_f_inner_diam,
		//_f_o_diam_min,
		//_f_o_diam_max,
		_f_outer_diam,
		_f_height_min,
		_f_height_max,
		//_f_height,
		_f_static_press,
		//_f_static_height_min,
		//_f_static_height_max,
		_f_static_height,
		_f_static_arrow_min,
		_f_static_arrow_max,
		//_f_static_arrow,
		_f_exam_press,
		_f_cover_height,
		_f_counts
	};
	TEditField* m_fields[_f_counts];
	QPushButton m_btn_save;
	QPushButton m_btn_cancel;
	bool is_edit;
};

// ���� ��� �����/����� ������
class LoginDialog: public QDialog
{
	Q_OBJECT
public:
	LoginDialog(QSqlDatabase& db, QWidget *parent = 0, Qt::WFlags flags = 0);
	
public slots:
	virtual void accept();
	virtual void reject();
private:
	QSqlDatabase& m_db;

	QPushButton m_btn_accept;
	QPushButton m_btn_reject;

	QLineEdit *m_password;
	QLineEdit *m_new_password;
	QLineEdit *m_confirm_new_password;
	QCheckBox *m_change_password;

	void clear();
private slots:
	void enableChangePassword(int state);
};

// to do: ������������ �������� ������ � �� (��������/��������, ��� ��������� ������ � �.�.)
class SpringDatabase : public QDialog 
{
	Q_OBJECT
public:
	SpringDatabase(QWidget *parent = 0, Qt::WFlags flags = 0);
	~SpringDatabase();
	
	bool findSpring(SpringScheme& sp, double inner_diam_, double outer_diam_, double height_);
public slots:

	bool open();
	void close();

private:
	// ��� �������, ����������� �� ��, ������� ���������� ������ ����
	std::vector<SpringScheme> m_springs;

	QTableWidget* m_table;

	// ����������� ������
	enum {
		_btn_login,
		_btn_add,
		_btn_edit,
		_btn_delete,
		_btn_choose,
		_btn_close,
		_btn_counts
	};
	QPushButton* m_btn[_btn_counts];

	typedef enum{
		_logoff,
		_logon
	} Login;

	Login m_login;

	int m_row, m_col; // �������� ������
	QSqlDatabase m_db;
	QString m_db_name;
	QString m_db_definition;

	bool readSpringDatabase();
	bool createDatabase(const QString& database_name);
	bool checkDatabase(const QString& database_name);

private slots:
	bool executeQuery(const QString& sql, QSqlDatabase& db);

	// ������� �������
	int selectionChanged();
	// ��������� ������
	void loginButtonPressed();
	void addButtonPressed();
	void editButtonPressed();
	void removeButtonPressed();
	void chooseButtonPressed();
	void cancelButtonPressed();
signals:
	void fetchSpring(const SpringScheme&);
};

#endif // SPRINGDATABASE_H
