#include "springcalibration.h"

static CC const& Par = config.getCommon();

extern QMutex Ltr41Mod, Ltr11Mod, Ltr42Mod, Ltr212Mod;
volatile static bool RunTest = false;//������� ���������� ���������� ���������
volatile static bool Run212Test = false;

// ����� ��� ������ ������ � Ltr11
const size_t data_sz = 6000;
double data[6000] = {0.0};
int data_ptr = 0;

volatile WORD code_status = 0x4; 
QMutex mtStatus;

QMutex mtSpringPress;
volatile double spring_pressure = 0;
volatile double spring_height = 0;

// �������� ������ �����
#define LOG_ 

SpringCalibration::SpringCalibration(QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags), m_status(0)
{
	setWindowTitle(tr("��������� ������"));

	// ������ � ��
	MainTestButton* reset_btn = new MainTestButton(tr("�����"), 0);
	MainTestButton* db_edit_btn = new MainTestButton(tr("���� ������"), 0);
	reset_btn->setFixedSize(QSize(150, 50));
	db_edit_btn->setFixedSize(QSize(150, 50));

	m_db_dialog = new SpringDatabase(this);

	connect(reset_btn, SIGNAL(pressed(int)), SLOT(resetButtonPressed()));
	connect(db_edit_btn, SIGNAL(pressed(int)), m_db_dialog, SLOT(open()));
	connect(m_db_dialog, SIGNAL(fetchSpring(SpringScheme)), SLOT(fetchSpringScheme(SpringScheme)));

	// ������ ����� � ���� ������
	QHBoxLayout* db_btn = new QHBoxLayout;
	db_btn->addWidget(reset_btn);
	db_btn->addSpacing(20);
	db_btn->addWidget(db_edit_btn);
	db_btn->setAlignment(Qt::AlignRight);

	// �������������� ����, �������� �������������� �������
	QGridLayout* par_layout = new QGridLayout;
	par_layout->addWidget(new Title(tr("�������������� �������")), 0, 1, 1, 2);
	
	par_layout->addWidget(new ParamName(tr("����� ���������:")), 1, 0);
	par_layout->addWidget(m_engine = new TDisplayField("",""), 1, 1, 1, 2);
	par_layout->addWidget(new ParamName(tr("���������� �������:")), 2, 0);
	par_layout->addWidget(m_target = new TDisplayField("",""), 2, 1, 1, 2);
	par_layout->addWidget(new ParamName(tr("������:")), 3, 0);
	par_layout->addWidget(m_scheme = new TDisplayField("",""), 3, 1, 1, 2);
	par_layout->addWidget(new ParamName(tr("��� �������:")), 4, 0);
	par_layout->addWidget(m_type = new TDisplayField("",""), 4, 1, 1, 2);
	m_engine->setWidth(270);
	m_target->setWidth(270);
	m_scheme->setWidth(270);
	m_type->setWidth(270);
	m_engine->setAlignment(Qt::AlignLeft);
	m_target->setAlignment(Qt::AlignLeft);
	m_scheme->setAlignment(Qt::AlignLeft);
	m_type->setAlignment(Qt::AlignLeft);

	par_layout->addWidget(new Title(tr("�� �������"), 13), 5, 1, Qt::AlignLeft);
	par_layout->addWidget(new Title(tr("�����������"), 13), 5, 2, Qt::AlignLeft);
	par_layout->addWidget(new ParamName(tr("������� ������:")), 6, 0);
	par_layout->addWidget(m_et_diam = new TDisplayField("", tr("��.")), 6, 1);
	par_layout->addWidget(m_fa_diam = new TDisplayField("", tr("��.")), 6, 2);
	par_layout->addWidget(new ParamName(tr("���������� �������:")), 7, 0);
	par_layout->addWidget(m_et_inner_diam = new TDisplayField("", tr("��.")), 7, 1);
	par_layout->addWidget(m_fa_inner_diam = new TDisplayField("", tr("��.")), 7, 2);
	par_layout->addWidget(new ParamName(tr("������� �������:")), 8, 0);
	par_layout->addWidget(m_et_outer_diam = new TDisplayField("", tr("��.")), 8, 1);
	par_layout->addWidget(m_fa_outer_diam = new TDisplayField("", tr("��.")), 8, 2);
	par_layout->addWidget(new ParamName(tr("������:")), 9, 0);
	par_layout->addWidget(m_et_height = new TDisplayField("", tr("��.")), 9, 1);
	par_layout->addWidget(m_fa_height = new TDisplayField("", tr("��.")), 9, 2);
	par_layout->addWidget(new ParamName(tr("��� �������:")), 10, 0);
	par_layout->addWidget(m_et_step = new TDisplayField("", tr("��.")), 10, 1);
	par_layout->addWidget(m_fa_step = new TDisplayField("", tr("��.")), 10, 2);
	par_layout->addWidget(new ParamName(tr("�����-�� ���� �������:")), 11, 0);
	par_layout->addWidget(m_et_step_range = new TDisplayField("", tr("��.")), 11, 1);
	par_layout->addWidget(m_fa_step_range = new TDisplayField("", tr("��.")), 11, 2);
	par_layout->addWidget(new ParamName(tr("������-��� ����������:")), 12, 0);
	par_layout->addWidget(m_et_no_ortogonal = new TDisplayField("", tr("��.")), 12, 1);
	par_layout->addWidget(m_fa_no_ortogonal = new TDisplayField("", tr("��.")), 12, 2);
	par_layout->addWidget(new ParamName(tr("������ �������:")), 13, 0);
	par_layout->addWidget(m_et_arrow = new TDisplayField("", tr("��.")), 13, 1);
	par_layout->addWidget(m_fa_arrow = new TDisplayField("", tr("��.")), 13, 2);
	par_layout->addWidget(new ParamName(tr("����������� ��������:")), 14, 0);
	par_layout->addWidget(m_et_static_press = new TDisplayField("", tr("���/��2")), 14, 1);
	par_layout->addWidget(new ParamName(tr("������� ��������:")), 15, 0, Qt::AlignTop);
	par_layout->addWidget(m_et_exam_press = new TDisplayField("", tr("���/��2")), 15, 1, Qt::AlignTop);

	par_layout->addLayout(db_btn, 16, 0, 1, 3, Qt::AlignCenter);

	par_layout->setRowStretch(16, 1);
	par_layout->setHorizontalSpacing(20);

	// ���������� ����� ������������ ��������� ������� � �������
	QHBoxLayout *middle_layout = new QHBoxLayout;
	middle_layout->addStretch(1);
	middle_layout->addLayout(par_layout);
	middle_layout->addSpacing(20);
	middle_layout->addWidget(m_active_scheme = new ActiveScheme("scheme.png", this));
	middle_layout->addStretch(1);

	// for debug
	QHBoxLayout *debug_layout = new QHBoxLayout;
	debug_layout->addStretch(1);
	debug_layout->addWidget(new ParamName(tr("������:")));
	debug_layout->addWidget(m_ltr11_value = new TDisplayField("", tr("��.")));
	debug_layout->addWidget(new ParamName(tr("��������:")));
	debug_layout->addWidget(m_ltr212_value = new TDisplayField("", tr("���/��2")));
	debug_layout->addWidget(new ParamName(tr("���. �������:")));
	debug_layout->addWidget(m_ltr42_value = new TDisplayField("", ""));
	debug_layout->addStretch(1);

	
	// ������
	QStringList buttons = (QStringList() 
		<< tr("��������")
		<< tr("���������")
		<< tr("���������� ����������") 
		<< tr("������ �������")
		<< tr("����") 
		<< tr("��������")
		<< tr("�����"));

	int btn_sz = buttons.count();
	MainTestButton** pbutton = new MainTestButton*[btn_sz];
	for(int i = 0; i < btn_sz; i++)
	{
		pbutton[i] = new MainTestButton(buttons.at(i), 0);
		pbutton[i]->setFixedSize(QSize(120, 50));
	}
	QHBoxLayout* btn_layout = new QHBoxLayout;
	for(int i = 0; i < btn_sz; i++)
		btn_layout->addWidget(pbutton[i]);
	btn_layout->setAlignment(Qt::AlignCenter);
	
	// ���������� � ������� �����������
	connect(pbutton[0], SIGNAL(pressed(int)), SLOT(validateButtonPressed()));  // ��������
	connect(pbutton[1], SIGNAL(pressed(int)), SLOT(recognizeButtonPressed())); // ���������
	connect(pbutton[2], SIGNAL(pressed(int)), SLOT(deformButtonPressed()));    // ���������� ����������
	connect(pbutton[3], SIGNAL(pressed(int)), SLOT(arrowButtonPressed()));     // ������ �������
	connect(pbutton[4], SIGNAL(pressed(int)), SLOT(stopButtonPressed()));      // ����
	connect(pbutton[5], SIGNAL(pressed(int)), SLOT(protocolButtonPressed()));  // ��������
	connect(pbutton[6], SIGNAL(pressed(int)), SLOT(exit()));                   // �����

	// ������ ���������
	m_stBar = new QStatusBar;
	m_stBar->showMessage(tr("\t��������� �������� ���������� ���������"));
	m_stBar->setFixedHeight(60);
	m_stBar->setFont(QFont("Times", 16, QFont::Normal));
	setStatusBar(m_stBar);

	// ��� �����
	QVBoxLayout* main_layout = new QVBoxLayout;
	main_layout->addStretch(1);
	main_layout->addWidget(new TestTitle(tr("��������� ��������� ������")));
	main_layout->addSpacing(50);
	main_layout->addLayout(middle_layout);
	main_layout->addSpacing(10);
	main_layout->addLayout(debug_layout);
	main_layout->addSpacing(50);
	main_layout->addLayout(btn_layout);
	main_layout->addStretch(2);

	// ��������� ������������ ������� ����������
	QWidget* central_wgt = new QWidget;
	central_wgt->setLayout(main_layout);
	setCentralWidget(central_wgt);

	// ������� �������� ���� - ������ ����� ���
	central_wgt->setBackgroundRole(QPalette::Window);
	central_wgt->setAutoFillBackground(true);
	QPalette p = palette();
	p.setBrush(QPalette::Window, QBrush(Qt::lightGray));
	central_wgt->setPalette(p);

	m_msg = new QMessageBox(QMessageBox::Information, tr("���������"), "", QMessageBox::NoButton, this);

	// ��������
	m_protocol = new Protocol(this);
	m_protocol_dialog = new ProtocolDialog(this);

	// ���������� ��� ������ � �������� ������
	m_ltr11 = new Ltr11Module(this, Ltr11_slot);
	m_ltr41 = new Ltr41Module(this, Ltr41_slot);
	m_ltr42 = new Ltr42Module(this, Ltr42_slot);
	m_ltr212 = new Ltr212Module(this, Ltr212_slot);

	// �������� ���������� ���������
	m_check_task = new CheckTask(m_ltr42, this);
	connect(m_check_task, SIGNAL(msg(QString)), SLOT(showMsg(QString)));
	connect(m_check_task, SIGNAL(complete()), SLOT(completeValidate()));

	// ���������, ������ ���������� � ������������� �������
	m_recogn_task = new RecognizeTask(m_ltr11, m_ltr42, this);
	connect(m_recogn_task, SIGNAL(msg(QString)), SLOT(showMsg(QString)));
	connect(m_recogn_task, SIGNAL(complete()), SLOT(completeRecognize()));

	// ��������� �� ���������� ����������
	m_deform_task = new DeformationTask(m_ltr11, m_ltr212, m_ltr42, this);
	connect(m_deform_task, SIGNAL(msg(QString)), SLOT(showMsg(QString)));
	connect(m_deform_task, SIGNAL(complete()), SLOT(completeDeform()));

	// ��������� �� ������ �������
	m_arrow_task = new ArrowTask(m_ltr11, m_ltr212, m_ltr42, this);
	connect(m_arrow_task, SIGNAL(msg(QString)), SLOT(showMsg(QString)));
	connect(m_arrow_task, SIGNAL(complete()), SLOT(completeArrow()));
	
	// ����������� ������ ������� ������ Ltr41 - ������� ��������� ���������
	m_st_reader = new StatusReader(m_ltr41, this);
	connect(m_st_reader, SIGNAL(updateStatus()), SLOT(updateStatusCode()));

	// for debug: ��������� ������� �������� �� ltr11, ltr212, ltr42
	connect(m_check_task, SIGNAL(ltr42code(unsigned)), SLOT(ltr42Cmd(unsigned)));
	connect(m_recogn_task, SIGNAL(ltr42code(unsigned)), SLOT(ltr42Cmd(unsigned)));
	connect(m_recogn_task, SIGNAL(ltr11value(double)), SLOT(ltr11Value(double)));
	connect(m_deform_task, SIGNAL(ltr42code(unsigned)), SLOT(ltr42Cmd(unsigned)));
	connect(m_deform_task, SIGNAL(ltr11value(double)), SLOT(ltr11Value(double)));
	connect(m_deform_task, SIGNAL(ltr212value(double)), SLOT(ltr212Value(double)));
	connect(m_arrow_task, SIGNAL(ltr42code(unsigned)), SLOT(ltr42Cmd(unsigned)));
	connect(m_arrow_task, SIGNAL(ltr11value(double)), SLOT(ltr11Value(double)));
	connect(m_arrow_task, SIGNAL(ltr212value(double)), SLOT(ltr212Value(double)));
	

	// ��������� ������ � �������� (���� ���)
	if(openModules())
	{
		m_st_reader->start();
	}
	else
	{
		m_stBar->showMessage(tr("��������� ����"));
		m_status |= csError;
	}

}
void SpringCalibration::showMsg(const QString& msg)
{
	m_stBar->showMessage(msg);
}
SpringCalibration::~SpringCalibration()
{}

bool SpringCalibration::openModules()
{
	bool bLtr41 = m_ltr41->open();
	bool bLtr11 = m_ltr11->open();
	bool bLtr42 = m_ltr42->open();
	bool bLtr212 = m_ltr212->open();
	if(!(bLtr11 && bLtr42 && bLtr41 && bLtr212))
	{
		QString m = tr("�� ������� ������� ����� ����� �� ���������� ��������:\n") +
			(!bLtr41 ? tr("������ Ltr41\n") : "") +
			(!bLtr11 ? tr("������ Ltr11\n") : "") +
			(!bLtr42 ? tr("������ Ltr42\n") : "") +
			(!bLtr212 ? tr("������ Ltr212\n") : "") +
			tr("\n���������, ��� ������� ������ ��������, � ��� ����� ��������� � ��.");
		closeModules();//������� �� ������, ������� ������� �������
		m_msg->setText(m);
		m_msg->exec();//������� ��������� ������������
		return false;
	}
	return true;
}
void SpringCalibration::closeModules()
{
	if(m_ltr41->isOpen()) m_ltr41->close();
	if(m_ltr11->isOpen()) m_ltr11->close();
	if(m_ltr42->isOpen()) m_ltr42->close();
	if(m_ltr212->isOpen()) m_ltr212->close();
}

void SpringCalibration::exit()
{
	// ������������� ��� ���������� ���������
	stopButtonPressed();
	closeModules();
	QCoreApplication::quit();
}

// ����� ����������� ������ ��������� ltr41 �� ��������� ����
void SpringCalibration::updateStatusCode()
{
	mtStatus.lock();
	WORD code = code_status;
	mtStatus.unlock();
	// �������� �� ��������� ������� ���������
	if(code == 0 && !isError())
	{
		RunTest = false;
		m_ltr42->writePort(0);
		m_stBar->showMessage(tr("��������� ����"));
		m_status |= csError;
		// ���������, ���� �����-�� ����� ��� �������, �� ���� ��� ����������
		m_check_task->wait();
		m_recogn_task->wait();
		m_deform_task->wait();
		m_arrow_task->wait();
	}
	// ������� �� ��������� ������, ��������� ��������
	else if(code == 0x4 && isError())
	{
		m_status = csNone;
		m_stBar->showMessage(tr("��������� �������� ���������� ���������!"));
	}
}

///////////////////////////////////////////////////////////////////////////////
// ��������� ����������� ������

bool SpringCalibration::isAllowRun() const
{
	return !isRun() && isValidate() && !isError();
}
bool SpringCalibration::isValidate() const
{
	return (m_status & csStartWait);
}
bool SpringCalibration::isRun() const
{
	return (m_status & (csCheckRun | csRecognizeRun | csDeformRun | csArrowRun));
}
bool SpringCalibration::isRecognize() const
{
	return (m_status & csSpringRecogn);
}
bool SpringCalibration::isError() const
{
	return m_status & csError;
}
// �������� ���������� ��������� � ����������
void SpringCalibration::validateButtonPressed()
{
	if(!isRun() && !isError())
	{
		RunTest = true;
		m_check_task->start();
		m_status |= csCheckRun;
		m_stBar->showMessage(tr("����������� �������� ���������� ��������� � ������..."));
	}
}
void SpringCalibration::completeValidate()
{
	RunTest = false;
	m_status &= ~csCheckRun;
	m_status |= csStartWait;
	m_stBar->showMessage(tr("�������� � ������ �����"));
}

void SpringCalibration::recognizeButtonPressed()
{
	if(isAllowRun())
	{
		RunTest = true;
		m_recogn_task->setMode(RecognizeTask::rmRecognizeSpring);
		m_recogn_task->start();
		m_status |= csRecognizeRun;
		m_stBar->showMessage(tr("����������� ��������� �������������� ���������� �������..."));
	}else{
		m_msg->setText(tr("������ ��������� ��������� ���������: �� ���������� ��������, ���� �������� ������ ���������!"));
		m_msg->exec();
	}
}

void SpringCalibration::completeRecognize()
{
	// ������������ ���������� ��������� �������
	// drawProfile(m_recogn_task->point_data());

	// ������ ��������� (������������� ���� �������)
	if(m_recogn_task->mode() == RecognizeTask::rmRecognizeSpring)
	{
		// ��������� ���������� ��������� � m_spring_info (��� ���������)
		m_spring_info.inner_diam = m_recogn_task->m_inner_diam;
		m_spring_info.outer_diam = m_recogn_task->m_outer_diam;
		m_spring_info.stick_diam = m_recogn_task->m_stick_diam;
		m_spring_info.height = m_recogn_task->m_height;
		m_spring_info.step = m_recogn_task->m_step;
		m_spring_info.step_irregular = m_recogn_task->m_step_irregular;
		m_spring_info.not_ortogonal = m_recogn_task->m_not_ortogonal;

		// ���������� ���������� ������������
		m_fa_diam->setText(QString::number(m_spring_info.stick_diam, 'f', 0));
		m_fa_inner_diam->setText(QString::number(m_spring_info.inner_diam, 'f', 0));
		m_fa_outer_diam->setText(QString::number(m_spring_info.outer_diam, 'f', 0));
		m_fa_height->setText(QString::number(m_spring_info.height, 'f', 1));
		m_fa_step->setText(QString::number(m_spring_info.step, 'f', 1));
		m_fa_step_range->setText(QString::number(m_spring_info.step_irregular, 'f', 1));
		m_fa_no_ortogonal->setText(QString::number(m_spring_info.not_ortogonal, 'f', 1));

		m_active_scheme->m_stick_diam = QString::number((int)m_spring_info.stick_diam);
		m_active_scheme->m_inner_diam = QString::number((int)m_spring_info.inner_diam);
		m_active_scheme->m_outer_diam = QString::number((int)m_spring_info.outer_diam);
		m_active_scheme->m_step = QString::number((int)m_spring_info.step);
		m_active_scheme->m_height = QString::number((int)m_spring_info.height);
		m_active_scheme->updateIndicators();

		// ������������� �������, ��� ��������� ���������� ������� ������
		m_status |= csSpringMesure;
		if(m_db_dialog->findSpring(m_spring_info.scheme, m_spring_info.inner_diam, m_spring_info.outer_diam, m_spring_info.height))
		{
			// ������� ������� � ��
			m_status |= csSpringRecogn;
			showSpringScheme(m_spring_info.scheme);

			// ���� ������� ������� ����������, �� ��� ��������� �������
			// ������ ����������� ���������� ���������� ��������� �� �������
			m_spring_info.inner_diam = m_spring_info.scheme.inner_diam;
			m_spring_info.outer_diam = m_spring_info.scheme.outer_diam;
			m_spring_info.stick_diam = m_spring_info.scheme.stick_diam;

			m_fa_diam->setText(QString::number(m_spring_info.scheme.stick_diam, 'f', 0));
			m_fa_inner_diam->setText(QString::number(m_spring_info.scheme.inner_diam, 'f', 0));
			m_fa_outer_diam->setText(QString::number(m_spring_info.scheme.outer_diam, 'f', 0));

			m_active_scheme->m_stick_diam = QString::number((int)m_spring_info.scheme.stick_diam);
			m_active_scheme->m_inner_diam = QString::number((int)m_spring_info.scheme.inner_diam);
			m_active_scheme->m_outer_diam = QString::number((int)m_spring_info.scheme.outer_diam);
			m_active_scheme->updateIndicators();
		}
		else
		{
			// ������� �� �������
			QString msg = 
				tr("�� ������� ����� ������� � ���������� �����������.\n") + 
				tr("���������� �������������� ����� ������� � ��.\n") + 
				tr("���� ������ ������� ����������� � �� - �������� ��!")
				;
			QMessageBox::information(0, tr("������� �� �������"), msg);
		}
	}
	// �������� �� ���������� ����������
	else
	{
		m_spring_info.height_ex = m_recogn_task->m_height;
		m_spring_info.step_ex = m_recogn_task->m_step;
		m_spring_info.step_irregular_ex = m_recogn_task->m_step_irregular;

		// ������ ������� ��� ������� ���������
		m_spring_info.exam_height = m_deform_task->m_exam_height;
		m_active_scheme->m_exam_height = QString::number((int)m_spring_info.exam_height);
		m_active_scheme->updateIndicators();

		// ��� ���������
		m_spring_info.deformation = fabs(m_spring_info.height_ex - m_spring_info.height);
		// ������� ������� ���������� ���������� � ������� ��������� ������������
		// ���������� ���������� - ������� � ���������� ������ ������� �� � ����� ������� ��������
		if(m_spring_info.deformation >= Par.deform_allow)
		{
			QString msg = tr("���������� ���������� ���������� - ") + QString::number(m_spring_info.deformation, 'f', 1) + tr(" ��.\n") + 
				tr("�������� ������� �� ������!");
			QMessageBox::warning(0, tr("��������������"), msg);
		}
		else
		{
			// ���� ���������� ���������� � �����, �� � �������� ������� 0.0
			m_spring_info.deformation = 0;
			QMessageBox::information(0, tr("���������"), tr("���������� ���������� � �����."));
		}
	}

	RunTest = false;
	m_status &= ~csRecognizeRun;
	m_stBar->showMessage(tr("��������� �������������� ���������� ������� ���������"));
}

void SpringCalibration::deformButtonPressed()
{
	if(isAllowRun())
	{
		if(!isRecognize())
		{
			m_msg->setText(tr("����� ���������� �� ���������� ���������� ���������� ���������� ��� �������!"));
			m_msg->exec();
			return;
		}
		RunTest = true;
		m_deform_task->m_exam_press = m_spring_info.scheme.exam_press;
		m_deform_task->start();
		m_status |= csDeformRun;
		m_stBar->showMessage(tr("����������� ������� �������� �� �������..."));
	}else{
		m_msg->setText(tr("������ ��������� ��������� ���������: �� ���������� ��������, ���� �������� ������ ���������!"));
		m_msg->exec();
	}
}

void SpringCalibration::completeDeform()
{
	m_status &= ~csDeformRun;
	m_stBar->showMessage(tr("������� �������� ���������"));
	// ��������� ���������� ����������
	m_recogn_task->setMode(RecognizeTask::rmEstimateSpring);
	m_recogn_task->start();
	m_status |= csRecognizeRun;
	m_stBar->showMessage(tr("����������� ��������� �������������� ���������� �������..."));
}

void SpringCalibration::arrowButtonPressed()
{
	if(isAllowRun())
	{
		if(!isRecognize())
		{
			m_msg->setText(tr("����� ���������� �� ������ ������� ���������� ���������� ��� �������!"));
			m_msg->exec();
			return;
		}
		RunTest = true;
		m_arrow_task->m_arrow_press = m_spring_info.scheme.static_press;
		m_arrow_task->start();
		m_status |= csArrowRun;
		m_stBar->showMessage(tr("����������� ����������� �������� �������..."));
	}else{
		m_msg->setText(tr("������ ��������� ��������� ���������: �� ���������� ��������, ���� �������� ������ ���������!"));
		m_msg->exec();
	}
}
void SpringCalibration::completeArrow()
{
	double arrow = m_spring_info.height - m_arrow_task->m_arrow_height;
	// ��������� ������ �� 0.5 � ����������� �� �������� ������� �����
	double d = arrow - (int)arrow;
	if(d > 0 && d <= 0.25)        arrow = (int)arrow;
	else if(d > 0.25 && d < 0.75) arrow = (int)arrow + 0.5;
	else                          arrow = (int)arrow + 1;
	m_spring_info.arrow_height = arrow;

	m_active_scheme->m_static_height = QString::number((int)m_spring_info.arrow_height);
	m_active_scheme->updateIndicators();
	m_fa_arrow->setText(QString::number(m_spring_info.arrow_height,'f',1));

	RunTest = false;
	m_status &= ~csArrowRun;
	m_stBar->showMessage(tr("����������� �������� ���������"));
}

void SpringCalibration::stopButtonPressed()
{
	RunTest = false;
	m_ltr42->writePort(0);
	
	// ���������, ���� �����-�� ����� ��� �������, �� ���� ��� ����������
	m_check_task->wait();
	m_recogn_task->wait();
	m_deform_task->wait();
	m_arrow_task->wait();

	m_status &= ~(csCheckRun | csRecognizeRun | csDeformRun | csArrowRun);
	m_stBar->showMessage(tr("��������� �����������"));
}

///////////////////////////////////////////////////////////////////////////////
//

void SpringCalibration::drawProfile(const std::vector<double>& src)
{
	if(!m_graph) return;
	std::vector<double> points;
	size_t mod = src.size() / 6000 + 1;
	for(size_t i = 0; i < src.size(); i++)
	{
		if(i%mod == 0) // mod == 2
			points.push_back(src[i]);
	}
	m_graph->beginDraw();
	m_graph->drawGraph(&points[0], points.size());
}
void SpringCalibration::showSpringScheme(const SpringScheme& sp)
{
	// ���������� � ����� ����� (��������� �� �������)
	m_engine->setText(sp.engine);
	m_target->setText(sp.target);
	m_scheme->setText(sp.scheme);
	m_type->setText(sp.type);
	m_et_diam->setText(QString::number(sp.stick_diam, 'f', 0));
	m_et_inner_diam->setText(QString::number(sp.inner_diam, 'f', 0));
	m_et_outer_diam->setText(QString::number(sp.outer_diam, 'f', 0));
	m_et_height->setText(QString::number(sp.height_min, 'f', 0)+" - "+QString::number(sp.height_max, 'f', 0));
	m_et_step->setText(QString::number(sp.step, 'f', 1));
	m_et_step_range->setText(QString::number(sp.step_irregular, 'f', 1));
	m_et_no_ortogonal->setText(QString::number(sp.not_ortogonal, 'f', 1));
	m_et_arrow->setText(QString::number(sp.static_arrow_min, 'f', 0)+" - "+QString::number(sp.static_arrow_max, 'f', 0));
	m_et_static_press->setText(QString::number(sp.static_press, 'f', 0));
	m_et_exam_press->setText(QString::number(sp.exam_press, 'f', 0));

	// �������� ����������� � ������� �������� ��������� �� ���.
	m_active_scheme->m_exam_press = QString::number((int)sp.exam_press);
	m_active_scheme->m_static_press = QString::number((int)sp.static_press);
	m_active_scheme->updateIndicators();
}
void SpringCalibration::fetchSpringScheme(const SpringScheme& sp)
{
	// �������, ��������� �� ��
	m_spring_info.scheme = sp;
	m_status |= csSpringRecogn;
	showSpringScheme(m_spring_info.scheme);
}
void SpringCalibration::resetButtonPressed()
{
	if(isRun())
	{
		QMessageBox::warning(0, tr("��������������"), tr("���������� ��������� ���������� ���������!"));
		return;
	}
	m_status = csNone;
	m_stBar->showMessage(tr("��������� �������� ���������� ���������"));

	m_active_scheme->clearIndicators();
	m_spring_info = Spring(); // ��� ������ ��� ��������� �����!
	
	// �������� ���� �����
	m_engine->clear();
	m_target->clear();
	m_scheme->clear();
	m_type->clear();
	m_et_diam->clear();
	m_fa_diam->clear();
    m_et_inner_diam->clear();
	m_fa_inner_diam->clear();
    m_et_outer_diam->clear();
	m_fa_outer_diam->clear();
    m_et_height->clear();
	m_fa_height->clear();
    m_et_step->clear();
	m_fa_step->clear();
    m_et_step_range->clear();
	m_fa_step_range->clear();
    m_et_no_ortogonal->clear();
	m_fa_no_ortogonal->clear();
    m_et_arrow->clear();
	m_fa_arrow->clear();
    m_et_static_press->clear();
    m_et_exam_press->clear();
}

//debug
void SpringCalibration::ltr42Cmd(unsigned code)
{
	std::stringstream ss;
	ss << code;
	m_ltr42_value->setText(QString(ss.str().c_str()));
}

void SpringCalibration::ltr11Value(double value)
{
	m_ltr11_value->setText(QString::number(value, 'f', 3));
}

void SpringCalibration::ltr212Value(double value)
{
	m_ltr212_value->setText(QString::number(value, 'f', 3));
}

///////////////////////////////////////////////////////////////////////////////
// �������� ���������� ��������� � ����������

void CheckTask::run()
{
	// ��������� ��������
	m_ltr42->writePort(0x3); // INP5 && INP6
	emit ltr42code(0x3);

	// ����� ���������� �������� �� ltr41 ���������� 0xC
	WORD status = 0;
	for(;status != 0xC;) // ������� ��������� OUT5 - 4-� ������ ltr41
	{
		mtStatus.lock();
		status = code_status;
		mtStatus.unlock();
	}

	//emit msg(tr("���� �������� ���������� ���������"));
	// �������� ����������� 
	for(;status == 0xC;)
	{
		mtStatus.lock();
		status = code_status;
		mtStatus.unlock();
	}

	// ��������� ��������
	m_ltr42->writePort(0); // ��������� INP5 � INP6
	emit ltr42code(0);

	if(status == 0x4)
		emit msg(tr("�������� � ������ �����"));
	else
		emit msg(tr("��� �������� �������� �������� - �������� �� �����!"));

	emit complete();
}

///////////////////////////////////////////////////////////////////////////////
// ������������� ���� �������
// ���� ������� = 96.5 ��
// ���� ������� = 230 ��

void RecognizeTask::run()
{
	src.clear();
	data.clear();
	// ��������� ��������� (������ �������� ��)
	m_ltr42->writePort(0x1); // INP5 && !INP6
	emit ltr42code(0x1);

	// ���� ��������� ���� 0x6, ����� ��������� ����� ������ ������ � ������
	//emit msg(tr("���� ������ � ������ ���������..."));
	WORD status = 0;
	do
	{
		mtStatus.lock();
		status = code_status;
		mtStatus.unlock();
	}while(status == 0x4 && RunTest); // ���� ��������� ������� �� OUT3

	// ������ � ltr41 ������ ���� ����� 0x6 - OUT3 = 1
	if(status != 0x6)
	{
		emit msg(tr("���-�� ����� �� ���!"));
		m_ltr42->writePort(0);
		emit ltr42code(0);
		return;
	}

	//emit msg(tr("���� ��������� ���������� �������..."));
	
	m_ltr11->setConfig(0x0, 23999, 1); // 625 ��
	//m_ltr11->setConfig(0x0, 116, 64); // 2000 ��
	const int buf_sz = 625;
	double buf[buf_sz];
	int size = buf_sz;
	
#ifdef LOG_
	std::ofstream out("RecognizeTask.log", std::ios::out);
	out << "Params: laser_scale = " << Par.laser_scale << ", laser_offset = " << Par.laser_offset << std::endl;
#endif
	// ������ ������� � ltr11 � ��������� �� � ������� ��� ����������� ���������
	// 1.9V - ������� (0 ��)
	// 9.53V - �������� (500 ��)
	// 7.63V - ���������� ��������
	// 500 / 7.63 = 65.53 - ���������
	// const double scale = 65.53; // ������������� (CbrCoef.Gain) � ���������� ������������
	while(status == 0x6 && RunTest)
	{
		// ������ ���� ������ �� 625 ��������, ������� - 1.5 �������
		m_ltr11->readData(buf, size, 1500); 

		for(int i = 0; i < size; i++)
			src.push_back(Par.laser_scale*(buf[i] - Par.laser_offset));

		emit ltr11value(Par.laser_scale*(buf[size-1] - Par.laser_offset));

		// ��������� ������
		mtStatus.lock();
		status = code_status;
		mtStatus.unlock();
	}

	// ��������� ���������
	m_ltr42->writePort(0);
	emit ltr42code(0);

#ifdef LOG_
	if(out.is_open())
	{
		for(size_t i = 0; i < src.size(); i++)
			out << src[i] << " " << src[i]/Par.laser_scale << std::endl;
		out.close();
	}
#endif

	// �������� ������������� ���������� �������
	if(RunTest)
		recognizeSpring(src);

	emit complete();
}

///////////////////////////////////////////////////////////////////////////////
// ���������� ������ ������� � ltr41 - ������� ��������� ���������

void StatusReader::run()
{
#ifdef LOG_
	std::ofstream out("ltr41_data.log", std::ios::out);
#endif
	for(;;)
	{
		mtStatus.lock();
		code_status = m_ltr41->readPort();
#ifdef LOG_
		out << code_status << std::endl;
#endif
		mtStatus.unlock();
		emit updateStatus();
	}
#ifdef LOG_
	out.close();
#endif
}

///////////////////////////////////////////////////////////////////////////////
// �������� �������� �� ������� � ����� ������� ��� ���������

void PressureControl::run()
{
	if(!RunTest) return;
	Run212Test = true;
	// ��������� ���� ������, �������� ������������
#ifdef LOG_
	std::ofstream out("PressureControl.log", std::ios::out);
	out << "Params: press_scale1 = " << Par.press_scale_1 << ", press_offset1 = " << Par.press_offset_1 
		<< ", press_scale2 = " << Par.press_scale_2 << ", press_offset2 = " << Par.press_offset_2 << std::endl;
#endif
	PTLTR212 pltr = m_ltr212->getHandler();
	if(LTR212_Start(pltr) != LTR_OK)
	{
#ifdef LOG_
		if(out) out << "������ ��� ������� ����� ������." << std::endl;
#endif
		Run212Test = false;
		//return;
	}
	// ������ ���� �������� ����� ���������, ������ ���������� ����� ��������������� ������� - 4 ��-�� � ����� ������
	const DWORD ltr212_sz = 4 * 1; // ������ 1 ����
	DWORD ltr212_src[ltr212_sz];
	double ltr212_data[ltr212_sz];
	DWORD ltr212_size = ltr212_sz;
	DWORD timeout = 500; // ������� 0.5 �������


	//m_ltr11->setConfig(0x1, 116, 64); // 2000 ��, 2-� �����
#ifdef LOG_
	if(out) out << "�������� ���� ������" << std::endl;
#endif

	double pressure;
	double height;
	// ����������� ������� ����� ������
	while(RunTest && Run212Test)
	{
		if(LTR212_Recv(pltr, ltr212_src, NULL, ltr212_size, timeout) != ltr212_size)
		{
#ifdef LOG_
			if(out) out << "������ ��� ����� ������." << std::endl;
#endif
			//Run212Test = false;
		}
		DWORD size = ltr212_size;
		// ��������� ����� ������ � ������
		if(LTR212_ProcessData(pltr, ltr212_src, ltr212_data, &size, TRUE) != LTR_OK){
#ifdef LOG_
			if(out) out << "������ ��� ��������� ��������� ������." << std::endl;
#endif
			//Run212Test = false;
		}
		// ������ ltr212_data ������ ��������� ����� ������ ������
#ifdef LOG_
		if(out) {
			out << "src_size = " << ltr212_size << ", data_size = " << size << std::endl;
			for(DWORD i = 0; i < size; i++)
				out << ltr212_data[i] << std::endl;
			out << std::endl;
		}
		if(ltr212_size/2 != size)
		{
			if(out) out << "������ ����� ������ �������� � �������� �� ���������" << std::endl;
			//Run212Test = false;
		}
#endif
		pressure = Par.press_scale_1*(ltr212_data[0] - Par.press_offset_1) + Par.press_scale_2*(ltr212_data[1] - Par.press_offset_2);
		//to do: ������ ������ ������� � ��������� �����
		//height = Par.ultra_scale*(m_ltr11->readVoltage() - Par.ultra_offset);

		mtSpringPress.lock();
		spring_pressure = pressure;
		//spring_height = height;
		mtSpringPress.unlock();

		emit update();
	}

	// ������������� ���� ������
#ifdef LOG_
	if(out) out << "��������� ���� ������" << std::endl;
	if(LTR212_Stop(pltr) != LTR_OK)
	{
		out << "������ ��� �������� ����� ������." << std::endl;
	}
	if(out.is_open()) out.close();
#endif
}
///////////////////////////////////////////////////////////////////////////////
// ��������� �� ���������� ����������
void DeformationTask::run()
{
	emit msg(tr("���� ��������� ��� ������� ���������"));

#ifdef LOG_
	std::ofstream out("DeformTask.log", std::ios::out);
	out << "Params: ultra_scale = " << Par.ultra_scale << ", ultra_offset = " << Par.ultra_offset << ", ultra_const = " << Par.ultra_const << std::endl;
#endif
	// �������� ���� ������ � �������� �� �������
	m_pc.start();
	WORD status = 0;
	double pressure;
	double height;
	// ��������� 3 �������� ������� ��������
	int iteration = 3;
	m_ltr42->writePort(0xA); // INP8 && INP6
#ifdef LOG_
	emit ltr42code(0xA);
	out << "Cmd: " << 0xA << std::endl;
#endif
	while(iteration-- > 0 && RunTest)
	{
		pressure = height = 0;

		// ������� ��������� �� ltr41 ����� 0x5 (���������� �� ������� ������ ������)
		do
		{
			mtStatus.lock();
			status = code_status;
			mtStatus.unlock();
		}while(status != 0x5 && RunTest);

		if(!RunTest)
		{ 
			m_ltr42->writePort(0); 
#ifdef LOG_
			emit ltr42code(0); 
			out << "Cmd: " << 0xA << std::endl;
#endif
			return; 
		}
#ifdef LOG_		
		out << "���� ��������� �� 0.95 ��������" << std::endl;
#endif
		// ���������� ��������� ������� �������� �������� � �����
		// ��� ���������� �������� 0.95 ������� �������� �� ltr42 �������� ������� 0x1A (���������� ��������).
		for(pressure = 0; pressure < 0.95*m_exam_press && RunTest; )
		{
			mtSpringPress.lock();
			pressure = spring_pressure;
			height = spring_height;
			mtSpringPress.unlock();
#ifdef LOG_
			emit ltr11value(height);
			emit ltr212value(pressure);
			out << pressure << " " << height << std::endl;
#endif
		}
		if(!RunTest)
		{ 
			m_ltr42->writePort(0); 
#ifdef LOG_
			emit ltr42code(0); 
			out << "Cmd: " << 0 << std::endl;
#endif
			return; 
		}
		m_ltr42->writePort(0x1A); 
#ifdef LOG_
		out << "Cmd: " << 0x1A << std::endl;
		out << "���� ������ ��������" << std::endl;
#endif
		//��� ���������� �������� ������� �������� �� ltr42 �������� ������� 0xE
		for(; pressure < m_exam_press && RunTest; )
		{
			mtSpringPress.lock();
			pressure = spring_pressure;
			height = spring_height;
			mtSpringPress.unlock();
#ifdef LOG_
			emit ltr11value(height);
			emit ltr212value(pressure);
			out << pressure << " " << height << std::endl;
#endif
		}
		if(!RunTest)
		{
			m_ltr42->writePort(0); 
#ifdef LOG_
			emit ltr42code(0); 
			out << "Cmd: " << 0 << std::endl;
#endif
			return; 
		}
		m_ltr42->writePort(0xE); // ������ �����������, ��� �������� ����������
#ifdef LOG_
		emit ltr42code(0xE);
		out << "Cmd: " << 0xE << std::endl;
#endif

		m_ltr11->setConfig(0x1, 116, 64); // 2000 ��, 2-� �����
		// ������ ������� ��� ������� ���������
		m_exam_height = Par.ultra_scale*(m_ltr11->readVoltage() - Par.ultra_offset) + Par.ultra_const; 

		// ��������� ������� INP8, INP7 (INP6 ���������) - ������������ �������� �����, ���� �� �� �� ������� �����
		m_ltr42->writePort(0x2);
#ifdef LOG_
		emit ltr42code(0x2);
		out << "Cmd: " << 0x2 << std::endl;
		out << "������ ��� ������� ��������� = " << m_exam_height << std::endl;
		out << "���� �������� �� 0.1 ��������" << std::endl;
		emit ltr11value(m_exam_height);
#endif
		// ��� �������� ������ �� 0.1 ������� ��������, ��������� ��������� ��� ��������� - �������� �� ��������� ����
		// �� ltr42 �������� ������� 0x5.
		for(; pressure > 0.1*m_exam_press && RunTest; )
		{
			mtSpringPress.lock();
			pressure = spring_pressure;
			height = spring_height;
			mtSpringPress.unlock();
#ifdef LOG_
			emit ltr11value(height);
			emit ltr212value(pressure);
			out << pressure << std::endl;
#endif
		}
		if(!RunTest)
		{
			m_ltr42->writePort(0); 
#ifdef LOG_
			emit ltr42code(0); 
			out << "Cmd: " << 0 << std::endl;
#endif
			return; 
		}

		m_ltr42->writePort(0xA); // INP8 && INP6
		
#ifdef LOG_		
		emit ltr42code(0xA);
		out << "Cmd: " << 0xA << std::endl;
		out << "���� ��������" << std::endl << std::endl;
#endif
	}

	m_ltr42->writePort(0);
#ifdef LOG_
	emit ltr42code(0); 
	out << "Cmd: " << 0 << std::endl;
#endif
	// ��������� �����
	Run212Test = false; 
	m_pc.wait();

#ifdef LOG_
	out.close();
#endif

	// ����, ����� �������� ����� �������� � ��������� ���������
	// � ���������� ���������� ��������� ������� ��������� �� ltr41 ����� 0x4
	do
	{
		mtStatus.lock();
		status = code_status;
		mtStatus.unlock();
	}while(status != 0x4 && RunTest);

	emit msg(tr("��������� ��� ��������� ���������"));
	emit complete(); // ������ ���� ��������� �������� ���������� �������
}

///////////////////////////////////////////////////////////////////////////////
// ��������� �� ������ �������

void ArrowTask::run()
{
	emit msg(tr("���� ��������� ��� ����������� ���������"));

#ifdef LOG_
	std::ofstream out("ArrowTask.log", std::ios::out);
	out << "Params: ultra_scale = " << Par.ultra_scale << ", ultra_offset = " << Par.ultra_offset << ", ultra_const = " << Par.ultra_const << std::endl;
#endif

	m_ltr42->writePort(0x2);
	emit ltr42code(2);
	// ���� ��������� ����� 0x5 (�������� ����������� ������������� ������)
	WORD status = 0;
	do
	{
		mtStatus.lock();
		status = code_status;
		mtStatus.unlock();
	}while(status != 0x5 && RunTest);

	if(!RunTest){ m_ltr42->writePort(0); emit ltr42code(0); return; }

	// �������� ���� ������ � �������� �� �������
	m_pc.start();
	
	double pressure;
	double height;

#ifdef LOG_
	out << "���� ��������� �� 0.95 ��������" << std::endl;
#endif
	// ���������� ��������� ������� �������� �������� � �����
	// ��� ���������� �������� 0.95 ����������� �������� �� ltr42 �������� ������� 0x12 (���������� ��������).
	for(pressure = 0; pressure < 0.95*m_arrow_press && RunTest; )
	{
		mtSpringPress.lock();
		pressure = spring_pressure;
		height = spring_height;
		mtSpringPress.unlock();
#ifdef LOG_
		emit ltr11value(height);
		emit ltr212value(pressure);
		out << pressure << " " << height << std::endl;
#endif
	}
	if(!RunTest){ m_ltr42->writePort(0); emit ltr42code(0); return; }
	m_ltr42->writePort(0x12);
	emit ltr42code(0x12);

#ifdef LOG_
	out << "���� ������ ��������" << std::endl;
#endif
	//��� ���������� �������� ����������� �������� �� ltr42 �������� ������� 0x6
	for(; pressure < m_arrow_press && RunTest; )
	{
		mtSpringPress.lock();
		pressure = spring_pressure;
		height = spring_height;
		mtSpringPress.unlock();
#ifdef LOG_
		emit ltr11value(height);
		emit ltr212value(pressure);
		out << pressure << " " << height << std::endl;
#endif
	}
	if(!RunTest){ m_ltr42->writePort(0); emit ltr42code(0); return; }
	m_ltr42->writePort(0x6);
	emit ltr42code(0x6);

	// ��������� ����� ������� ��� ������. ���������
	m_ltr11->setConfig(0x1, 116, 64); // 2000 ��, 2-� �����

	// ������ ������� � ������ ��������� (�� ����������� ���������)
	// ������������ ������� ������� ����� ������� � ��������� ��������� � ������� � ������ ��������� 
	m_arrow_height = Par.ultra_scale*(m_ltr11->readVoltage() - Par.ultra_offset) + Par.ultra_const;

#ifdef LOG_
	out << "������ ��� ����������� ��������� = " << m_arrow_height << std::endl;
	out << "���� �������� �� 0.1 ��������" << std::endl;
	emit ltr11value(m_arrow_height);
#endif
	//��� �������� ������ �� 0.1 ����������� �������� �� ltr42 �������� ������� 0
	for(; pressure > 0.1*m_arrow_press && RunTest; )
	{
		mtSpringPress.lock();
		pressure = spring_pressure;
		height = spring_height;
		mtSpringPress.unlock();
#ifdef LOG_
		emit ltr11value(height);
		emit ltr212value(pressure);
		out << pressure << std::endl;
#endif
	}
	// ������ ������� �� ���������� ��������
	m_ltr42->writePort(0x2);
	emit ltr42code(0x2);
	msleep(50);
	m_ltr42->writePort(0);
	emit ltr42code(0);
	// ��������� �����
	Run212Test = false; // to do: �������� �������!
	m_pc.wait();

	// � ���������� ���������� ��������� ������� ��������� �� ltr41 ����� 0x4
	do
	{
		mtStatus.lock();
		status = code_status;
		mtStatus.unlock();
	}while(status != 0x4 && RunTest);

#ifdef LOG_
	out.close();
#endif
	emit msg(tr("��������� ��� ����������� ��������� ���������"));
	emit complete();
}

///////////////////////////////////////////////////////////////////////////////
// ��������� �������� ������ - ������������� �������������� ���������� �������

static void print_segments(std::vector<Segment>& segments, std::ostream& out)
{
	for(size_t i = 0; i < segments.size(); i++)
	{
		out << "Segment #" << i+1 << std::endl;
		out << "First pnt = " << segments[i].first() << std::endl;
		out << "Last pnt = " << segments[i].last() << std::endl;
		out << "Len = " << segments[i].len() << std::endl;
		out << "Min = " << segments[i].smin << std::endl;
		out << "Max = " << segments[i].smax << std::endl;
		out << "Diam = " << segments[i].diam << std::endl;
		out << std::endl;
	}
}

// ��� ������������� ������ ���� ������:
// - ������ � ��������� ��������� �;
// - ������� ������ stock_d;
// - ���������� ������� d;
// - �������� ������� D;
// D = 2*stock_d + d

bool RecognizeTask::recognizeSpring(const std::vector<double>& src)
{
	preprocess(src, data);
	// ����� ������� � ��������� ��������� - ����� ����� �� ����� ����
	m_height = Par.laser_len_scale * data.size(); //to do: 372
	// ��������� ������ �� 0.5
	double d = m_height - (int)m_height;
	m_height = (double)(d <= 0.5 ? (int)m_height + 0.5 : (int)m_height + 1);

	// ��������� �������������������� ������� ��-���
	m_not_ortogonal = Par.not_ort_coef * m_height;

	std::vector<Segment> segments;
	split_segments(data, segments);

#ifdef LOG_
	std::ofstream out("segments.log", std::ios::out);
	if(out.is_open())
	{
		print_segments(segments, out);
		out.close();
	}
#endif

	// ��������� ���������� �������, ����. ������� � ������� ������
	calc_params(segments);

#ifdef LOG_
	std::ofstream sout("spring.log", std::ios::out);
	if(sout.is_open())
	{
		sout << "height = " << m_height << std::endl;
		sout << "s_diam = " << m_stick_diam << std::endl;
		sout << "i_diam = " << m_inner_diam << std::endl;
		sout << "o_diam = " << m_outer_diam << std::endl;
		sout.close();
	}
#endif
	return true;
}

// ������������� ����� ������, ��������� � ltr11
// - �������� ��������� �������� �� ����������
// - ��������� ������ � ������ � � ����� ������������������
void RecognizeTask::preprocess(const std::vector<double>& src, std::vector<double>& dest)
{
	const double e_code = 500;
	std::vector<double> temp;
	// ������ n ����� ������������ - ����� �� ����������� �������
	for(size_t i = 500; i < src.size(); i++)
	{
		temp.push_back( (src[i] >= m_min_dest && src[i] <= m_max_dest ? src[i] : e_code) );
	}
	std::vector<double>::iterator it_b = temp.begin(), it_e = temp.end();
	while(*it_b == e_code && it_b != it_e) it_b++;
	while(*(it_e - 1) == e_code && it_b != it_e) it_e--;
	// ��� ������
	if(it_b == it_e)
		return;
	// ������� � dest ������
	dest.insert(dest.begin(), it_b, it_e);

#ifdef LOG_
	// ������� ������� � ����
	std::ofstream out1("origin_data.log", std::ios::out);
	if(out1.is_open())
	{
		for(size_t i = 0; i < src.size(); i++)
			out1 << src[i] << std::endl;
		out1.close();
	}
	std::ofstream out("processed_data.log", std::ios::out);
	if(out.is_open())
	{
		for(size_t i = 0; i < dest.size(); i++)
			out << dest[i] << std::endl;
		out.close();
	}
#endif
}

// ��������� ����� ������ �� ��������� �������
// ��������� ��������� ������� ��������
void RecognizeTask::split_segments(std::vector<double>& data, std::vector<Segment>& segments)
{
	// ������������� �������, ��������� �� ����� ��� �� 100 ����. ����� = 5��
	// ������� ������ ������������: �������/������
	int counter = 0;
	const double e_code = 500;
	double delta = 0;

	for(size_t i = 0; i < data.size(); i++)
	{
		if(i > 0) delta = fabs(data[i] - data[i-1]);

		if(data[i] == e_code || delta >= 30) // 30 ��
		{
			if(counter >= 100)
			{
				// ��������� ����� � ��������� �������
				Segment sg;
				sg.smin = data[i-1];
				sg.smax = data[i-1];
				for(size_t j = i - counter + 1; j < i; j++)
				{
					// smin - �������� ������� ������
					if(sg.smin > data[j])
					{
						sg.smin = data[j];
						sg.smin_pos = sg.points.size();
						sg.vertex_pos = j;
					}
					if(sg.smax < data[j]) sg.smax = data[j];
					sg.points.push_back(data[j]);
				}
				segments.push_back(sg);
			}
			// �������� ������
			counter = 0;
		}
		else
			counter++;
	}
}
bool lessByVertexPos(const Segment& s1, const Segment& s2)
{
	return s1.vertex_pos < s2.vertex_pos;
}
void RecognizeTask::calc_params(std::vector<Segment>& segments)
{
	// ������ � ��������� �������� � ������� �� ���������, �.�. ������
	// ��� ������� �������� ����� ��������� ������� (���� ��� ��������)
	if(segments.size() < 3) return;

	// ��������� ������ � ��������� ��������, ���������� ������������� �� ���-�� min
	segments.erase(segments.begin());
	segments.erase(segments.end()-1); 
	
	// ������� ������ ������� ��������
	std::sort(segments.begin(), segments.end());

#ifdef LOG_
	std::ofstream out("segments_sorted.log", std::ios::out);
	if(out.is_open())
	{
		print_segments(segments, out);
		out.close();
	}
#endif

	// ��������� �������� �� ������� � ������
	std::vector<Segment> top_segs;
	std::vector<Segment> bot_segs;
	double delta = 0;
	std::vector<Segment>* ptr = &top_segs;
	for(size_t i = 0; i < segments.size(); i++)
	{
		if(i > 0) delta = fabs(segments[i].smin - segments[i-1].smin);
		if(delta >= 30) ptr = &bot_segs;
		ptr->push_back(segments[i]);
	}

	// ���������� ������� ��������, ����� ��������� ��� � ��������������� ���� �������
	std::vector<double> segment_steps;
	std::sort(top_segs.begin(), top_segs.end(), lessByVertexPos);
	for(size_t i = 1; i < top_segs.size(); i++)
		segment_steps.push_back(Par.laser_len_scale * (top_segs[i].vertex_pos - top_segs[i-1].vertex_pos));

	double sum_steps = 0;
	for(size_t i = 0; i < segment_steps.size(); i++)
		sum_steps += segment_steps[i];
	m_step = sum_steps / segment_steps.size();

	// ��������������� ���� ������� - ������������ ���������� �� �������� ����
	m_step_irregular = 0;
	for(size_t i = 0; i < segment_steps.size(); i++)
	{
		double delta = fabs(segment_steps[i] - m_step);
		if(m_step_irregular < delta)
			m_step_irregular = delta;
	}

	// ������� ������� �������
	double outer_diam = 0;
	for(size_t i = 0; i < top_segs.size(); i++)
	{
		outer_diam += calc_diam(top_segs[i].smin);
		//top_segs[i].diam = calc_diam(top_segs[i].smin);
	}
	outer_diam /= top_segs.size();
	outer_diam *=2; 

	double avg_smin = 0, avg_smax = 0;
	for(size_t i = 0; i < top_segs.size(); i++)
		avg_smin += top_segs[i].smin;
	avg_smin /= top_segs.size();

	for(size_t i = 0; i < bot_segs.size(); i++)
		avg_smax += bot_segs[i].smin;
	avg_smax /= bot_segs.size();

	m_stick_diam = outer_diam - (avg_smax - avg_smin);
	m_outer_diam = outer_diam;
	m_inner_diam = outer_diam - 2*m_stick_diam;
}
/*double RecognizeTask::getStepIrregular(double scheme_step)
{
	double result = 0;
	for(size_t i = 0; i < segment_steps.size(); i++)
	{
		double delta = fabs(segment_steps[i] - scheme_step);
		if(result < delta)
			result = delta;
	}
	return result;
}*/
double RecognizeTask::dist(const Point& A, const Point& B)
{
	double x = A.x - B.x;
	double y = A.y - B.y;
	return sqrt( x*x + y*y );
}
double RecognizeTask::calc_diam(double h)
{
#ifdef LOG_
	std::ofstream out("calc_diam.log", std::ios::out);
#endif
	double hh = Par.laser_dest_roll + Par.roll_radius; // ���������� �� ������ �����
	Point A(0, hh);
	Point B(105, hh);
	Point C(52.5, h);
	Point X = C;
	
	double last = dist(X, A) - Par.roll_radius;
	double R1, R2, delta;

	for(;;)
	{
		X.y += 0.1;
		R1 = dist(X, A) - Par.roll_radius;
		R2 = dist(X, C);
		delta = fabs(R1 - R2);
#ifdef LOG_
		out << "R1 = " << R1 << "; R2 = " << R2 << "; d = " << delta << std::endl;
#endif
		if(delta < last)
			last = delta;
		else
			break;
	}
#ifdef LOG_
	out.close();
#endif

	return R2;
}

void SpringCalibration::protocolButtonPressed()
{
	ProtocolDialog p;
	if(p.exec() == QDialog::Accepted)
	{
		m_protocol->m_operator = p.m_operator;
		m_protocol->m_master = p.m_master;
		// ��������� ��������
		m_protocol->writeProtocol(m_spring_info);
		//m_protocol->printProtocol();
	}
}

