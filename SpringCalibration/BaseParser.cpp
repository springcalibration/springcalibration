#include "BaseParser.h"

using namespace std;

string ConfigParser::ConfigParseError::m_action = "";

void ConfigParser::parseConfig(std::string text)
{
	m_text = text + '\n';
	m_cursor = m_text.begin();
	
	nextToken();
	while(match(m_token, ttLeftSquareBrace))
	{
		act("���������� ������ ����������");
		check(nextToken(), ttId);
		string group = m_token.text;
		while(match(nextToken(), ttNamespace)){
			group += m_token.text;
			check(nextToken(), ttId);
			group += m_token.text;
		}
		check(m_token, ttRightSquareBrace);
		act("������ ������ ���������� " + group);
		nextToken();
		while(!match(m_token, ttLeftSquareBrace) && !match(m_token, ttEof))
		{
			parseConfigParameter(group);
			nextToken();
		}
	}
	act("������ ����������������� ����� ��������, ��������� ����� �����");
	check(m_token, ttEof);
}
ConfigParser::Token ConfigParser::nextToken()
{
	skipSpaces();
	if(isEof()) return (m_token = Token(ttEof));
	skipComments();
	if(isEof()) return (m_token = Token(ttEof));
	if(isLetter(*m_cursor))
	{
		string id;
		do{
			id += *m_cursor;
			m_cursor++;
		}while(isLetter(*m_cursor) || isDigit(*m_cursor));
		return m_token = Token(ttId, id);
	}
	int sign = 1;
	if(isSign(*m_cursor))
	{
		sign = ((*m_cursor == '-') ? -1 : 1);
		m_cursor++;
	}
	//������������ ����� ����� ��� ����� � ����������, ������������ � ����������������� ��������
	//������������ ������������� �������������� ����� (����������� - �����)
	if(isDigit(*m_cursor))
	{
		string num;
		if(*m_cursor == '0'){
			if(*++m_cursor == 'x'){
				num = "0x";
				if(!isHexDigit(*++m_cursor)) return m_token = Token(ttError);
				do{
					num += *m_cursor;
				}while(isHexDigit(*++m_cursor));
				return m_token = Token(ttNumber, num);
			}else
				m_cursor--;
		}
		//������������ � ���������� �������, �������������� �����
		bool isFloat = false;
		do{
			if(*m_cursor == '.')
				if(!isFloat) isFloat = true;
				else throw ConfigParseError("����������� ������ ���� ���������� ����� � �������������� �����", m_line);
			num += *m_cursor;
			m_cursor++;
		}while(isDigit(*m_cursor) || *m_cursor == '.');
		if(sign == -1) num = "-" + num;
		if(isFloat) return m_token = Token(ttFloatNumber, num);
		return m_token = Token(ttNumber, num);
	}
	if(isBrace(*m_cursor))
	{
		if(*m_cursor == '(') m_token = Token(ttLeftBrace, std::string("("));
		if(*m_cursor == ')') m_token = Token(ttRightBrace, std::string(")"));
		if(*m_cursor == '[') m_token = Token(ttLeftSquareBrace, std::string("["));
		if(*m_cursor == ']') m_token = Token(ttRightSquareBrace, std::string("]"));
		m_cursor++;
		return m_token;
	}
	if(isDelim(*m_cursor))
	{
		if(*m_cursor == ':' && *++m_cursor == ':') m_token = Token(ttNamespace, std::string("::"));
		else if(*m_cursor == '=') m_token = Token(ttEqual, std::string("="));
		else throw ConfigParseError("������������ �����������", m_line);
		m_cursor++;
		return m_token;
	}
	//��������� ��������
	if(*m_cursor == '"')
	{
		string str;
		while(*++m_cursor != '"')
			str += *m_cursor;
		m_cursor++;
		return m_token = Token(ttString, str);
	}
	return Token(ttError);
}
std::string ConfigParser::tokenStream(std::string text)
{
	stringstream s;
	m_text = text + '\n';
	m_cursor = m_text.begin();
	while(!isEof())
		s << nextToken().text << endl;
	return s.str();
}
void ConfigParser::check(ConfigParser::Token token, ConfigParser::TokenType type)
{
	if(token.type != type) 
		throw ConfigParseError("������� " + token.text + " �� ������������� ���������� ����", m_line);
}
void ConfigParser::skipSpaces()
{
	while(!isEof() && isSpace(*m_cursor)){
		if(*m_cursor == '\n') m_line++;
		m_cursor++;
	}
}
void ConfigParser::skipComments()
{
	while(*m_cursor == '/')
	{
		if(!iterate()) throw ConfigParseError("����������� ����� �����", m_line);
		if(*m_cursor == '*'){
			while(iterate()){
				if(*m_cursor == '\n') m_line++, iterate();
				if(*m_cursor == '*'){
					if(!iterate()) throw ConfigParseError("����������� ����� �����", m_line);
					if(*m_cursor == '/') break;
				}
			}
		}else if(*m_cursor == '/'){
			while(iterate() && (*m_cursor != '\n'));
			m_line++;
		}else
			throw ConfigParseError("������ ��� ������� ����������������� �����: ������������ ������ /", m_line);
		iterate();
		skipSpaces();
		if(isEof()) return;
	}
}
bool ConfigParser::iterate()
{
	if(m_cursor == m_text.end()) return false;
	m_cursor++;
	return true;
}
std::string ConfigParser::ConfigParseError::info() const
{
	std::stringstream s;
	s << "Line " << m_line << ": " << std::endl;
	s << "\t" << m_action << std::endl;
	s << "\t" << what();
	return s.str();
}
