#ifndef LTR_MODULES_H_
#define LTR_MODULES_H_

#include "ltrapidefine.h"
#include "ltrapitypes.h"
#include "ltrapi.h"
#include "ltr11api.h"
#include "ltr41api.h"
#include "ltr42api.h"
#include "ltr212api.h"

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
//#include <QtGui/QtGui>
#include <QObject>
#include <QString>
#include <QTextEdit>
#include <QMutex>

// ������������� ��������� (������� �� ������)
const int Ltr41_slot = 1;
const int Ltr42_slot = 2;
const int Ltr11_slot = 3;
const int Ltr212_slot = 4;

//const WORD Ltr42_forward = 0x5;// 101
//const WORD Ltr42_back    = 0x7;// 111

//const WORD Ltr42_on = 0x8;   // 01000
//const WORD Ltr42_off = 0x18; // 11000


extern volatile bool Ltr41Run, Ltr11Run, Ltr212Run;

class Ltr41Module: public QObject{
	Q_OBJECT
public:
	Ltr41Module(QObject* parent = 0, unsigned int slot = 3);
	~Ltr41Module(){
		if(isOpen()) close();
	}
	bool open();
	inline bool isOpen();
	inline void close();
	PTLTR41 getHandler(){return &m_ltr;}
	WORD readPort();
	void readPort(WORD* InputData);
	inline QString getErrorString(INT error);
	void setTerminal(QTextEdit* terminal){m_terminal = terminal;}
	void processData(DWORD* src, WORD* dest, DWORD size);
private:
	const unsigned int m_slot;
	TLTR41 m_ltr;
	INT m_error;
	QTextEdit* m_terminal;

	void print(const QString& text);
};

class Ltr42Module: public QObject{
	Q_OBJECT
public:
	Ltr42Module(QObject* parent = 0, unsigned int slot = 4);
	~Ltr42Module(){
		if(isOpen()) close();
	}
	bool open();
	inline bool isOpen();
	inline void close();
	PTLTR42 getHandler(){return &m_ltr;}
	inline QString getErrorString(INT error);
	void setTerminal(QTextEdit* terminal){m_terminal = terminal;}
	bool writePort(WORD data);
private:
	const unsigned int m_slot;
	TLTR42 m_ltr;
	INT m_error;
	QTextEdit* m_terminal;

	void print(const QString& text);
};

class Ltr11Module: public QObject{
	Q_OBJECT
public:
	Ltr11Module(QObject* parent = 0, unsigned int slot = 2);
	~Ltr11Module(){
		if(isOpen()) close();
	}
	bool open();
	inline bool isOpen();
	inline void close();
	PTLTR11 getHandler(){return &m_ltr;}
	inline QString getErrorString(INT error);
	INT setConfig(BYTE canal, INT divider, INT prescaler);
	double readVoltage();
	void readData(double data[], int size, size_t timeout_ = 1000);
private:
	const unsigned int m_slot;
	TLTR11 m_ltr;
	INT m_error;
	bool m_isOpen;
};

class Ltr212Module: public QObject{
	Q_OBJECT
public:
	Ltr212Module(QObject* parent = 0, unsigned int slot = 1);
	~Ltr212Module(){
		if(isOpen()) close();
		if(m_log.is_open()) m_log.close();
	}
	bool open();
	inline bool isOpen();
	inline void close();
	PTLTR212 getHandler(){return &m_ltr;}
	inline QString getErrorString(INT error);

private:
	const unsigned int m_slot;
	TLTR212 m_ltr;
	INT m_error;
	std::ofstream m_log;
	inline void print(const QString& text);
	inline void print(const char* text);
};


#endif //LTR_MODULES_H_