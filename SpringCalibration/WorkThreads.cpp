#include "WorkThreads.h"

static CC const& Par = config.getCommon();

extern QMutex Ltr41Mod, Ltr11Mod, Ltr42Mod, Ltr212Mod;

volatile static bool RunTest = false;//������� ���������� ���������� ���������

volatile static bool Run212Test = false; // ������ ?

// ����� ��� ������ ������ � Ltr11 (��������� �������������� ���������� �������)
const size_t data_sz = 6000;
double data[6000] = {0.0};
int data_ptr = 0;

// ������� ��������� ���������, ltr41
volatile WORD code_status = 0x4; 
QMutex mtStatus;

// ������� �������� �� ������� � ������ ������� ��� ���������
QMutex mtSpringPress;
volatile double spring_pressure = 0;
volatile double spring_height = 0;

///////////////////////////////////////////////////////////////////////////////
// ���������� ������ ������� � ltr41 - ������� ��������� ���������

void StatusReader::run()
{
	std::ofstream out("ltr41_data.log", std::ios::out);
	for(;;)
	{
		mtStatus.lock();
		code_status = m_ltr41->readPort();
		out << code_status << std::endl;
		mtStatus.unlock();
		emit updateStatus();
	}
	out.close();
}

///////////////////////////////////////////////////////////////////////////////
// �������� ���������� ��������� � ����������

void CheckTask::run()
{
	// ��������� ��������
	m_ltr42->writePort(0x3); // INP5 && INP6

	// ����� ���������� �������� �� ltr41 ���������� 0xC
	WORD status = 0;
	for(;status != 0xC;) // ������� ��������� OUT5 - 4-� ������ ltr41
	{
		mtStatus.lock();
		status = code_status;
		mtStatus.unlock();
	}

	emit msg(tr("���� �������� ���������� ���������"));
	// �������� ����������� 
	for(;status == 0xC;)
	{
		mtStatus.lock();
		status = code_status;
		mtStatus.unlock();
	}

	// ��������� ��������
	m_ltr42->writePort(0); // ��������� INP5 � INP6

	if(status == 0x4)
		emit msg(tr("�������� � ������ �����"));
	else
		emit msg(tr("��� �������� �������� �������� - �������� �� �����!"));

	emit complete();
}

///////////////////////////////////////////////////////////////////////////////
// �������� �������� �� ������� � ����� ������� ��� ���������

void PressureControl::run()
{
	if(!RunTest) return;
	Run212Test = true;
	// ��������� ���� ������, �������� ������������
	std::ofstream out;
	if(Par.debug) out.open("PressureControl.log", std::ios::out);
	PTLTR212 pltr = m_ltr212->getHandler();
	if(LTR212_Start(pltr) != LTR_OK){
		if(out) out << "������ ��� ������� ����� ������." << std::endl;
		Run212Test = false;
		//return;
	}
	// ������ ���� �������� ����� ���������, ������ ���������� ����� ��������������� ������� - 4 ��-�� � ����� ������
	const DWORD ltr212_sz = 4 * 1; // ������ 1 ����
	DWORD ltr212_src[ltr212_sz];
	double ltr212_data[ltr212_sz];
	DWORD ltr212_size = ltr212_sz;
	DWORD timeout = 500; // ������� 0.5 �������


	//m_ltr11->setConfig(0x1, 116, 64); // 2000 ��, 2-� �����

	if(out) out << "�������� ���� ������" << std::endl;

	double pressure;
	double height;
	// ����������� ������� ����� ������
	while(RunTest && Run212Test)
	{
		if(LTR212_Recv(pltr, ltr212_src, NULL, ltr212_size, timeout) != ltr212_size){
			if(out) out << "������ ��� ����� ������." << std::endl;
			//Run212Test = false;
		}
		DWORD size = ltr212_size;
		// ��������� ����� ������ � ������
		if(LTR212_ProcessData(pltr, ltr212_src, ltr212_data, &size, TRUE) != LTR_OK){
			if(out) out << "������ ��� ��������� ��������� ������." << std::endl;
			//Run212Test = false;
		}
		// ������ ltr212_data ������ ��������� ����� ������ ������
		if(out) {
			out << "src_size = " << ltr212_size << ", data_size = " << size << std::endl;
			for(DWORD i = 0; i < size; i++)
				out << ltr212_data[i] << std::endl;
			out << std::endl;
		}
		if(ltr212_size/2 != size)
		{
			if(out) out << "������ ����� ������ �������� � �������� �� ���������" << std::endl;
			//Run212Test = false;
		}

		pressure = Par.press_scale_1*(ltr212_data[0] - Par.press_offset_1) + Par.press_scale_2*(ltr212_data[1] - Par.press_offset_2);
		//to do: ������ ������ ������� � ��������� �����
		//height = Par.ultra_scale*(m_ltr11->readVoltage() - Par.ultra_offset);

		mtSpringPress.lock();
		spring_pressure = pressure;
		//spring_height = height;
		mtSpringPress.unlock();

		emit update();
	}

	// ������������� ���� ������
	if(out) out << "��������� ���� ������" << std::endl;
	if(LTR212_Stop(pltr) != LTR_OK){
		out << "������ ��� �������� ����� ������." << std::endl;
	}

	if(out.is_open()) out.close();
}
///////////////////////////////////////////////////////////////////////////////
// ��������� �� ���������� ����������
void DeformationTask::run()
{
	emit msg(tr("���� ��������� ��� ������� ���������"));

	m_exam_press = 2000; // ���
	std::ofstream out("DeformTask.log", std::ios::out);
	// �������� ���� ������ � �������� �� �������
	m_pc.start();
	WORD status = 0;
	double pressure;
	double height;
	// ��������� 2 �������� ������� ��������
	int iteration = 2;
	m_ltr42->writePort(0xA); // INP8 && INP6
	while(iteration-- > 0 && RunTest)
	{
		pressure = height = 0;

		// ������� ��������� �� ltr41 ����� 0x5 (���������� �� ������� ������ ������)
		do
		{
			mtStatus.lock();
			status = code_status;
			mtStatus.unlock();
		}while(status != 0x5 && RunTest);

		if(!RunTest){ m_ltr42->writePort(0); return; }
		
		out << "���� ��������� �� 0.95 ��������" << std::endl;
		// ���������� ��������� ������� �������� �������� � �����
		// ��� ���������� �������� 0.95 ������� �������� �� ltr42 �������� ������� 0x1A (���������� ��������).
		for(pressure = 0; pressure < 0.95*m_exam_press && RunTest; )
		{
			mtSpringPress.lock();
			pressure = spring_pressure;
			height = spring_height;
			mtSpringPress.unlock();
			// to do: ���������, ��� height <= height_min
			out << pressure << " " << height << std::endl;
		}
		if(!RunTest){ m_ltr42->writePort(0); return; }
		m_ltr42->writePort(0x1A); 

		out << "���� ������ ��������" << std::endl;
		//��� ���������� �������� ������� �������� �� ltr42 �������� ������� 0xE
		for(; pressure < m_exam_press && RunTest; )
		{
			mtSpringPress.lock();
			pressure = spring_pressure;
			height = spring_height;
			mtSpringPress.unlock();
			// to do: ���������, ��� height <= height_min
			out << pressure << " " << height << std::endl;
		}
		if(!RunTest){ m_ltr42->writePort(0); return; }
		m_ltr42->writePort(0xE); // ������ �����������, ��� �������� ����������

		m_ltr11->setConfig(0x1, 116, 64); // 2000 ��, 2-� �����
		// ������ ������� ��� ������� ���������
		m_exam_height = Par.ultra_scale*(m_ltr11->readVoltage() - Par.ultra_offset) + Par.ultra_const; 

		// ��������� ������� INP8, INP7 (INP6 ���������) - ������������ �������� �����, ���� �� �� �� ������� �����
		m_ltr42->writePort(0x2);

		out << "������ ��� ������� ��������� = " << m_exam_height << std::endl;
		out << "���� �������� �� 0.1 ��������" << std::endl;
		// ��� �������� ������ �� 0.1 ������� ��������, ��������� ��������� ��� ��������� - �������� �� ��������� ����
		// �� ltr42 �������� ������� 0x5.
		for(; pressure > 0.1*m_exam_press && RunTest; )
		{
			mtSpringPress.lock();
			pressure = spring_pressure;
			mtSpringPress.unlock();
			out << pressure << std::endl;
		}
		if(!RunTest){ m_ltr42->writePort(0); return; }

		m_ltr42->writePort(0xA); // INP8 && INP6
		
		out << "���� ��������" << std::endl << std::endl;
	}

	m_ltr42->writePort(0);
	// ��������� �����
	Run212Test = false; 
	m_pc.wait();
	out.close();

	// ����, ����� �������� ����� �������� � ��������� ���������
	// � ���������� ���������� ��������� ������� ��������� �� ltr41 ����� 0x4
	do
	{
		mtStatus.lock();
		status = code_status;
		mtStatus.unlock();
	}while(status != 0x4 && RunTest);

	emit msg(tr("��������� ��� ��������� ���������"));
	emit complete(); // ������ ���� ��������� �������� ���������� �������
}

///////////////////////////////////////////////////////////////////////////////
// ��������� �� ������ �������

void ArrowTask::run()
{
	m_arrow_press = 1000; // ���
	emit msg(tr("���� ��������� ��� ����������� ���������"));
	std::ofstream out("ArrowTask.log", std::ios::out);
	m_ltr42->writePort(0x2);
	// ���� ��������� ����� 0x5 (�������� ����������� ������������� ������)
	WORD status = 0;
	do
	{
		mtStatus.lock();
		status = code_status;
		mtStatus.unlock();
	}while(status != 0x5 && RunTest);

	if(!RunTest){ m_ltr42->writePort(0); return; }

	// �������� ���� ������ � �������� �� �������
	m_pc.start();
	
	double pressure;
	double height;

	out << "���� ��������� �� 0.95 ��������" << std::endl;
	// ���������� ��������� ������� �������� �������� � �����
	// ��� ���������� �������� 0.95 ����������� �������� �� ltr42 �������� ������� 0x12 (���������� ��������).
	for(pressure = 0; pressure < 0.95*m_arrow_press && RunTest; )
	{
		mtSpringPress.lock();
		pressure = spring_pressure;
		height = spring_height;
		mtSpringPress.unlock();
		// to do: ���������, ��� height <= height_min
		out << pressure << " " << height << std::endl;
	}
	if(!RunTest){ m_ltr42->writePort(0); return; }
	m_ltr42->writePort(0x12);

	out << "���� ������ ��������" << std::endl;
	//��� ���������� �������� ����������� �������� �� ltr42 �������� ������� 0x6
	for(; pressure < m_arrow_press && RunTest; )
	{
		mtSpringPress.lock();
		pressure = spring_pressure;
		height = spring_height;
		mtSpringPress.unlock();
		// to do: ���������, ��� height <= height_min
		out << pressure << " " << height << std::endl;
	}
	if(!RunTest){ m_ltr42->writePort(0); return; }
	m_ltr42->writePort(0x6);

	// ��������� ����� ������� ��� ������. ���������
	m_ltr11->setConfig(0x1, 116, 64); // 2000 ��, 2-� �����
	m_arrow_height = Par.ultra_scale*(m_ltr11->readVoltage() - Par.ultra_offset);//height;

	out << "������ ��� ����������� ��������� = " << m_arrow_height << std::endl;
	out << "���� �������� �� 0.1 ��������" << std::endl;
	//��� �������� ������ �� 0.1 ����������� �������� �� ltr42 �������� ������� 0
	for(; pressure > 0.1*m_arrow_press && RunTest; )
	{
		mtSpringPress.lock();
		pressure = spring_pressure;
		mtSpringPress.unlock();
		out << pressure << std::endl;
	}
	// ������ ������� �� ���������� ��������
	m_ltr42->writePort(0x2);
	msleep(50);
	m_ltr42->writePort(0);
	// ��������� �����
	Run212Test = false; // to do: �������� �������!
	m_pc.wait();

	// � ���������� ���������� ��������� ������� ��������� �� ltr41 ����� 0x4
	do
	{
		mtStatus.lock();
		status = code_status;
		mtStatus.unlock();
	}while(status != 0x4 && RunTest);

	out.close();
	emit msg(tr("��������� ��� ����������� ��������� ���������"));
	emit complete();
}

///////////////////////////////////////////////////////////////////////////////
// ��������� �������� ������ - ������������� �������������� ���������� �������

static void print_segments(std::vector<Segment>& segments, std::ostream& out)
{
	for(size_t i = 0; i < segments.size(); i++)
	{
		out << "Segment #" << i+1 << std::endl;
		out << "First pnt = " << segments[i].first() << std::endl;
		out << "Last pnt = " << segments[i].last() << std::endl;
		out << "Len = " << segments[i].len() << std::endl;
		out << "Min = " << segments[i].smin << std::endl;
		out << "Max = " << segments[i].smax << std::endl;
		out << "Diam = " << segments[i].diam << std::endl;
		out << std::endl;
	}
}

///////////////////////////////////////////////////////////////////////////////
// ������������� ���� �������
// ���� ������� = 96.5 ��
// ���� ������� = 230 ��

void RecognizeTask::run()
{
	src.clear();
	data.clear();
	// ��������� ��������� (������ �������� ��)
	m_ltr42->writePort(0x1); // INP5 && !INP6

	// ���� ��������� ���� 0x6, ����� ��������� ����� ������ ������ � ������
	emit msg(tr("���� ������ � ������ ���������..."));
	WORD status = 0;
	do
	{
		mtStatus.lock();
		status = code_status;
		mtStatus.unlock();
	}while(status == 0x4 && RunTest); // ���� ��������� ������� �� OUT3

	// ������ � ltr41 ������ ���� ����� 0x6 - OUT3 = 1
	if(status != 0x6)
	{
		emit msg(tr("���-�� ����� �� ���!"));
		m_ltr42->writePort(0);
		return;
	}

	emit msg(tr("���� ��������� ���������� �������..."));
	
	m_ltr11->setConfig(0x0, 23999, 1); // 625 ��
	//m_ltr11->setConfig(0x0, 116, 64); // 2000 ��
	const int data_sz = 625;
	double data[data_sz]; // �������� �������� - ���������������
	int size = data_sz;
	
	// ������ ������� � ltr11 � ��������� �� � ������� ��� ����������� ���������
	// 1.9V - ������� (0 ��)
	// 9.53V - �������� (500 ��)
	// 7.63V - ���������� ��������
	// 500 / 7.63 = 65.53 - ���������
	// const double scale = 65.53; // ������������� (CbrCoef.Gain) � ���������� ������������
	while(status == 0x6 && RunTest)
	{
		// ������ ���� ������ �� 625 ��������, ������� - 1.5 �������
		m_ltr11->readData(data, size, 1500); 

		for(int i = 0; i < size; i++)
			src.push_back(Par.laser_scale*(data[i] - Par.laser_offset));
			//src.push_back(scale * (data[i] - 1.9));

		// ��������� ������
		mtStatus.lock();
		status = code_status;
		mtStatus.unlock();
	}

	// ��������� ���������
	m_ltr42->writePort(0);
	emit msg(tr("��������� ���������, ������������� �������..."));

	// for debug
	std::ofstream out("ltr11_data.log", std::ios::out);
	if(out.is_open())
	{
		for(size_t i = 0; i < src.size(); i++)
			out << src[i] << " " << src[i]/Par.laser_scale << std::endl;
		out.close();
	}

	// �������� ������������� ���������� �������
	if(RunTest)
		recognizeSpring(src);

	emit msg(tr("������������� ���������"));
	emit complete();
}

// ��� ������������� ������ ���� ������:
// - ������ � ��������� ��������� �;
// - ������� ������ stock_d;
// - ���������� ������� d;
// - �������� ������� D;
// D = 2*stock_d + d

bool RecognizeTask::recognizeSpring(const std::vector<double>& src)
{
	preprocess(src, data);
	// ����� ������� � ��������� ��������� - ����� ����� �� ����� ����
	m_height = 0.05762 * data.size(); //to do: 372

	/* ��������� ����-�� �� ��� �������-��: ������� � ������ �����.
	** ��� ������� �������� ����� ����������: 
	   - ������ ���������� �����,
	   - ��������� ���������� �����,
	   - ������ �����,
	   - ��������� ����� ������ � ��������� �������,
	   - ��������� ������� ������, 
	** ��� ������� ������� ����� ���������� ������ �����.
	** ��� ������ ���� �������� ������� ������ ���������� ���������� ����� ����.
	*/
	std::vector<Segment> segments;
	split_segments(data, segments);

	// debug
	std::ofstream out("segments.log", std::ios::out);
	if(out.is_open())
	{
		print_segments(segments, out);
		out.close();
	}

	/* ��� ������� ����� ���������� �������
	** ��������� ������� ������� - ������� �����
	** ��������� ������� � ���������� �������� �������
	*/
	calc_params(segments);

	// debug
	std::ofstream sout("spring.log", std::ios::out);
	if(sout.is_open())
	{
		sout << "height = " << m_height << std::endl;
		sout << "s_diam = " << m_stick_diam << std::endl;
		sout << "i_diam = " << m_inner_diam << std::endl;
		sout << "o_diam = " << m_outer_diam << std::endl;
		sout.close();
	}

	return true;
}

// ������������� ����� ������, ��������� � ltr11
// - �������� ��������� �������� �� ����������
// - ��������� ������ � ������ � � ����� ������������������
void RecognizeTask::preprocess(const std::vector<double>& src, std::vector<double>& dest)
{
	const double e_code = 500;
	std::vector<double> temp;
	for(size_t i = 0; i < src.size(); i++)
	{
		temp.push_back( (src[i] >= m_min_dest && src[i] <= m_max_dest ? src[i] : e_code) );
	}
	std::vector<double>::iterator it_b = temp.begin(), it_e = temp.end();
	while(*it_b == e_code && it_b != it_e) it_b++;
	while(*(it_e - 1) == e_code && it_b != it_e) it_e--;
	// ��� ������
	if(it_b == it_e)
		return;
	// ������� � dest ������
	dest.insert(dest.begin(), it_b, it_e);

	// debug: ������� ������� � ����
	std::ofstream out1("origin_data.log", std::ios::out);
	if(out1.is_open())
	{
		for(size_t i = 0; i < src.size(); i++)
			out1 << src[i] << std::endl;
		out1.close();
	}
	std::ofstream out("processed_data.log", std::ios::out);
	if(out.is_open())
	{
		for(size_t i = 0; i < dest.size(); i++)
			out << dest[i] << std::endl;
		out.close();
	}
}

void RecognizeTask::split_segments(std::vector<double>& data, std::vector<Segment>& segments)
{
	// ������������� �������, ��������� �� ����� ��� �� 100 ����. ����� = 5��
	// ������� ������ ������������: �������/������
	int counter = 0;
	const double e_code = 500;
	double delta = 0;

	for(size_t i = 0; i < data.size(); i++)
	{
		if(i > 0) delta = fabs(data[i] - data[i-1]);

		if(data[i] == e_code || delta >= 30) // 30 ��
		{
			if(counter >= 100)
			{
				// ��������� ����� � ��������� �������
				Segment sg;
				sg.smin = data[i-1];
				sg.smax = data[i-1];
				for(size_t j = i - counter + 1; j < i; j++)
				{
					if(sg.smin > data[j])
					{
						sg.smin = data[j];
						sg.smin_pos = sg.points.size();
					}
					if(sg.smax < data[j]) sg.smax = data[j];
					sg.points.push_back(data[j]);
				}
				segments.push_back(sg);
			}
			// �������� ������
			counter = 0;
		}
		else
			counter++;
	}
}
void RecognizeTask::calc_params(std::vector<Segment>& segments)
{
	// ������ � ��������� �������� � ������� �� ���������, �.�. ������
	// ��� ������� �������� ����� ��������� ������� (���� ��� ��������)
	if(segments.size() < 3) return;

	// ��������� ������ � ��������� ��������, ���������� ������������� �� ���-�� min
	segments.erase(segments.begin());
	segments.erase(segments.end()-1); 
	
	// ������� ������ ������� ��������
	std::sort(segments.begin(), segments.end());

	// debug
	std::ofstream out("segments_sorted.log", std::ios::out);
	if(out.is_open())
	{
		print_segments(segments, out);
		out.close();
	}

	// ��������� �������� �� ������� � ������
	std::vector<Segment> top_segs;
	std::vector<Segment> bot_segs;
	double delta = 0;
	std::vector<Segment>* ptr = &top_segs;
	for(size_t i = 0; i < segments.size(); i++)
	{
		if(i > 0) delta = fabs(segments[i].smin - segments[i-1].smin);
		if(delta >= 30) ptr = &bot_segs;
		ptr->push_back(segments[i]);
	}

	// ������� ������� �������
	double outer_diam = 0;
	for(size_t i = 0; i < top_segs.size(); i++)
	{
		outer_diam += calc_diam(top_segs[i].smin);
		//top_segs[i].diam = calc_diam(top_segs[i].smin);
	}
	outer_diam /= top_segs.size();
	outer_diam *=2; 

	double avg_smin = 0, avg_smax = 0;
	for(size_t i = 0; i < top_segs.size(); i++)
		avg_smin += top_segs[i].smin;
	avg_smin /= top_segs.size();

	for(size_t i = 0; i < bot_segs.size(); i++)
		avg_smax += bot_segs[i].smin;
	avg_smax /= bot_segs.size();

	m_stick_diam = outer_diam - (avg_smax - avg_smin);
	m_outer_diam = outer_diam;
	m_inner_diam = outer_diam - 2*m_stick_diam;
}
double RecognizeTask::dist(const Point& A, const Point& B)
{
	double x = A.x - B.x;
	double y = A.y - B.y;
	return sqrt( x*x + y*y );
}
double RecognizeTask::calc_diam(double h)
{
	std::ofstream out("calc_diam.log", std::ios::out);
	double hh = Par.laser_dest_roll + Par.roll_radius; // ���������� �� ������ �����
	Point A(0, hh);
	Point B(105, hh);
	Point C(52.5, h);
	Point X = C;
	
	double last = dist(X, A) - Par.roll_radius;
	double R1, R2, delta;

	for(;;)
	{
		X.y += 0.1;
		R1 = dist(X, A) - Par.roll_radius;
		R2 = dist(X, C);
		delta = fabs(R1 - R2);
		out << "R1 = " << R1 << "; R2 = " << R2 << "; d = " << delta << std::endl;
		if(delta < last)
			last = delta;
		else
			break;
	}
	out.close();

	return R2;
}

///////////////////////////////////////////////////////////////////////////////////////
// for debug

bool RecognizeTask::readData(const char* filename, std::vector<double>& vec)
{
	//std::ofstream out("ttt", std::ios::out);

	std::ifstream in(filename, std::ios::in);
	if(in.is_open())
	{
		double value;
		while(in >> value)
		{
			//out << value << std::endl;
			vec.push_back(value);
		}
		//out.close();
		in.close();
		return true;
	}
	else
		std::cerr << "Can't open file " << filename << std::endl;
	return false;
}
// ��������� ������ data �� csv-�����
// ������������ ��� ������������
bool RecognizeTask::readCSV(const char* filename, double data[], size_t& size)
{
	std::ifstream in(filename, std::ios::in);
	if(in.is_open())
	{
		size = 0;
		float time, value;
		char buf[128] = {0};
		int cnt = 0;
		while(in.getline(buf, 128).good())
		{
			sscanf(buf, "%f;%f", &time, &value);
			//sscanf(buf, "%f", &time, &value);
			if(++cnt%2 == 0)
			{
				data[size] = value;
				size++;
			}
		}
		in.close();


		//std::ofstream out("data_out.txt", std::ios::out);
		//out << "size = " << size << std::endl;
		//for(size_t i = 0; i < size; i++)
		//	out << data[i] << std::endl;
		//out.close();

		return true;
	}
	else
		std::cerr << "Can't open file " << filename << std::endl;
	return false;
}