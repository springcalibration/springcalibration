#include "springcalibration.h"
#include <QtGui/QApplication>

// ��������� ���������� � params.cpp
extern SpringCalibrationConfig config;

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	QTextCodec::setCodecForTr(QTextCodec::codecForName("Windows-1251"));
	SpringCalibration w;
	// ������ ����������������� �����
	std::ifstream in("config");
	if(in){
		std::stringstream s;
		s << in.rdbuf();
		in.close();
		try{
			//SpringCalibrationConfig config;
			config.parseConfig(s.str());
			//config.printValue();
		}
		catch(const ConfigParser::ConfigParseError& e){
			QMessageBox::critical(0, QObject::tr("������"), QObject::tr(e.info().c_str()));
			QCoreApplication::quit();
		}
	}
	w.showFullScreen();
	return a.exec();
}
