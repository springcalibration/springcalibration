#ifndef SPRINGCALIBRATION_H
#define SPRINGCALIBRATION_H

//#include <QtGui/QtGui>
#include "widgets.h"
#include "LtrModules.h"
#include "database.h"
#include "activescheme.h"
//#include "WorkThreads.h"

//debug
#include <ctime>
#include <cmath>
#include <cstdlib>
#include <vector>
#include <algorithm>


///////////////////////////////////////////////////////////////////////////////
// ���������

inline double cpuTime(){return (double)clock()/CLOCKS_PER_SEC;}

extern volatile WORD code_status; 
extern double data[];
extern const size_t data_sz;
extern int data_ptr;

// ������ ������ � ltr41, ���������� �������� � ����� ������
class StatusReader: public QThread{
	Q_OBJECT
public:
	StatusReader(Ltr41Module *ltr41, QObject* parent = 0)
		: QThread(parent), m_ltr41(ltr41){}
	void run();
private:
	Ltr41Module *m_ltr41;
signals:
	void updateStatus();
};

// �������� ���������, ����������� ����� ����� �����������
class CheckTask: public QThread {
	Q_OBJECT
public:
	CheckTask(Ltr42Module* ltr42, QObject* parent = 0)
		: QThread(parent), m_ltr42(ltr42){}
	void run();
private:
	Ltr42Module* m_ltr42;
signals:
	void complete();
	void msg(const QString&);

	void ltr42code(unsigned);
};


// �������� �������� � ����� �������
class PressureControl: public QThread
{
	Q_OBJECT
public:
	PressureControl(Ltr11Module *ltr11, Ltr212Module *ltr212, QObject* parent = 0)
		: QThread(parent), m_ltr11(ltr11), m_ltr212(ltr212){}
	void run();
private:
	Ltr11Module *m_ltr11;
	Ltr212Module *m_ltr212;
signals:
	void update();
	void msg(const QString&);
};

// ��������� �� ���������� ����������
class DeformationTask: public QThread
{
	Q_OBJECT
public:
	DeformationTask(Ltr11Module *ltr11, Ltr212Module *ltr212, Ltr42Module *ltr42, QObject* parent = 0)
		: QThread(parent), m_ltr42(ltr42), m_pc(ltr11, ltr212), m_exam_height(0), m_exam_press(10), m_ltr11(ltr11){}
	void run();
	double m_exam_height; // ������ ��� ������� ��������� (����������)
	double m_exam_press; // �������� ������� �������� (�� �������)
private:
	Ltr42Module *m_ltr42;
	Ltr11Module *m_ltr11;
	PressureControl m_pc;
signals:
	void complete();
	void msg(const QString&);

	void ltr42code(unsigned);
	void ltr11value(double);
	void ltr212value(double);
};

class ArrowTask: public QThread
{
	Q_OBJECT
public:
	ArrowTask(Ltr11Module *ltr11, Ltr212Module *ltr212, Ltr42Module *ltr42, QObject* parent = 0)
		: QThread(parent), m_ltr42(ltr42), m_pc(ltr11, ltr212), m_arrow_height(0), m_arrow_press(0), m_ltr11(ltr11){}
	void run();
	double m_arrow_height;
	double m_arrow_press;
private:
	Ltr42Module *m_ltr42;
	Ltr11Module *m_ltr11;
	PressureControl m_pc;
signals:
	void complete();
	void msg(const QString&);

	void ltr42code(unsigned);
	void ltr11value(double);
	void ltr212value(double);
};

///////////////////////////////////////////////////////////////////////////////
// ���������� ���-�� ������� 
struct Spring
{
	Spring()
		: height(0)
		, inner_diam(0)
		, outer_diam(0)
		, stick_diam(0)
		, step(0)
		, step_irregular(0)
		, not_ortogonal(0)
		, exam_height(0)
		, arrow_height(0)
		, height_ex(0)
		, step_ex(0)
		, step_irregular_ex(0)
		, deformation(0)
	{}
	// �������� ���������� ���� ��� ��� ������������� ������� (������ �� ��������)
	double inner_diam;
	double outer_diam;
	double stick_diam;
	// ������, ��� � �����-�� ���� ����� ���������� ����� ���������
	double height;
	double step;
	double step_irregular;
	double not_ortogonal;
	double deformation; // ��������� ����������

	double height_ex;
	double step_ex;
	double step_irregular_ex;
	// ������ ������� ��� ������� � ������. ���������� (����������)
	double exam_height;
	double arrow_height;
	// �� �������
	SpringScheme scheme;
};


struct Segment
{
	Segment(): smin(0), smax(0), smin_pos(0), diam(0){}
	double first() const {return points.front();}
	double last() const {return points.back();}
	double smin;     // ����������� ���������� �� ������ �� �������� (������� ��������)
	size_t smin_pos; // ������� �����, ���������� ������� �������� � ������� points
	double smax;     // ������������ ���������� �� ������ �� ��������
	double diam;     // ������� ������ (�� ����������)
	size_t vertex_pos; // ����� ����� � �������� �������, ������� ���� ������� ������� ��������
	double len() const {return 0.1 * points.size();} // to do: scale
	std::vector<double> points;
	bool operator < (const Segment& other) const {return smin < other.smin;}
};


class RecognizeTask: public QThread {
	Q_OBJECT
public:

	typedef enum {
		rmRecognizeSpring, // ����������� ���� �������, ����� � ��
		rmEstimateSpring   // ��������� ���-��� ������� ����� ��������� �� ���������� ����������
	} Mode;

	RecognizeTask(Ltr11Module *ltr11, Ltr42Module *ltr42, QObject* parent = 0)
		: QThread(parent), m_ltr11(ltr11), m_ltr42(ltr42)
		, m_min_dest(50)
		, m_max_dest(350)
		, m_mode(rmRecognizeSpring)
	{}
	void run();
	
	void setMode(Mode mode) {m_mode = mode;}
	Mode mode() const {return m_mode;}
	const std::vector<double>& point_src() const {return src;}
	const std::vector<double>& point_data() const {return data;}

	//double getStepIrregular(double scheme_step);

	double m_inner_diam;
	double m_outer_diam;
	double m_stick_diam;
	double m_height;
	double m_step;
	double m_step_irregular;
	double m_not_ortogonal;
private:
	Ltr42Module *m_ltr42;
	Ltr11Module *m_ltr11;
	Mode m_mode; 
	const double m_min_dest;
	const double m_max_dest;
	std::vector<double> src;
	std::vector<double> data; // ����� ����� ��������������
	//std::vector<double> segment_steps; // ���� ��� ������� ��������

	// �������� �����
	bool recognizeSpring(const std::vector<double>& src);
	void preprocess(const std::vector<double>& src, std::vector<double>& dest);
	void split_segments(std::vector<double>& data, std::vector<Segment>& segments);
	void calc_params(std::vector<Segment>& segments);
	double calc_diam(double h);
	double dist(const Point& A, const Point& B);

signals:
	void complete();
	void msg(const QString&);

	void ltr42code(unsigned);
	void ltr11value(double);
};
///////////////////////////////////////////////////////////////////////////////

class Protocol: public QObject
{
	Q_OBJECT
public:
	Protocol(QObject* parent = 0);
	~Protocol();

	QString m_operator;
	QString m_master;

	void clear();
	void readNum(){ m_num++; }
public slots:
	void writeProtocol(const Spring& sp); // ��������� �������� � ���� PDF-���������
	void printProtocol(const Spring& sp); // ��������� �������� �� ������
private:
	QString& buildProtocol(const Spring& sp); // c����������� ����� ���������
	int m_num; // ����� ���������
	QString m_text;
};

class SpringCalibration : public QMainWindow
{
	Q_OBJECT
public:
	SpringCalibration(QWidget *parent = 0, Qt::WFlags flags = 0);
	~SpringCalibration();

private:
	typedef enum {
		csNone = 0,
		csStartWait    = 1 << 0, // ����� � ��������� (����������� ���-���, ���� ��� ���������), ����� ��������� ��������
		csSpringMesure = 1 << 1, // �������� ����������� ���-�� �������
		csSpringRecogn = 1 << 2, // ������� ���������� �������������, ���� ������� ������������� �� ����
		csCheckRun     = 1 << 3, // ����������� �������� ����������
		csRecognizeRun = 1 << 4, // �������� ��������� - ����������� ���� �������
		csDeformRun    = 1 << 5, // �������� ��������� - ���������� ����������
		csArrowRun     = 1 << 6, // �������� ��������� - ������ �������
		csError        = 1 << 7	 // ������, ��������� ����
	} ComplexState;

	//ComplexState m_status;
	unsigned m_status;

	bool openModules();
	void closeModules();

	QStatusBar* m_stBar;

	Oscilloscope* m_graph; // ����������� ������� ������� �� ������� (� �������� �������� ������ �� ������������)
	ActiveScheme* m_active_scheme; // ����� ������� � ������ ��� ����������� ���������� ���-���

	// ������������ �������
	TDisplayField *m_engine;
	TDisplayField *m_target;
	TDisplayField *m_scheme;
	TDisplayField *m_type;
	// ��������� �������: �� ������� / �����������
	TDisplayField *m_et_diam, *m_fa_diam; // ������� ������
    TDisplayField *m_et_inner_diam, *m_fa_inner_diam; // ���������� ������� �������
    TDisplayField *m_et_outer_diam, *m_fa_outer_diam; // �������� ������� �������
    TDisplayField *m_et_height, *m_fa_height; // ������ ������� � ��������� ���������
    TDisplayField *m_et_step, *m_fa_step; // ��� �������
    TDisplayField *m_et_step_range, *m_fa_step_range; // ��������������� ���� �������
    TDisplayField *m_et_no_ortogonal, *m_fa_no_ortogonal; // �������������������� ������� ���������� ������� ������������ �� ���
    TDisplayField *m_et_arrow, *m_fa_arrow;// ������ (��� ������ ������� � ������� �� �������) ������� ��� ����������� ���������
	// ��������� ��������� ����� ��� ��������� (��� ������������ ��������)
    TDisplayField *m_et_static_press; // ����������� ��������
    TDisplayField *m_et_exam_press; // ������� �������� 

	// for debug
	// ������� ������ ������� (� ��� ����� ��� ���������)
	TDisplayField *m_ltr11_value;
	// ��������, ����������� �� �������
	TDisplayField *m_ltr212_value;
	// ��������� ����������� �������
	TDisplayField *m_ltr42_value;

	ProtocolDialog* m_protocol_dialog;
	Protocol* m_protocol;

	QMessageBox* m_msg;
	QTextEdit* m_terminal;
	//������� ��� ������ � Ltr-��������
	Ltr42Module *m_ltr42;
	Ltr41Module *m_ltr41;
	Ltr11Module *m_ltr11;
	Ltr212Module *m_ltr212;
	
	// ���������
	StatusReader* m_st_reader;
	CheckTask* m_check_task;
	RecognizeTask* m_recogn_task;
	DeformationTask* m_deform_task;
	ArrowTask* m_arrow_task;

	inline bool isRun() const;      // ������� - true, ���� � ������ ������ ����������� �����-������ ���������
	inline bool isAllowRun() const; // ������� - ����� �� ��������� ���������
	inline bool isValidate() const; // ������� - true, ���� ���� ��������� ��������
	inline bool isRecognize() const;// ������� - true, ���� ��� ������� ��� �������� (������ �� �� ��� ��������� �������������)
	inline bool isError() const;


	// ������ � ��
	Spring m_spring_info; // ���������� �������
	SpringDatabase* m_db_dialog;

private slots:

	// ��������� ���������
	void validateButtonPressed();
	void recognizeButtonPressed();
	void deformButtonPressed();
	void arrowButtonPressed();
	void protocolButtonPressed();
	void stopButtonPressed();
	void resetButtonPressed();
	void exit();

	// ����������� ���������� ���������
	void completeValidate();
	void completeRecognize();
	void completeDeform();
	void completeArrow();

	void showSpringScheme(const SpringScheme& sp);
	void fetchSpringScheme(const SpringScheme& sp);
	
	// ���������� ���������������� ������� �������
	void drawProfile(const std::vector<double>& src);
	void showMsg(const QString&);
	// ������, �������� ������ ���������
	void updateStatusCode();

	//! ���������� ������� ���������
	void ltr42Cmd(unsigned code);
	void ltr11Value(double value);
	void ltr212Value(double value);
};

#endif // SPRINGCALIBRATION_H
