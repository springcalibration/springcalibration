#include "SpringCalibration.h"

Protocol::Protocol(QObject *parent): QObject(parent), m_num(0)
{
	// ����� ���������� ��������� � ����� "��� ���������\Reports"
	QDir dir(QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation) + QString("\\Reports\\"));
	if(!dir.exists())
		dir.mkdir(QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation) + QString("\\Reports\\"));
	// ������ ���������� ���� (����� ���������)
	std::ifstream in("session");
	if(in.is_open()){
		in >> m_num;
		in.close();
	}
}
Protocol::~Protocol()
{
	std::ofstream out("session");
	out << m_num;
	out.close();
}

QString& Protocol::buildProtocol(const Spring& sp)
{
	// ����� ���������
	readNum();

	QString protocol_title = tr("�������� �") + QString::number(m_num) + tr(" �� ") + 
		QDate::currentDate().toString("dd.MM.yyyy") +
		tr("<br>��������� �������: ") + sp.scheme.scheme + ", " + sp.scheme.target + ", " + sp.scheme.type + tr(". ���������: ") + sp.scheme.engine + ".";

	m_text = "<table width='100%'>"
		"<tr><td colspan='3' align='center'><font size='+2'>" + protocol_title + "</font></td></tr>"
		"</table><br><br>"
		"<table width='100%' cellspacing='1'>" 
	+ tr("<tr><td width='40%'><b>������������ ���������</b><br></td><td align='right'><b>�� �������</b></td><td align='right'><b>����</b></td></tr>");

	m_text += tr("<tr><td>������� ������</td><td align='right'>")                  + QString::number(sp.scheme.stick_diam, 'f', 0)     +tr(" ��.</td><td align='right'>")+ QString::number(sp.stick_diam, 'f', 0)     +tr(" ��.</td></tr>");
	m_text += tr("<tr><td>���������� �������</td><td align='right'>")              + QString::number(sp.scheme.inner_diam, 'f', 0)     +tr(" ��.</td><td align='right'>")+ QString::number(sp.inner_diam, 'f', 0)     +tr(" ��.</td></tr>");
	m_text += tr("<tr><td>������� �������</td><td align='right'>")                 + QString::number(sp.scheme.outer_diam, 'f', 0)     +tr(" ��.</td><td align='right'>")+ QString::number(sp.outer_diam, 'f', 0)     +tr(" ��.</td></tr>");
	m_text += tr("<tr><td>������ � ��������� ���������</td><td align='right'>")    + QString::number(sp.scheme.height_min, 'f', 0) +" - "+ QString::number(sp.scheme.height_max, 'f', 0)        +tr(" ��.</td><td align='right'>")+ QString::number(sp.height, 'f', 1)         +tr(" ��.</td></tr>");
	m_text += tr("<tr><td>���</td><td align='right'>")                             + QString::number(sp.scheme.step, 'f', 1)           +tr(" ��.</td><td align='right'>")+ QString::number(sp.step, 'f', 1)           +tr(" ��.</td></tr>");
	m_text += tr("<tr><td>��������������� ����</td><td align='right'>")            + QString::number(sp.scheme.step_irregular, 'f', 1) +tr(" ��.</td><td align='right'>")+ QString::number(sp.step_irregular, 'f', 1) +tr(" ��.</td></tr>");
	m_text += tr("<tr><td>�������������������� ����������</td><td align='right'>") + QString::number(sp.scheme.not_ortogonal, 'f', 1)  +tr(" ��.</td><td align='right'>")+ QString::number(sp.not_ortogonal, 'f', 1)  +tr(" ��.</td></tr>");
	m_text += tr("<tr><td>���������� ����������</td><td align='right'>")           + QString::number(sp.scheme.deformation, 'f', 1)    +tr(" ��.</td><td align='right'>")+ QString::number(sp.deformation, 'f', 1)    +tr(" ��.</td></tr>");
	m_text += tr("<tr><td>������ �������</td><td align='right'>")                  + QString::number(sp.scheme.static_arrow_min, 'f', 0) +" - "+ QString::number(sp.scheme.static_arrow_max, 'f', 0)   +tr(" ��.</td><td align='right'>")+ QString::number(sp.arrow_height, 'f', 1)   +tr(" ��.</td></tr>");

	m_text += "</table><br><br><br>"
		"<table width='100%' cellspacing='5'>"
		
		+tr("<tr><td width='25%'><b>��������� �����:</b></td><td align='left' width='25%'>")+m_operator+tr("</td><td>�������: ___________</td></tr>")
		+tr("<tr><td><br></td></tr>")
		+tr("<tr><td><b>��������:</b></td><td align='left'>")+m_master+tr("</td><td>�������: ___________</td></tr>") +
		"</table>";

	return m_text;
}

void Protocol::writeProtocol(const Spring& sp)
{
	QTextDocument doc;
	doc.setHtml(buildProtocol(sp));

	QString file_name = QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation) + 
		QString("\\Reports\\Report") + QString::number(m_num) + 
		"_" + QDate::currentDate().toString("dd.MM.yyyy") + 
		"_" + QTime::currentTime().toString("H.m") + QString(".pdf");

	QPrinter file;
	file.setOutputFormat(QPrinter::PdfFormat);
	file.setOutputFileName(file_name);
	doc.print(&file);
	
	if(!QDesktopServices::openUrl(QUrl::fromLocalFile(file_name)))
	{
		QMessageBox msg;
		msg.setText(tr("�� ������� ������� ���� ") + file_name + QString("!"));
		msg.exec();
	}
}
void Protocol::printProtocol(const Spring& sp)
{
	// ��������� �������� � ������ ��� �� ������
	QTextDocument doc;
	doc.setHtml(buildProtocol(sp));

	// ����� ������� �� ������ ��������� ����� ��������� �� ��������� ������ � pdf
	QString file_name = QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation) + 
		QString("\\Reports\\Report") + QString::number(m_num) + 
		"_" + QDate::currentDate().toString("dd.MM.yyyy") + 
		"_" + QTime::currentTime().toString("H.m") + QString(".pdf");

	QPrinter file;
	file.setOutputFormat(QPrinter::PdfFormat);
	file.setOutputFileName(file_name);

	QPrinter printer;
	printer.setOutputFormat(QPrinter::NativeFormat);
	
	doc.print(&file);   // �������� � ����
	doc.print(&printer);// �������� �� ��������
}
// �������� ���������� ���� ���������
void Protocol::clear()
{}
