#ifndef WORK_THREADS_H
#define WORK_THREADS_H

#include "LtrModules.h"
#include "params.h"
#include <QThread>
#include <ctime>
#include <cmath>
#include <cstdlib>
#include <vector>
#include <algorithm>

// declarations

extern QMutex Ltr41Mod, Ltr11Mod, Ltr42Mod, Ltr212Mod;

extern volatile bool RunTest;//������� ���������� ���������� ���������

//volatile static bool Run212Test = false; // ������ ?

// ����� ��� ������ ������ � Ltr11 (��������� �������������� ���������� �������)
extern double data[];
extern const size_t data_sz;
extern int data_ptr;

// ������� ��������� ���������, ltr41
extern volatile WORD code_status; 
extern QMutex mtStatus;

// ������� �������� �� ������� � ������ ������� ��� ���������
extern QMutex mtSpringPress;
extern volatile double spring_pressure;
extern volatile double spring_height;


inline double cpuTime(){return (double)clock()/CLOCKS_PER_SEC;}

// ������ ������ � ltr41, ���������� �������� � ����� ������
// �������� ������
// �������� ��������� ���� ������ ���������� ����� LTR-41-1 � 0000 0000 0000 0100.
extern volatile WORD code_status; 

class StatusReader: public QThread{
	Q_OBJECT
public:
	StatusReader(Ltr41Module *ltr41, QObject* parent = 0)
		: QThread(parent), m_ltr41(ltr41){}
	void run();
private:
	Ltr41Module *m_ltr41;
signals:
	void updateStatus();
};

// �������� ���������, ����������� ����� ����� �����������
class CheckTask: public QThread {
	Q_OBJECT
public:
	CheckTask(Ltr42Module* ltr42, QObject* parent = 0)
		: QThread(parent), m_ltr42(ltr42){}
	void run();
private:
	Ltr42Module* m_ltr42;
signals:
	void complete();
	void msg(const QString&);
};

struct Segment
{
	Segment(): smin(0), smax(0), smin_pos(0), diam(0){}
	double first() const {return points.front();}
	double last() const {return points.back();}
	double smin;
	size_t smin_pos;
	double smax;
	double diam;
	double len() const {return 0.1 * points.size();} // to do: scale
	std::vector<double> points;
	bool operator < (const Segment& other) const {return smin < other.smin;}
};


class RecognizeTask: public QThread {
	Q_OBJECT
public:

	typedef enum {
		rmRecognizeSpring, // ����������� ���� �������, ����� � ��
		rmEstimateSpring   // ��������� ���-��� ������� ����� ��������� �� ���������� ����������
	} Mode;

	RecognizeTask(Ltr11Module *ltr11, Ltr42Module *ltr42, QObject* parent = 0)
		: QThread(parent), m_ltr11(ltr11), m_ltr42(ltr42)
		, m_min_dest(50)
		, m_max_dest(350)
		, m_mode(rmRecognizeSpring)
	{}
	void run();
	
	void setMode(Mode mode) {m_mode = mode;}
	Mode mode() const {return m_mode;}
	const std::vector<double>& point_src() const {return src;}
	const std::vector<double>& point_data() const {return data;}

	double m_inner_diam;
	double m_outer_diam;
	double m_stick_diam;
	double m_height;
	double m_step;
	double m_step_irregular;
private:
	Ltr42Module *m_ltr42;
	Ltr11Module *m_ltr11;
	Mode m_mode; 
	const double m_min_dest;
	const double m_max_dest;
	std::vector<double> src;
	std::vector<double> data; // ����� ����� ��������������

	// �������� �����
	bool recognizeSpring(const std::vector<double>& src);
	void preprocess(const std::vector<double>& src, std::vector<double>& dest);
	void split_segments(std::vector<double>& data, std::vector<Segment>& segments);
	void calc_params(std::vector<Segment>& segments);
	double calc_diam(double h);
	double dist(const Point& A, const Point& B);

	// debug
	bool readData(const char* filename, std::vector<double>& vec);
	bool readCSV(const char* filename, double data[], size_t& size);
signals:
	void complete();
	void msg(const QString&);
};

// �������� �������� � ����� �������
class PressureControl: public QThread
{
	Q_OBJECT
public:
	PressureControl(Ltr11Module *ltr11, Ltr212Module *ltr212, QObject* parent = 0)
		: QThread(parent), m_ltr11(ltr11), m_ltr212(ltr212){}
	void run();
private:
	Ltr11Module *m_ltr11;
	Ltr212Module *m_ltr212;
signals:
	void update();
	void msg(const QString&);
};

// ��������� �� ���������� ����������
class DeformationTask: public QThread
{
	Q_OBJECT
public:
	DeformationTask(Ltr11Module *ltr11, Ltr212Module *ltr212, Ltr42Module *ltr42, QObject* parent = 0)
		: QThread(parent), m_ltr42(ltr42), m_pc(ltr11, ltr212), m_exam_height(0), m_exam_press(10), m_ltr11(ltr11){}
	void run();
	double m_exam_height; // ������ ��� ������� ��������� (����������)
	double m_exam_press; // �������� ������� �������� (�� �������)
private:
	Ltr42Module *m_ltr42;
	Ltr11Module *m_ltr11;
	PressureControl m_pc;
signals:
	void complete();
	void msg(const QString&);
};

class ArrowTask: public QThread
{
	Q_OBJECT
public:
	ArrowTask(Ltr11Module *ltr11, Ltr212Module *ltr212, Ltr42Module *ltr42, QObject* parent = 0)
		: QThread(parent), m_ltr42(ltr42), m_pc(ltr11, ltr212), m_arrow_height(0), m_arrow_press(0), m_ltr11(ltr11){}
	void run();
	double m_arrow_height;
	double m_arrow_press;
private:
	Ltr42Module *m_ltr42;
	Ltr11Module *m_ltr11;
	PressureControl m_pc;
signals:
	void complete();
	void msg(const QString&);
};

#endif //WORK_THREADS_H