#ifndef WIDGETS_H
#define WIDGETS_H

#include <QtGui/QtGui>

class AnalogDisplay: public QWidget{
	Q_OBJECT
	double m_maxVal, m_minVal, m_halfVal;
	double m_value;
	double m_anglePerVal;
	QString m_title;
	struct CalbCoef{
		CalbCoef(double m, double a): cmul(m), cangle(a){}
		double cmul, cangle;
	};
	static const CalbCoef m_coef[11];
public:
	AnalogDisplay(QString title, QWidget *parent = 0);
	void setRangeValue(double minVal, double halfVal, double maxVal);
	void setAnglePerValue(double angle){m_anglePerVal = angle;}
	void setDisplayTitle(QString title){m_title = title;}
public slots:
	void setValue(double value);
protected:
	void paintEvent(QPaintEvent* e);
};
//����� �������������� ������
class MainTestButton: public QLabel{
	Q_OBJECT
public:
	MainTestButton(const QString& title, int num, QWidget* parent = 0);
	void press(){
		setPalette(active_palette);
	}
	void release(){
		setPalette(default_palette);
	}
protected:
	void mousePressEvent(QMouseEvent* e);
	void mouseReleaseEvent(QMouseEvent* e);
private:
	QPalette default_palette, active_palette;
	int m_num;
signals:
	void pressed(int);
};
class PushButton: public MainTestButton{
	Q_OBJECT
public:
	PushButton(const QString& title, int num, QWidget* parent = 0)
		:MainTestButton(title, num, parent)
	{
		setFixedSize(QSize(130, 50));
	}
};

typedef PushButton** PushButtonList;
typedef MainTestButton* ptrMainTestButton;

class SubTestButton: public MainTestButton{
	Q_OBJECT
public:
	SubTestButton(const QString& title, int num, QWidget* parent = 0);
};

class MenuList: public QObject{
	Q_OBJECT
public:
	MainTestButton** items;
	int size;
	int item;
	MenuList* m_parent;
	MenuList(MainTestButton** menu, int sz, MenuList* parent = 0)
		: items(menu), size(sz), item(-1), m_parent(parent){}
	void nextItem();
	void prevItem();
	void setMinSize(const QSize& sz);
	void setMaxSize(const QSize& sz);
	void setFixedSize(const QSize& sz);
	void showMenu();
	void hideMenu();
	MenuList* parent() const {return m_parent;}
	void setParent(MenuList* parent) {m_parent = parent;}
public slots:
	void pressItem(int i);
};
//��������� �������� ��������� ��������� ������������
//to do - � ����������
class CheckMenuList: public MenuList{
	Q_OBJECT
public:
	CheckMenuList(MainTestButton** menu, int sz): MenuList(menu, sz)
	{
		m_check_item = new bool[sz];
		for(int i = 0; i < sz; i++) m_check_item[i] = false;
	}
	~CheckMenuList(){delete[] m_check_item;}
public slots:
	void pressItem(int i);
private:
	//������ ���������, ������� �������
	bool* m_check_item;
};
//-----------------------------------------------------------------------------------------------
//���� ��� ����������� ����������, ��������� � ��������
class Title: public QLabel{
public:
	Title(const QString& text, int font_size = 15, QWidget* parent = 0): QLabel(text, parent)
	{
		setFont(QFont("Times", font_size, QFont::Bold));
		setAlignment(Qt::AlignCenter);
	}
};

class ParamName: public QLabel{
public:
	ParamName(const QString& text, QWidget* parent = 0): QLabel(text, parent)
	{
		setFont(QFont("Times", 13, QFont::Light));
		setAlignment(Qt::AlignRight);
	}
};
class DisplayField: public QLabel{
public:
	DisplayField(const QString& text = "", QWidget* parent = 0): QLabel(text, parent)
	{
		setFrameStyle(QFrame::Panel|QFrame::Sunken);
		setLineWidth(1);
		setMidLineWidth(0);
		setFont(QFont("Times", 13, QFont::Light));

		setAlignment(Qt::AlignRight);
		setBackgroundRole(QPalette::Base);
		setAutoFillBackground(true);
	}
};
class TDisplayField: public QWidget{
	Q_OBJECT
public:
	TDisplayField(const QString& content = "", const QString& lbl = "", QWidget* parent = 0): QWidget(parent)
	{
		QLabel* plbl = new QLabel("<b>" + lbl + "</b>");
		plbl->setIndent(5);
		QHBoxLayout* layout = new QHBoxLayout;
		layout->addWidget(m_field = new DisplayField(content), 3);
		layout->addWidget(plbl, 1, Qt::AlignLeft);
		layout->setSpacing(0);
		layout->setContentsMargins(0,0,0,0);
		setLayout(layout);
		m_field->setFixedWidth(100);
	}
	QString text(){return m_field->text();}
	void clear(){m_field->clear();}
	void setWidth(int w){m_field->setFixedWidth(w);}
	void setAlignment(Qt::Alignment a){m_field->setAlignment(a);}
public slots:
	void setText(const QString& text){m_field->setText(text);}
private:
	DisplayField* m_field;
};
class TEditField: public QWidget{
	Q_OBJECT
public:
	TEditField(const QString& content = "", const QString& lbl = "", int w = 80, QWidget* parent = 0): QWidget(parent)
	{
		QHBoxLayout* layout = new QHBoxLayout;
		layout->addWidget(m_field = new QLineEdit(content), 0, Qt::AlignLeft);
		layout->addWidget(m_lbl = new QLabel("<b>" + lbl + "</b>"), 0, Qt::AlignLeft);
		layout->setSpacing(0);
		layout->setContentsMargins(0,0,0,0);
		setLayout(layout);
		m_field->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
		m_field->setFixedWidth(w);
		m_lbl->setIndent(5);

		connect(m_field, SIGNAL(textEdited(const QString&)), SIGNAL(textEdited(const QString&)));
	}
	void setText(const QString& text) {m_field->setText(text);}
	void setReadOnly(bool key) {m_field->setReadOnly(key);}
	QString text() const {return m_field->text();}
	void clear() {m_field->clear();}
private:
	QLineEdit* m_field;
	QLabel* m_lbl;
signals:
	void textEdited(const QString&);
};
//-----------------------------------------------------------------------------------------------

class TestInfo: public QLabel{
	Q_OBJECT
public:
	TestInfo(const QString& text, QWidget *parent = 0);
};
class TestTitle: public QLabel{
	Q_OBJECT
public:
	TestTitle(const QString& text, QWidget *parent = 0);
};
class MsgQuestion: public QMessageBox{
	Q_OBJECT
public:
	MsgQuestion(const QString& title, const QString& text, QWidget* parent = 0);
	QPushButton *pYes, *pNo;
signals:
	void yesClicked();
	void noClicked();
};
//-------------------------------------------------------------------------------------------
//����������� �������������
class Switcher: public QLabel{
	Q_OBJECT
public:
	typedef enum {_on, _off} State;
	Switcher(const QString& title, QWidget* parent = 0);
protected:
	void paintEvent(QPaintEvent*);
	void mousePressEvent(QMouseEvent*);
private:
	State m_state;
	const qreal m_on_angle, m_off_angle;
	qreal m_angle;
	QString m_on_text, m_off_text, m_title;
public slots:
	void switchOn();
	void switchOff();
	void toggle();
signals:
	void on();
	void off();
};

class SwitcherSA4: public QLabel{
	Q_OBJECT
public:
	SwitcherSA4(QSize size = QSize(100, 100), QWidget* parent = 0);
	QString m_zero_text, m_one_text, m_two_text, m_three_text, m_four_text, m_title;
	int value() const {m_switch->value();}
private:
	QDial *m_switch;
protected:
	void paintEvent(QPaintEvent*);
public slots:
	void setValue(int);
signals:
	void valueChanged(int);
};

class SwitcherSA: public QLabel{
	Q_OBJECT
public:
	SwitcherSA(QSize size = QSize(100, 100), QWidget* parent = 0);
	QString m_on_text, m_off_text, m_title;
	int value() const {m_switch->value();}
private:
	QDial *m_switch;
protected:
	void paintEvent(QPaintEvent*);
public slots:
	void setValue(int);
signals:
	void valueChanged(int);
};
//-----------------------------------------------------------------------------------------------
//���� ����������� ���������

class ProtocolDialog : public QDialog{
	Q_OBJECT
public:
	ProtocolDialog(QWidget *parent = 0, Qt::WFlags flags = 0);
	~ProtocolDialog();
	//����, ������������ ��� �������� ���������� ������������ ������
	QString m_operator, m_master;
	QLineEdit *m_eOperator, *m_eMaster;
	QPushButton *pYes, *pNo;
private slots:
	void pressYes();
	void pressNo();
};

#include "graphicx.h"

#endif //WIDGETS_H