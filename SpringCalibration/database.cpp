#include "database.h"

static CC const& Par = config.getCommon();

SpringDatabase::SpringDatabase(QWidget *parent, Qt::WFlags flags)
	: QDialog(parent, flags)
	, m_row(-1)
	, m_col(-1)
	, m_db_name("spring.db")
	, m_db_definition("create_db.sql")
	, m_login(_logoff)
{
	setWindowTitle(tr("���� ��������� ���� ������ ������"));

	m_db = QSqlDatabase::addDatabase("QSQLITE", "edit_conn");
	m_db.setDatabaseName(m_db_name);

	QStringList headers = (QStringList() 
		<< tr("�����") // id, �� ������������
		<< tr("����� ���������") 
		<< tr("���������� �������")
		<< tr("������")
		<< tr("��� �������")
		<< tr("������")
		);

	m_table = new QTableWidget(this);
	m_table->setColumnCount(6);
	m_table->setHorizontalHeaderLabels(headers);
	m_table->hideColumn(0); // ������������� ������ � ������� �� ������������
	m_table->setWordWrap(true);
	m_table->setEditTriggers(QAbstractItemView::NoEditTriggers);

	// ����������� ������: ��������, �������, ���������, ������.
	QStringList buttons = QStringList() 
		<< tr("Login")
		<< tr("��������")
		<< tr("�������������")
		<< tr("�������")
		<< tr("�������")
		<< tr("�������")
		;
	std::vector<QPushButton*> btns;
	for(int i = 0; i < buttons.size(); i++)
		btns.push_back(m_btn[i] = new QPushButton(buttons.at(i)));

	QHBoxLayout* btn_layout = new QHBoxLayout;
	for(int i = 0; i < buttons.size(); i++)
	{
		if(i==1) btn_layout->addStretch(1);
		btn_layout->addWidget(btns[i]);
	}
	btn_layout->setAlignment(Qt::AlignRight);

	connect(btns[0], SIGNAL(clicked()), SLOT(loginButtonPressed()));
	connect(btns[1], SIGNAL(clicked()), SLOT(addButtonPressed()));
	connect(btns[2], SIGNAL(clicked()), SLOT(editButtonPressed()));
	connect(btns[3], SIGNAL(clicked()), SLOT(removeButtonPressed()));
	connect(btns[4], SIGNAL(clicked()), SLOT(chooseButtonPressed()));
	connect(btns[5], SIGNAL(clicked()), SLOT(cancelButtonPressed()));

	// ���������� ���� ��������� ����������
	QVBoxLayout* layout = new QVBoxLayout;
	layout->addWidget(m_table);    // �������
	layout->addLayout(btn_layout); // ������
	setLayout(layout);

	connect(m_table, SIGNAL(itemSelectionChanged()), SLOT(selectionChanged()));

	m_table->setMinimumSize(600, 300);
}

SpringDatabase::~SpringDatabase()
{}

bool SpringDatabase::open()
{
	// ���������, ��� �� ����������;
	// ���� ����� ���, �� �������� ������� ��
	if(checkDatabase(m_db_name))
	{
		// ��������� ��
		if(m_db.open())
		{
			// ������ ������ �� ��
			if(readSpringDatabase())
			{
				show();
				return true;
			}else
				QMessageBox::critical(0, tr("������"), tr("��������� ��������� ������ �� ��!"));
		}else
			QMessageBox::critical(0, tr("������"), tr("��������� ������� ���� ��!"));
	}else
		QMessageBox::critical(0, tr("������"), tr("�� �� ����������!"));
	return false;
}
void SpringDatabase::close()
{
	if(m_db.isOpen())
		m_db.close();
	hide();
}

// ���������, ��� �� ��������� �����������. ���� ��� ��, �� ���������� true
bool SpringDatabase::checkDatabase(const QString& database_name)
{
	QFile file(database_name);
	if(!file.exists() && !createDatabase(database_name))
	{
		QMessageBox::critical(0, tr("������"), tr("��������� ������� ���� ���� ������!"));
		return false;
	}
	return true;
}


// ��������� ���� ������ �����������
bool SpringDatabase::createDatabase(const QString& database_name)
{
	QFile file(m_db_definition);
	if(file.open(QIODevice::ReadOnly))
	{
		QList<QByteArray> stmt_list = file.readAll().split(';');

		if(stmt_list.isEmpty())
		{
			QMessageBox::information(0, "", tr("���������� ������� ��: ������ � ���� ����������� ��."));
			return false;
		}
		file.close();

		if(!m_db.open())
			return false;

		QSqlQuery query(m_db);
		for(int i = 0; i < stmt_list.size(); i++)
		{
			query.exec(tr(stmt_list[i]));
		}
		m_db.close();
		
		return true;
	}
	return false;
}


bool SpringDatabase::readSpringDatabase()
{
	if(!m_db.isOpen()) false;
	QSqlQuery query(m_db);
	if(!query.exec("SELECT * FROM springs;"))
	{
		QSqlError e = query.lastError();
		QMessageBox::information(0, tr("������ ��� ���������� SQL-�������"), e.text());
		return false;
	}

	m_row = m_col = -1;
	m_springs.clear();
	m_btn[_btn_add]->setEnabled(false);
	m_btn[_btn_edit]->setEnabled(false);
	m_btn[_btn_delete]->setEnabled(false);
	m_btn[_btn_choose]->setEnabled(false);
	//m_btn[_btn_close]->setEnabled(true);
	m_btn[_btn_login]->setText(tr("Login"));
	m_login = _logoff;

	for(int i = 0; i < m_table->rowCount(); i++)
		m_table->removeRow(i);
	m_table->reset();

	while(query.next()) 
	{
		SpringScheme sp;
		sp.id = query.value(0).toInt();
		sp.engine = query.value(1).toString();
		sp.target = query.value(2).toString();
		sp.scheme = query.value(3).toString();
		sp.type = query.value(4).toString();
		sp.group = query.value(5).toInt();
		sp.step = query.value(6).toDouble();
		sp.step_irregular = query.value(7).toDouble();
		sp.not_ortogonal = query.value(8).toDouble();
		sp.stick_diam = query.value(9).toDouble();
		//sp.inner_diam_min = query.value(10).toDouble();
		//sp.inner_diam_max = query.value(11).toDouble();
		sp.inner_diam = query.value(10).toDouble();
		//sp.outer_diam_min = query.value(12).toDouble();
		//sp.outer_diam_max = query.value(13).toDouble();
		sp.outer_diam = query.value(11).toDouble();
		sp.height_min = query.value(12).toDouble();
		sp.height_max = query.value(13).toDouble();
		//sp.height = query.value(12).toDouble();
		sp.static_press = query.value(14).toInt();
		//sp.static_height_min = query.value(17).toDouble();
		//sp.static_height_max = query.value(18).toDouble();
		sp.static_height = query.value(15).toDouble();
		sp.static_arrow_min = query.value(16).toDouble();
		sp.static_arrow_max = query.value(17).toDouble();
		//sp.static_arrow = query.value(15).toDouble();
		sp.exam_press = query.value(18).toInt();
		sp.cover_height = query.value(19).toInt();
		
		m_springs.push_back(sp);
	}

	// ��������� �������� � m_table
	m_table->setRowCount(m_springs.size());
	for(size_t i = 0; i < m_springs.size(); i++)
	{
		m_table->setItem(i, 0, new QTableWidgetItem(QString::number(m_springs[i].id) ));
		m_table->setItem(i, 1, new QTableWidgetItem(m_springs[i].engine));
		m_table->setItem(i, 2, new QTableWidgetItem(m_springs[i].target));
		m_table->setItem(i, 3, new QTableWidgetItem(m_springs[i].scheme));
		m_table->setItem(i, 4, new QTableWidgetItem(m_springs[i].type));
		m_table->setItem(i, 5, new QTableWidgetItem(QString::number(m_springs[i].group) ));
	}
	

	// debug
	//std::ofstream out("sp_list.txt", std::ios::out);
	//for(size_t i = 0; i < m_springs.size(); i++)
	//{
	//	out << m_springs[i];
	//}
	//out.close();

	return true;
}
bool SpringDatabase::executeQuery(const QString& sql, QSqlDatabase& db)
{
	if(!db.isOpen()) false;
	QSqlQuery query(db);
	if(!query.exec(sql))
	{
		QSqlError e = query.lastError();
		QMessageBox::information(0, tr("������ ��� ���������� SQL-�������"), e.text());
		return false;
	}
	return true;
}
int SpringDatabase::selectionChanged()
{
	if(!m_btn[_btn_choose]->isEnabled())
	{
		//m_btn[_btn_edit]->setEnabled(true);
		//m_btn[_btn_delete]->setEnabled(true);
		m_btn[_btn_choose]->setEnabled(true);
	}

	//m_col = m_table->currentItem()->column();
	//m_row = m_table->currentItem()->row();

	return m_row;
}
// �������� ����� ������ � ����. � � ��
void SpringDatabase::addButtonPressed()
{
	if(!m_db.isOpen()) return;

	SpringScheme sp;
	SpringEdit se(sp);

	if(se.exec() != QDialog::Accepted)
		return;


	QSqlQuery query(m_db);
	query.exec("SELECT max(id) FROM springs;");
	query.next(); // ��������� ������ �� ������ ������

	sp = se.m_spring;
	sp.id = query.value(0).toInt() + 1;

	QString s_query = sp.insertQuery();
	if(!query.exec(s_query))
	{
		QFile file("error_insert");
		if(file.open(QIODevice::ReadWrite))
		{
			file.write(s_query.toAscii());
			QSqlError e = query.lastError();
			file.write(e.text().toAscii());
			file.close();
		}
		else
			QMessageBox::information(0, "", tr("�� ������� ������� ����"));
		QMessageBox::information(0, "", tr("�� ������� �������� ������ � ��"));
	}
		 
	int row = m_springs.size(); // ����� ������ � �������
	m_table->insertRow(row); 
	// ����� �� ��������� item's
	m_table->setItem(row, 0, new QTableWidgetItem(QString::number(sp.id)));
	m_table->setItem(row, 1, new QTableWidgetItem(sp.engine));
	m_table->setItem(row, 2, new QTableWidgetItem(sp.target));
	m_table->setItem(row, 3, new QTableWidgetItem(sp.scheme));
	m_table->setItem(row, 4, new QTableWidgetItem(sp.type));
	m_table->setItem(row, 5, new QTableWidgetItem(QString::number(sp.group)));
	m_table->setCurrentCell(row, 0);

	m_springs.push_back(sp);
}
// �������� ������ ���� ���� �������� ������
void SpringDatabase::removeButtonPressed()
{
	if(!m_db.isOpen()) return;
	int row = m_table->currentRow();
	if( row >= 0 && row < m_table->rowCount())
	{
		MsgQuestion msg(tr("�������������"), tr("��������������� ������ ������� �������?"));
		msg.exec();
		if(msg.clickedButton() == msg.pYes)
		{
			// ������� ������ �� ��
			QSqlQuery query(m_db);
			if(!query.exec(m_springs[row].deleteQuery()))
			{
				QMessageBox::information(0, "", tr("�� ������� �������� ������"));
			}
			m_springs.erase(m_springs.begin() + row);
			// ������� ������ �� ����������� �������������
			m_table->removeRow(row);
		}
	}
}
// �������� ������ ���� ���-�� �������� (��� ��������� ����� ������)
void SpringDatabase::editButtonPressed()
{
	if(!m_db.isOpen()) return;
	int row = m_table->currentRow();
	if( row >= 0 && row < m_table->rowCount())
	{
		SpringEdit se(m_springs[row]);		
		if(se.exec() != QDialog::Accepted)
			return;

		SpringScheme sp = se.m_spring;
		QString s_query = sp.updateQuery();
		QSqlQuery query(m_db);
		if(!query.exec(s_query))
		{
			QMessageBox::information(0, "", tr("�� ������� �������� ������"));
			QFile file("error_update");
			if(file.open(QIODevice::ReadWrite))
			{
				file.write(s_query.toAscii());
				QSqlError e = query.lastError();
				file.write(e.text().toAscii());
				file.close();
			}
			else
				QMessageBox::information(0, "", tr("�� ������� ������� ����"));
		}
		m_springs[row] = sp;
		m_table->setItem(row, 0, new QTableWidgetItem(QString::number(sp.id)));
		m_table->setItem(row, 1, new QTableWidgetItem(sp.engine));
		m_table->setItem(row, 2, new QTableWidgetItem(sp.target));
		m_table->setItem(row, 3, new QTableWidgetItem(sp.scheme));
		m_table->setItem(row, 4, new QTableWidgetItem(sp.type));
		m_table->setItem(row, 5, new QTableWidgetItem(QString::number(sp.group)));
	}
}
void SpringDatabase::chooseButtonPressed()
{
	int row = m_table->currentRow();
	if(row >= 0 && row < m_springs.size())
	{
		emit fetchSpring(m_springs[row]);
		cancelButtonPressed();
	}
}

void SpringDatabase::cancelButtonPressed()
{
//	for(int i = 0; i < m_table->rowCount(); i++)
//		m_table->removeRow(i);
//	m_table->reset();
//	m_springs.clear();
	close();
}

void SpringDatabase::loginButtonPressed()
{
	if(m_login == _logoff)
	{
		// ��������� ������
		LoginDialog login(m_db);
		if(login.exec() == QDialog::Accepted)
		{	
			m_btn[_btn_add]->setEnabled(true);
			m_btn[_btn_edit]->setEnabled(true);
			m_btn[_btn_delete]->setEnabled(true);
			m_btn[_btn_login]->setText(tr("Logoff"));
			m_login = _logon;
		}
	}
	else
	{
		m_btn[_btn_add]->setEnabled(false);
		m_btn[_btn_edit]->setEnabled(false);
		m_btn[_btn_delete]->setEnabled(false);
		m_btn[_btn_login]->setText(tr("Login"));
		m_login = _logoff;
	}
}

bool SpringDatabase::findSpring(SpringScheme& sp_, double inner_diam_, double outer_diam_, double height_)
{
	if(!m_db.open()) 
	{
		QMessageBox::critical(0, "������", tr("��������� ������� ���� ���� ������!"));
		return false;
	}
	QSqlQuery query(m_db);
	if(!query.exec("SELECT * FROM springs;"))
	{
		QMessageBox::critical(0, tr("������"), tr("������ ��� ������ ������ �� ������� springs!"));
		return false;
	}

	while(query.next()) 
	{
		SpringScheme sp;
		sp.id = query.value(0).toInt();
		sp.engine = query.value(1).toString();
		sp.target = query.value(2).toString();
		sp.scheme = query.value(3).toString();
		sp.type = query.value(4).toString();
		sp.group = query.value(5).toInt();
		sp.step = query.value(6).toDouble();
		sp.step_irregular = query.value(7).toDouble();
		sp.not_ortogonal = query.value(8).toDouble();
		sp.stick_diam = query.value(9).toDouble();
		//sp.inner_diam_min = query.value(10).toDouble();
		//sp.inner_diam_max = query.value(11).toDouble();
		sp.inner_diam = query.value(10).toDouble();
		//sp.outer_diam_min = query.value(12).toDouble();
		//sp.outer_diam_max = query.value(13).toDouble();
		sp.outer_diam = query.value(11).toDouble();
		sp.height_min = query.value(12).toDouble();
		sp.height_max = query.value(13).toDouble();
		//sp.height = query.value(12).toDouble();
		sp.static_press = query.value(14).toInt();
		//sp.static_height_min = query.value(17).toDouble();
		//sp.static_height_max = query.value(18).toDouble();
		sp.static_height = query.value(15).toDouble();
		sp.static_arrow_min = query.value(16).toDouble();
		sp.static_arrow_max = query.value(17).toDouble();
		//sp.static_arrow = query.value(15).toDouble();
		sp.exam_press = query.value(18).toInt();
		sp.cover_height = query.value(19).toInt();
		
		if(
			( (sp.inner_diam - Par.inner_diam_min) <= inner_diam_ && inner_diam_ <= (sp.inner_diam + Par.inner_diam_max) ) && 
			( (sp.outer_diam - Par.outer_diam_min) <= outer_diam_ && outer_diam_ <= (sp.outer_diam + Par.outer_diam_max) ) && 
			(  sp.height_min <= height_ && height_ <= sp.height_max )
			)
		{
			sp_ = sp;
			return true;
		}
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////
// ���� ��� �������������� ���� ������

LoginDialog::LoginDialog(QSqlDatabase& db, QWidget *parent, Qt::WFlags flags)
	: QDialog(parent, flags)
	, m_db(db)
	, m_btn_accept(tr("�������"))
	, m_btn_reject(tr("������"))
{
	QGridLayout *glayout = new QGridLayout;
	glayout->addWidget(new QLabel(tr("������:")), 0, 0, Qt::AlignRight);
	glayout->addWidget(m_password = new QLineEdit, 0, 1);
	glayout->addWidget(new QLabel(tr("����� ������:")), 1, 0, Qt::AlignRight);
	glayout->addWidget(m_new_password = new QLineEdit, 1, 1);
	glayout->addWidget(new QLabel(tr("������������� ������:")), 2, 0, Qt::AlignRight);
	glayout->addWidget(m_confirm_new_password = new QLineEdit, 2, 1);
	glayout->addWidget(m_change_password = new QCheckBox(tr("�������� ������")),3,0,1,2, Qt::AlignLeft);

	m_password->setEchoMode(QLineEdit::Password);
	m_new_password->setEchoMode(QLineEdit::Password);
	m_confirm_new_password->setEchoMode(QLineEdit::Password);

	m_new_password->setEnabled(false);
	m_confirm_new_password->setEnabled(false);

	m_change_password->setTristate(false);

	QHBoxLayout *blayout = new QHBoxLayout;
	blayout->addStretch(2);
	blayout->addWidget(&m_btn_accept);
	blayout->addStretch(1);
	blayout->addWidget(&m_btn_reject);
	blayout->addStretch(2);

	QVBoxLayout *layout = new QVBoxLayout;
	layout->addLayout(glayout);
	layout->addStretch(1);
	layout->addLayout(blayout);

	setLayout(layout);
	setWindowTitle(tr("�����������"));
	setMinimumSize(300, 140);

	connect(&m_btn_accept, SIGNAL(clicked()), SLOT(accept()));
	connect(&m_btn_reject, SIGNAL(clicked()), SLOT(reject()));
	connect(m_change_password, SIGNAL(stateChanged(int)), SLOT(enableChangePassword(int)));
}
void LoginDialog::accept()
{
	// ��������� ������, ���� �� ���������, �� ������
	// ���� ���������, �� ������ ��������� ������ ��������, �������������, �������
	if(m_db.isOpen())
	{
		QSqlQuery query(m_db);
		if(!query.exec("SELECT password FROM users WHERE login='admin';"))
		{
			QMessageBox::critical(0, tr("������"), tr("������ ��� ������ ������ �� ������� springs!"));
			clear();
			QDialog::reject();
			return;
		}
		// �������� ������ ������
		query.next();

		if(m_password->text() == query.value(0).toString())
		{
			// ���������, ����� �� ������� ����������� ������
			if(m_change_password->checkState() == Qt::Checked)
			{
				QString new_pass = m_new_password->text().trimmed();
				QString confirm_new_pass = m_confirm_new_password->text().trimmed();
				if(new_pass.isEmpty() || confirm_new_pass.isEmpty())
					QMessageBox::warning(0, tr("������"), tr("�� ������� ������� ������: ������ �� ����� ���� ������!"));
				else if(new_pass != confirm_new_pass)
					QMessageBox::warning(0, tr("������"), tr("�� ������� ������� ������: ������������ ����������!"));
				else
				{
					QSqlQuery q(m_db);
					if(!q.exec(QString("UPDATE users SET password='")+new_pass+QString("' WHERE login='admin';")))
					{
						QMessageBox::warning(0, tr("������"), tr("�� ������� ������� ������: ������ ��� ���������� ������� � ���� ������!"));
					}
					else
						QMessageBox::information(0, tr("���������"), tr("������ ������� �������!"));
				}
			}

			clear();
			QDialog::accept();
			return;
		}
		else
			QMessageBox::warning(0, tr("������"), tr("�������� ������!"));
	}
	else
		QMessageBox::critical(0, tr("������"), tr("��� ������� � ���� ������!"));

	clear();
	QDialog::reject();
}
void LoginDialog::reject()
{
	// ������� ����, ��������� ����, ������ �� ��������
	clear();
	QDialog::reject();
}
void LoginDialog::clear()
{
	m_password->clear();
	m_new_password->clear();
	m_confirm_new_password->clear();
}
void LoginDialog::enableChangePassword(int state)
{
	bool isChange = (state == Qt::Checked);
	if(isChange)
	{
		m_new_password->clear();
		m_confirm_new_password->clear();
	}
	m_new_password->setEnabled(isChange);
	m_confirm_new_password->setEnabled(isChange);
}
///////////////////////////////////////////////////////////////////////////////
//

SpringEdit::SpringEdit(const SpringScheme& s, QWidget *parent, Qt::WFlags flags )
	: QDialog(parent, flags)
	, m_btn_save(tr("���������"))
	, m_btn_cancel(tr("������"))
	, m_spring(s)
	, is_edit(false)
{
	setWindowTitle(tr("��������� ������� �� �������"));

	// ���� ��� �������������� ���������� ��������� �������
	QGridLayout* gl = new QGridLayout;
	// 1-� �������
	gl->addWidget(new ParamName(tr("����� ���������:")), 0, 0);
	gl->addWidget(m_fields[_f_engine] = new TEditField(""," ",150),0,1);
	gl->addWidget(new ParamName(tr("���������� �������:")), 1, 0);
	gl->addWidget(m_fields[_f_target] = new TEditField(""," ",150),1,1);
	gl->addWidget(new ParamName(tr("������:")), 2, 0);
	gl->addWidget(m_fields[_f_scheme] = new TEditField("","",150),2,1);
	gl->addWidget(new ParamName(tr("��� �������:")), 3, 0);
	gl->addWidget(m_fields[_f_type] = new TEditField("","",150),3,1);
	gl->addWidget(new ParamName(tr("������:")), 4, 0);
	gl->addWidget(m_fields[_f_group] = new TEditField("","",150),4,1);
	gl->addWidget(new ParamName(tr("��� �������:")), 5, 0);
	gl->addWidget(m_fields[_f_step] = new TEditField("",tr("��."),150),5,1);
	gl->addWidget(new ParamName(tr("�����-�� ����:")), 6, 0);
	gl->addWidget(m_fields[_f_step_irregular] = new TEditField("",tr("��."),150),6,1);
	gl->addWidget(new ParamName(tr("��������-���:")), 7, 0);
	gl->addWidget(m_fields[_f_not_ortogonal] = new TEditField("",tr("��."),150),7,1);
	gl->addWidget(new ParamName(tr("������� ������:")), 8, 0);
	gl->addWidget(m_fields[_f_stick_diam] = new TEditField("",tr("��."),150),8,1);
	//gl->addWidget(new ParamName(tr("�����. �������(min):")), 9, 0);
	//gl->addWidget(m_fields[_f_i_diam_min] = new TEditField("",tr("��."),150),9,1);
	//gl->addWidget(new ParamName(tr("�����. �������(max):")), 10, 0);
	//gl->addWidget(m_fields[_f_i_diam_max] = new TEditField("",tr("��."),150),10,1);
	gl->addWidget(new ParamName(tr("�����. �������:")), 9, 0);
	gl->addWidget(m_fields[_f_inner_diam] = new TEditField("",tr("��."),150),9,1);
	//gl->addWidget(new ParamName(tr("����. �������(min):")), 11, 0);
	//gl->addWidget(m_fields[_f_o_diam_min] = new TEditField("",tr("��."),150),11,1);
	//gl->addWidget(new ParamName(tr("����. �������(max):")), 12, 0);
	//gl->addWidget(m_fields[_f_o_diam_max] = new TEditField("",tr("��."),150),12,1);
	gl->addWidget(new ParamName(tr("����. �������:")), 10, 0);
	gl->addWidget(m_fields[_f_outer_diam] = new TEditField("",tr("��."),150),10,1);

	// 2-� �������
	gl->addWidget(new ParamName(tr("������ ������� (min):")), 0, 2);
	gl->addWidget(m_fields[_f_height_min] = new TEditField("",tr("��."),150),0,3);
	gl->addWidget(new ParamName(tr("������ ������� (max):")), 1, 2);
	gl->addWidget(m_fields[_f_height_max] = new TEditField("",tr("��."),150),1,3);
	//gl->addWidget(new ParamName(tr("������ �������:")), 0, 2);
	//gl->addWidget(m_fields[_f_height] = new TEditField("",tr("��."),150),0,3);

	gl->addWidget(new ParamName(tr("����������� ��������:")), 2, 2);
	gl->addWidget(m_fields[_f_static_press] = new TEditField("",tr("���."),150),2,3);
	//gl->addWidget(new ParamName(tr("������ ������� (��� ������. ���������, min):")), 3, 2);
	//gl->addWidget(m_fields[_f_static_height_min] = new TEditField("",tr("��."),150),3,3);
	//gl->addWidget(new ParamName(tr("������ ������� (��� ������. ���������, max):")), 4, 2);
	//gl->addWidget(m_fields[_f_static_height_max] = new TEditField("",tr("��."),150),4,3);
	gl->addWidget(new ParamName(tr("������ ������� (��� ������. ���������):")), 3, 2);
	gl->addWidget(m_fields[_f_static_height] = new TEditField("",tr("��."),150),3,3);

	gl->addWidget(new ParamName(tr("������ ������� (min):")), 4, 2);
	gl->addWidget(m_fields[_f_static_arrow_min] = new TEditField("",tr("��."),150),4,3);
	gl->addWidget(new ParamName(tr("������ ������� (max):")), 5, 2);
	gl->addWidget(m_fields[_f_static_arrow_max] = new TEditField("",tr("��."),150),5,3);
	//gl->addWidget(new ParamName(tr("������ �������:")), 3, 2);
	//gl->addWidget(m_fields[_f_static_arrow] = new TEditField("",tr("��."),150),3,3);

	gl->addWidget(new ParamName(tr("������� ��������:")), 6, 2);
	gl->addWidget(m_fields[_f_exam_press] = new TEditField("",tr("���."),150),6,3);
	gl->addWidget(new ParamName(tr("������ ������� �������:")), 7, 2);
	gl->addWidget(m_fields[_f_cover_height] = new TEditField("",tr("��."),150),7,3);

	for(size_t i = 0; i < _f_counts; i++)
		connect(m_fields[i], SIGNAL(textEdited(const QString&)), SLOT(editEvent(const QString&)));

	QHBoxLayout* btn_layout = new QHBoxLayout;
	btn_layout->addWidget(&m_btn_save);
	btn_layout->addWidget(&m_btn_cancel);
	btn_layout->setAlignment(Qt::AlignRight);

	m_btn_save.setEnabled(false);
	m_btn_cancel.setEnabled(true);
	connect(&m_btn_save, SIGNAL(clicked()), SLOT(accept()));
	connect(&m_btn_cancel, SIGNAL(clicked()), SLOT(reject()));

	// ���������� ���� ��������� ����������
	QVBoxLayout* layout = new QVBoxLayout;
	layout->addLayout(gl);         // ���� ��� ��������������
	layout->addLayout(btn_layout); // ������
	setLayout(layout);

	setSpring(s);
}

SpringEdit::~SpringEdit(){}

void SpringEdit::accept()
{
	// ��������� ���������� �� ����� � m_spring
	m_spring.engine = m_fields[_f_engine]->text();
	m_spring.target = m_fields[_f_target]->text();
	m_spring.scheme = m_fields[_f_scheme]->text();
	m_spring.type = m_fields[_f_type]->text();
	m_spring.group = m_fields[_f_group]->text().toInt();
	m_spring.step = m_fields[_f_step]->text().toDouble();
	m_spring.step_irregular = m_fields[_f_step_irregular]->text().toDouble();
	m_spring.not_ortogonal = m_fields[_f_not_ortogonal]->text().toDouble();
	m_spring.stick_diam = m_fields[_f_stick_diam]->text().toDouble();
	//m_spring.inner_diam_min = m_fields[_f_i_diam_min]->text().toDouble();
	//m_spring.inner_diam_max = m_fields[_f_i_diam_max]->text().toDouble();
	m_spring.inner_diam = m_fields[_f_inner_diam]->text().toDouble();
	//m_spring.outer_diam_min = m_fields[_f_o_diam_min]->text().toDouble();
	//m_spring.outer_diam_max = m_fields[_f_o_diam_max]->text().toDouble();
	m_spring.outer_diam = m_fields[_f_outer_diam]->text().toDouble();

	m_spring.height_min = m_fields[_f_height_min]->text().toDouble();
	m_spring.height_max = m_fields[_f_height_max]->text().toDouble();
	//m_spring.height = m_fields[_f_height]->text().toDouble();

	m_spring.static_press = m_fields[_f_static_press]->text().toInt();
	//m_spring.static_height_min = m_fields[_f_static_height_min]->text().toDouble();
	//m_spring.static_height_max = m_fields[_f_static_height_max]->text().toDouble();
	m_spring.static_height = m_fields[_f_static_height]->text().toDouble();

	m_spring.static_arrow_min = m_fields[_f_static_arrow_min]->text().toDouble();
	m_spring.static_arrow_max = m_fields[_f_static_arrow_max]->text().toDouble();
	//m_spring.static_arrow = m_fields[_f_static_arrow]->text().toDouble();

	m_spring.exam_press = m_fields[_f_exam_press]->text().toInt();
	m_spring.cover_height = m_fields[_f_cover_height]->text().toInt();

	clear();
	QDialog::accept();
}

void SpringEdit::reject()
{
	// ������� ����
	clear();
	QDialog::reject();
}
void SpringEdit::setSpring(const SpringScheme& sp)
{
	m_fields[_f_engine]->setText(sp.engine);
	m_fields[_f_target]->setText(sp.target);
	m_fields[_f_scheme]->setText(sp.scheme);
	m_fields[_f_type]->setText(sp.type);
	m_fields[_f_group]->setText(QString::number(sp.group));
	m_fields[_f_step]->setText(QString::number(sp.step));
	m_fields[_f_step_irregular]->setText(QString::number(sp.step_irregular));
	m_fields[_f_not_ortogonal]->setText(QString::number(sp.not_ortogonal));
	m_fields[_f_stick_diam]->setText(QString::number(sp.stick_diam));
	//m_fields[_f_i_diam_min]->setText(QString::number(sp.inner_diam_min));
	//m_fields[_f_i_diam_max]->setText(QString::number(sp.inner_diam_max));
	m_fields[_f_inner_diam]->setText(QString::number(sp.inner_diam));
	//m_fields[_f_o_diam_min]->setText(QString::number(sp.outer_diam_min));
	//m_fields[_f_o_diam_max]->setText(QString::number(sp.outer_diam_max));
	m_fields[_f_outer_diam]->setText(QString::number(sp.outer_diam));
	m_fields[_f_height_min]->setText(QString::number(sp.height_min));
	m_fields[_f_height_max]->setText(QString::number(sp.height_max));
	//m_fields[_f_height]->setText(QString::number(sp.height));
	m_fields[_f_static_press]->setText(QString::number(sp.static_press));
	//m_fields[_f_static_height_min]->setText(QString::number(sp.static_height_min));
	//m_fields[_f_static_height_max]->setText(QString::number(sp.static_height_max));
	m_fields[_f_static_height]->setText(QString::number(sp.static_height));
	m_fields[_f_static_arrow_min]->setText(QString::number(sp.static_arrow_min));
	m_fields[_f_static_arrow_max]->setText(QString::number(sp.static_arrow_max));
	//m_fields[_f_static_arrow]->setText(QString::number(sp.static_arrow));
	m_fields[_f_exam_press]->setText(QString::number(sp.exam_press));
	m_fields[_f_cover_height]->setText(QString::number(sp.cover_height));
}
void SpringEdit::editEvent(const QString& s)
{
	if(!is_edit || !m_btn_save.isEnabled())
	{
		is_edit = true;
		m_btn_save.setEnabled(true);
	}
}
void SpringEdit::clear()
{
	for(size_t i = 0; i < _f_counts; i++)
		m_fields[i]->clear();
	m_btn_save.setEnabled(false);
	is_edit = false;
}
///////////////////////////////////////////////////////////////////////////////
//
QString SpringScheme::insertQuery() const
{
	if(id == 0) return QString("");
	QString query = "insert into springs(id, engine, target, scheme, sp_type, sp_group, step, step_irreg, "
	"not_ortogonal, stick_diam, inner_diam, outer_diam, height_min, height_max, "
	"static_press, static_height, static_arrow_min, static_arrow_max, exam_press, cover_height"
	") values(" + toString(id) + ",'" + engine + "','" + target + "','" + scheme + "','" + type + "'," + 
	toString(group) + "," + toString(step) + "," +
	toString(step_irregular) + "," + toString(not_ortogonal) + "," + toString(stick_diam) + "," + 
	toString(inner_diam) + "," + toString(outer_diam) + "," + 
	toString(height_min) + "," + toString(height_max) + "," + 
	toString(static_press) + "," + toString(static_height) + "," + 
	toString(static_arrow_min) + "," + toString(static_arrow_max) + "," + 
	toString(exam_press) + "," + toString(cover_height) + ");";
	return query;
}
QString SpringScheme::updateQuery() const
{
	if(id == 0) return QString("");

	QString query = "update springs set "
		"engine = '" + engine + "',"
		"target = '" + target + "',"
		"scheme = '" + scheme + "',"
		"sp_type = '" + type + "',"
			"sp_group = " + toString(group) + ","
			"step = " + toString(step) + ","
			"step_irreg = " + toString(step_irregular) + ","
			"not_ortogonal = " + toString(not_ortogonal) + ","
			"stick_diam = " + toString(stick_diam) + ","
			"inner_diam = " + toString(inner_diam) + ","
			"outer_diam = " + toString(outer_diam) + ","
			"height_min = " + toString(height_min) + ","
			"height_max = " + toString(height_max) + ","
			"static_press = " + toString(static_press) + ","
			"static_height = " + toString(static_height) + ","
			"static_arrow_min = " + toString(static_arrow_min) + ","
			"static_arrow_max = " + toString(static_arrow_max) + ","
			"exam_press = " + toString(exam_press) + ","
			"cover_height = " + toString(cover_height) + 
			" where id = " + toString(id) + ";";
			;
	
	return query;
}
QString SpringScheme::deleteQuery() const
{
	if(id == 0) return QString("");
	QString query = "delete from springs where id = " + toString(id) + ";";
	return query;
}
QString SpringScheme::toString(int i) const
{
	return QString::number(i);
}
QString SpringScheme::toString(double d) const
{
	return QString::number(d, 'f', 3);
}
///////////////////////////////////////////////////////////////////////////////
// for debug

/*void FetchSpringDialog::print(const SpringScheme& sp)
{
	std::ofstream out("sp.txt", std::ios::out);
	if(out.is_open())
	{
		out << sp.engine.toAscii().data() << std::endl;
		out << sp.target.toAscii().data() << std::endl;
		out << sp.scheme.toAscii().data() << std::endl;
		out << sp.type.toAscii().data() << std::endl;
		out << sp.group << std::endl;
		out.close();
	}
}*/

/*std::ostream& operator<<(std::ostream& out, const Spring& sp)
{
	out << sp.engine.toStdString() << "," << sp.target.toStdString() << "," << sp.scheme.toStdString() << "," 
		<< sp.type.toStdString() << "," << sp.group << "," 
		<< sp.step << "," << sp.step_irregular << "," << sp.not_ortogonal << "," << sp.stick_diam << ","
		<< sp.inner_diam_min << "," << sp.inner_diam_max << "," << sp.outer_diam_min << "," 
		<< sp.outer_diam_max << "," << sp.height_min << "," << sp.height_max << "," << sp.static_press << ","
		<< sp.static_height_min << "," << sp.static_height_max << "," << sp.static_arrow_min << ","
		<< sp.static_arrow_max << "," << sp.exam_press << "," << sp.cover_height << std::endl;
	return out;
}*/
