#include "activescheme.h"


ActiveScheme::ActiveScheme(const QString& scheme_name, QWidget* parent)
	: QLabel(parent)
	, m_rect_size(30, 15)
	, m_stick_diam_rect(QPointF(180, 215), m_rect_size)
	, m_inner_diam_rect(QPointF(179, 360), m_rect_size)
	, m_outer_diam_rect(QPointF(11, 361), m_rect_size)
	, m_step_rect(QPointF(92, 215), m_rect_size)
	, m_height_rect(QPointF(92, 186), m_rect_size)
	, m_static_height_rect(QPointF(92, 156), m_rect_size)
	, m_exam_height_rect(QPointF(92, 126), m_rect_size)
	, m_exam_press_rect(QPointF(212, 8), m_rect_size)
	, m_static_press_rect(QPointF(266, 31), m_rect_size)
{
	QSize scale_size(600, 500);
	if(!m_scheme_empty.load(scheme_name))
		QMessageBox::information(0, tr(""), tr("�� ������� ��������� ��������!"));

	setPixmap(m_scheme_empty);

	// debug: ��� ������� ����������������
	/*m_stick_diam = "111";
	m_inner_diam = "222";
	m_outer_diam = "33";
	m_step = "44";
	m_height = "55";
	m_static_height = "66";
	m_exam_height = "77";
	m_exam_press = "8888";
	m_static_press = "9999";*/

	updateIndicators();

	//clearIndicators();
}
void ActiveScheme::updateIndicators()
{
	QPixmap pix(m_scheme_empty);
	QPainter painter(&pix);
	painter.setPen(QPen(Qt::blue, 1));
	painter.drawText(m_stick_diam_rect, Qt::AlignCenter, m_stick_diam);
	painter.drawText(m_inner_diam_rect, Qt::AlignCenter, m_inner_diam);
	painter.drawText(m_outer_diam_rect, Qt::AlignCenter, m_outer_diam);
	painter.drawText(m_step_rect, Qt::AlignCenter, m_step);
	painter.drawText(m_height_rect, Qt::AlignCenter, m_height);
	painter.drawText(m_static_height_rect, Qt::AlignCenter, m_static_height);
//	painter.drawText(m_exam_height_rect, Qt::AlignRight, m_exam_height);
	painter.drawText(m_exam_press_rect, Qt::AlignCenter, m_exam_press);
	painter.drawText(m_static_press_rect, Qt::AlignCenter, m_static_press);

	// debug
/*	painter.drawRect(m_stick_diam_rect);
	painter.drawRect(m_inner_diam_rect);
	painter.drawRect(m_outer_diam_rect);
	painter.drawRect(m_step_rect);
	painter.drawRect(m_height_rect);
	painter.drawRect(m_static_height_rect);
//	painter.drawRect(m_exam_height_rect);
	painter.drawRect(m_exam_press_rect);
	painter.drawRect(m_static_press_rect);
*/
	setPixmap(pix);
}
void ActiveScheme::clearIndicators()
{
	m_stick_diam = "";
	m_inner_diam = "";
	m_outer_diam = "";
	m_step = "";
	m_height = "";
	m_static_height = "";
	m_exam_height = "";
	m_exam_press = "";
	m_static_press = "";

	updateIndicators();
}

