#include "params.h"

//debug
#include <fstream>

//���������������� ��������� ���������
SpringCalibrationConfig config;

//������ ���������� ��������� � ����� ������������
void SpringCalibrationConfig::parseConfigParameter(std::string group_name)
{
	using namespace std;
	check(m_token, ttId);
	std::string id = m_token.text;
	check(nextToken(), ttEqual);
	if(group_name == m_common.name)
	{
		     if(id == "laser_scale")	 m_common.laser_scale = fetchFloat();
		else if(id == "laser_offset")	 m_common.laser_offset = fetchFloat();
		else if(id == "laser_len_scale") m_common.laser_len_scale = fetchFloat();
		else if(id == "laser_dest_roll") m_common.laser_dest_roll = fetchFloat();
		else if(id == "roll_radius")     m_common.roll_radius = fetchFloat();
		else if(id == "not_ort_coef")    m_common.not_ort_coef = fetchFloat();
		else if(id == "press_scale_1")   m_common.press_scale_1 = fetchFloat();
		else if(id == "press_offset_1")  m_common.press_offset_1 = fetchFloat();
		else if(id == "press_scale_2")   m_common.press_scale_2 = fetchFloat();
		else if(id == "press_offset_2")  m_common.press_offset_2 = fetchFloat();
		else if(id == "ultra_scale")     m_common.ultra_scale = fetchFloat();
		else if(id == "ultra_offset")    m_common.ultra_offset = fetchFloat();
		else if(id == "ultra_const")     m_common.ultra_const = fetchFloat();
		else if(id == "deform_allow")    m_common.deform_allow = fetchFloat();
		else if(id == "stick_diam_min")  m_common.stick_diam_min = fetchNumber();
		else if(id == "stick_diam_max")  m_common.stick_diam_max = fetchNumber();
		else if(id == "inner_diam_min")  m_common.inner_diam_min = fetchNumber();
		else if(id == "inner_diam_max")  m_common.inner_diam_max = fetchNumber();
		else if(id == "outer_diam_min")  m_common.outer_diam_min = fetchNumber();
		else if(id == "outer_diam_max")  m_common.outer_diam_max = fetchNumber();
		else if(id == "debug")           m_common.debug = fetchNumber();
		else
			throw ConfigParseError("� ������ ������ ��� ������ ���������", m_line);
	}
	else
		throw ConfigParseError("������ ���������� � ����� ������ �� ����������", m_line);
}
void SpringCalibrationConfig::printValue()
{
	using namespace std;
	ofstream out("config_print.txt");
	out << "[" << m_common.name << "]" << endl;
	out << "\t" << "laser_scale = " << m_common.laser_scale << endl;
	out << "\t" << "laser_offset = " << m_common.laser_offset << endl;
	out << "\t" << "laser_len_scale = " << m_common.laser_len_scale << endl;
	out << "\t" << "laser_dest_roll = " << m_common.laser_dest_roll << endl;
	out << "\t" << "roll_radius = " << m_common.roll_radius << endl;
	out << "\t" << "not_ort_coef = " << m_common.not_ort_coef << endl;
	out << "\t" << "press_scale_1 = " << m_common.press_scale_1 << endl;
	out << "\t" << "press_offset_1 = " << m_common.press_offset_1 << endl;
	out << "\t" << "press_scale_2 = " << m_common.press_scale_2 << endl;
	out << "\t" << "press_offset_2 = " << m_common.press_offset_2 << endl;
	out << "\t" << "ultra_scale = " << m_common.ultra_scale << endl;
	out << "\t" << "ultra_offset = " << m_common.ultra_offset << endl;
	out << "\t" << "ultra_const = " << m_common.ultra_const << endl;
	out << "\t" << "deform_allow = " << m_common.deform_allow << endl;
	out << "\t" << "stick_diam_min = " << m_common.stick_diam_min << endl;
	out << "\t" << "stick_diam_max = " << m_common.stick_diam_max << endl;
	out << "\t" << "inner_diam_min = " << m_common.inner_diam_min << endl;
	out << "\t" << "inner_diam_max = " << m_common.inner_diam_max << endl;
	out << "\t" << "outer_diam_min = " << m_common.outer_diam_min << endl;
	out << "\t" << "outer_diam_max = " << m_common.outer_diam_max << endl;
	out << "\t" << "debug = " << m_common.debug << endl;
	out.close();
}