#ifndef SPRINGSCHEME_H
#define SPRINGSCHEME_H

#include <QtGui/QtGui>

// ������������� ���������� ����� �����, ����������� ����� ������ - 3

class ActiveScheme: public QLabel
{
	Q_OBJECT
public:
	ActiveScheme(const QString& scheme_name, QWidget* parent = 0);

	QString m_stick_diam;
	QString m_inner_diam;
	QString m_outer_diam;
	QString m_step;
	QString m_height;
	QString m_static_height;
	QString m_exam_height; // �� ����������
	QString m_exam_press;
	QString m_static_press;


public slots:
	void updateIndicators();
	void clearIndicators();

private:
	
	QPixmap m_scheme_empty;

	QSize m_rect_size;
	// coordinates
	QRectF m_stick_diam_rect;
	QRectF m_inner_diam_rect;
	QRectF m_outer_diam_rect;
	QRectF m_step_rect;
	QRectF m_height_rect;
	QRectF m_static_height_rect;
	QRectF m_exam_height_rect;
	QRectF m_exam_press_rect;
	QRectF m_static_press_rect;
};

#endif // SPRINGSCHEME_H
